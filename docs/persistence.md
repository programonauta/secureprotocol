# 		Persistence

## Keys Database (keys.db)

### Tables

#### Table Keys

| Table name  | keys |
|--|--|

| Field | Type | Description |
| ---- | ---- | ---- |
| secrectKey | Blob | Node's Secret Key. Depending on implementation the persisted Secret Key could be encrypted by a Symetric Key, safely stored in the device. |
| publicKey | Blob | Node's Public Key |
| isPub | Integer | Flag to inform if node is a Pub |

**Note**

- The keys are stored in binary format

## Pubs Database (pubs.db)

### Tables

#### Pub Invites

**Only Pubs have this table filled**

| Table name  | invites |
|--|--|

| Field  | Type    | Description                                                  |
| ------ | ------- | ------------------------------------------------------------ |
| invite | Text    | Invite issued by pub, in Base 64 format                      |
| node   | blob    | Node's Public Key that sent the invite for the first time. Will be null during the period between invite creation and granting the affiliation for the node that send the invite |
| status | Integer | `INVITE_CREATED`<br>`TICKET_GRANTED`<br>`INVITE_DENIED_PUBLIC_MESSGE` |

Explanation of status field:

- `INVITE_CREATED` The status received as soon as invite is created. In this case, node field must be NULL
- `TICKET_GRANTED` If a node send a valid invite using private message, the pub signs the node's public key and return the signature (ticket) to the node.
- `INVITE_DENIED_PUBLIC_MESSGE` If node send the invite using a public message, the invite is discarded and another invite is needed 

## Feeds

### Tables

#### Pubs affiliated 

**All nodes have this table filled**

|**Table Name**| pubsAffiliated|
|--|--|

| Field        | Type    | Description                                                  |
|------------| ------- | ------------------------------------------------------------ |
| pubPublicKey | Text    | Public Key of Pub                                            |
| pubInvite    | Text    | Invite Received                                              |
| status       | Integer | `INVITE_RECEIVED`<br>`INVITE_NOT_SENT`<br>`INVITE_SENT`<br>`INVITE_DENIED_INVALID_CODE`<br>`INVITE_DENIED_PUBLIC_MESSGE`<br>`TICKET_GRANTED` |
| pubTicket    | Text    | Pub's signature of Node's public Key                         |

**Notes**

- As soon as the node receive the invite for a Pub, a new record is created with:
  - Pub Public Key
  - Invite Received
  - Status = `INVITE_RECEIVED`

- When the program starts, this table is verified, if there is any record with status = `INVITE_RECEIVED`, a new private message is prepared and the status is changed do `INVITE_NOT_SENT` when the message is sent to another node, the status is changed to `INVITE_SENT`

- The Pub reply the message confirming or not the node affiliation, the status is updated accordingly:
  - `INVITE_DENIED_INVALID_CODE` The didn't recognized the invite code	
  - `INVIDE_DENIED_PUBLIC_MESSAGE` Node sent a public message to Pub asking for affiliation. In this case another invite is needed
  - `TICKET_RECEIVED` The Pub confirmed the invite code and sent back the Ticket



#### Feed	

#### Peers' Feed Information

This table store peers' information. This indicates the feed's name for each pair Peer x Pub

| Table Name | feedInfo |
| -- | -- |

| Field      | Type   | Description                                         |
| ---------- | ------ | --------------------------------------------------- |
| Pub        | Blob   | Address of Pub                                      |
| Sender     | Blob   | Address (Public Key) of Peer that issue the feed    |
| Ticket     | Blob   | Ticket issued by Pub (Sender PK signed by the Pub)  |
| Feed Table | String | Name of feed table where messages blocks are stored |

**Note**

- The Table name of Feed message is the string `t-` followed by first 8 bytes of sender public key in Hexadecimal format plus character `-` plus first 8 bytes of Pub public key.

  Example (in Hex format):

  - Pub PK `5474a10faf7709870d7ae31e370a7efbd2f1175586b6069a5b626877c67ebc07`
  - Sender PK `2608E79DE78FEB67CBD7BB69160F84E9A1ED64C7989FD6921B5B0A6190A223EF`

  Table Name: `t-5474a10faf770987-2608E79DE78FEB67`



Store messages issued by nodes (peers and own node)

The name of table is given by Peer's Feed Information table, in cases of peers. The feed issued by node is given by Pub Address

| Field         | Type    | Description                               |
| ------------- | ------- | ----------------------------------------- |
| Previous Hash | Blob    | Hash of previous message                  |
| Sequence      | Long    | Message sequence number                   |
| Recipient     | Blob    | Public Key of message recipient           |
| tagMsg        | Boolean | Indicates if message is private or public |
| nonce Message | Blob    | Random array created at signature         |
| Message       | String  | Message                                   |
| Signature     | Blob    | Signature of message                      |
| Block Hash    | Blob    | Hash of this message                      |
| Confirmations | Long    | Number of confirmations by peers          |

