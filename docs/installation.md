# Installation Instructions

## Required Tools

- C Compilers / Debugger / CMake
  - Linux
  ```bash
    sudo apt update
    sudo apt install build-essencial
    sudo apt-get install cmake
    sudo apt install gdb
  ```

- Tools
  - Git
  ```bash
    sudo apt-get install git
  ```
  - PlanUML (needs Java and Graphviz)
  ```bash
    # Install java
    sudo apt-get update
    sudo apt-get install openjdk-11-jdk
  ```

- Visual Studio Code
  - Extensions:
    - [C/C++ for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
    - [CMake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
    - [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
  - Optional Extensions
    - Documentation (Markdown)
      - [Markdown Preview](https://marketplace.visualstudio.com/items?itemName=ezrafree.markdown-preview)
      - [Markdown Preview Enhenced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
      - [Markdodwn All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
    - Documentation (Diagrams)
      - [Draw.io Integration](https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio)
      - [PlantUML](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)
      - [PlantUML Syntax](https://marketplace.visualstudio.com/items?itemName=qhoekman.language-plantuml)
    - Documentation (Code)
      - [Doxygen Documentation Generator](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen)
      - [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
     

## Suggested Tools
- [Table Plus](https://tableplus.com/): To examine SQLite databases

## Libraries

This prototype was developed in MacOSX and Linux (Debian). In the links below there are detailed instructions to install the libraries required to run the programs.

  - [LibSodium](./libSodiumInstall.md)
  - [SQLite3](./libSQLite3.md)
  - [Jansson](./libJanssonInstall.md)

  # Test the installation

  After clone the repo (https://github.com/programonauta/secureprotocol), open the file `secureprotocol.code-workspace` with Visual Studio Code.

  ## Select, build, and debug the project

1. **Select the project**: F1 -> CMake: Select Active Folder and choose the project
1. **Build**: F1 -> CMake: Build, if the compile kit is not yet selected the system will ask you to select.
1. **Debug**: F1 -> Debug: Start Debug
    - The folder selected by CMake Tools could not be the same that will run with `Start Debug` command. To select the correct project to debug, click on Run and Debug icon, on the left side bar, and select the project on the top 
    - It's possible debug clicking in the Bug icon on the bottom bar, but you can face problems in Mac Systems.


