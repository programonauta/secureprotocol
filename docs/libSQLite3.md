# SQLite3 Library Installation

Jansson is a C Jlibrary for encoding, decoding and manipulating JSON data.

[Documentation](https://www.sqlite.org/c3ref/intro.html)

## Install Debian

```bash
sudo apt-get update
sudo apt-get install sqlite3
```

If doesn't work

```bash
sudo apt-get install libsqlite3-dev
```

## Install Mac OSX

On macOS sqlite is preinstalled.

### Include files installed in

```
/usr/include
```

### Library files installed in

```
/usr/lib/sqlite3
```

### To compile

```bash
gcc <file.c> -o <file> -lsqlite3
```