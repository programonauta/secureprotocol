# Command Line Programs
- [Common Nodes](#common-nodes)
  - [Create Keys](#create-keys)
  - [Export Key Pair](#export-key-pair)
  - [Import Key Pair](#import-key-pair)
- [Pubs](#pubs)
  - [createInvite](#createinvite)
  - [importInvite](#importinvite)
## Common Nodes

### Create Keys

Verify if exists a key pair stored in the database. If yes send an error message, otherwise, create a key pair and store in the Database, and send a success message.

After that, show the public key stored in the database.

```
createKeys [OPTIONS] -t [r|p]

OPTIONS
    -h show this text
    -t type [r|p] Node Type: (r)egular or (p)ub.
    -v verbose
```

### Export Key Pair

Verify if there is a Key Pair stored in the Database, verify if are valid, and export them in Binary or Base64 Format

```
exportKeys [OPTIONS] -o FILENAME

OPTIONS
    -h show this text
    -f format [bin | b64] Format of keys: binary (default) or base 64.
    -v verbose
    -o FILENAME. Will be created two files:
        FILENAME.pk (public key)
        FILENAME.sk (secret key)
```

### Import Key Pair

Read a Key Pair files provided, verify if are valid and store them in the Database **only** if there is no previous keys stored. 

```
importKeys [OPTIONS] -p PUBLIC_KEY_FILENAME -s SECRET_KEY_FILENAME -t [r|p]

OPTIONS
    -h show this text
    -f format [bin | b64] Format of keys: binary (default) or base 64.
    -v verbose
    -u force update keys  
    -p PUBLIC_KEY_FILENAME
    -s SECRET_KEY_FILENAME
    -t type [r|p] Node Type: (r)egular or (p)ub.
NOTE
    PUBLIC_KEY_FILENAME and SECRET_KEY_FILANME must be in the same format
```

## Pubs

To be a pub affiliated, the node needs an invite, that is issued by the pub. The pub can generates this invite exporting it to a file. 

The pub also must validate the invite when receive a message asking for affiliation. Basically, the pub must verify:

- The invite exists
- The invite is already granted to another node

### createInvite

Verify if node is a Pub, if yes, create a new invite, store it in the database with status `INVITE_CREATED`, also create a file with Pub Public Key and Invite, both in base 64 format, divided by line feed (`\n`) character.

```
createInvite [OPTIONS] 

OPTIONS
    -o FILENAME (if not provided file name, will be xxxxxx-nnnnnn.inv)
        where:
            - xxxxxx (first 6 bytes of base64 Pub's Public Key)
            - nnnnnn (invite sequence)
    -v verbose
    -h show this text
```

### importInvite

To be used by any node, read the invite file issued by a Pub and stored it in database. The program tests if the invite was issued by own pub that are trying to import, and if the invite have been imported before.

```
importInvite [OPTIONS] -i FILENAME

OPTIONS
    -i FILENAME 
    -h show this text
```
