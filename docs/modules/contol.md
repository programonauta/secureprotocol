### Control

This module is responsible for protocol functionalities, like structure, store, send and receive messages, calculate hash, sign, encrypt messages and so on.

- [Message Blocks](#message-blocks)
  - [Functions](#functions)
    - [signMessage](#signmessage)
    - [verfiySignature](#verfiysignature)
    - [cypherMessage](#cyphermessage)
    - [decypherMessage](#decyphermessage)
    - [hashBlock](#hashblock)
    - [verifyHash](#verifyhash)
- [Feeds](#feeds)
  - [Functions](#functions-1)
    - [storeFirstMsgBlock](#storefirstmsgblock)
    - [storeMsgBlock](#storemsgblock)
    - [verifyFeed](#verifyfeed)

#### Message Blocks

C programs deal with messages in a structure:

```c
SYNOPSIS
    #include "control/control.h"
    
    #define HASH_SIZE 		crypto_hash_sha256_BYTES
    #define HASH_SIZE_B64 	(((4 * HASH_SIZE / 3) + 3) & ~3) + 1
    #define MESSAGE_SIZE	128
    #define MESSAGE_SIZE_B64 (((4 * MESSAGE_SIZE / 3) + 3) & ~3) + 1
    #define SIGNATURE_SIZE	crypto_sign_BYTES
    #define SIGNATURE_SIZE_B64 (((4 * SIGNATURE_SIZE / 3) + 3) & ~3) + 1
    
FORMAT
    typedef struct
    {
        unsigned char   previousHASH[HASH_SIZE];
        unsigned long   sequence;
        unsigned char   recipient[PUBLICKEY_SIZE];
        unsigned char   tagMsg[1];
        unsigned char   nonceSign[NONCE_MSG_SIZE];
        unsigned char   message[MESSAGE_SIZE];
        unsigned char   signature[SIGNATURE_SIZE];
        unsigned char   blockHASH[HASH_SIZE];
        unsigned long	confirmations;
    } msgBlock_t;
```

All communications will use JSON format. The fields are stored in Base 64 format, except for sequence (int) and priavte (bool) fields:

```json
{
    previousHASH: "wfMpACY6kbOyQ8BCdHWflpEFu97FJiQpmQ25fYdiaxI=",
    sequence: 3403,
    receiver: "4dJMxnF4BKluUHeUGqeL1b5joS7JuDAJF24k6NZvnew=",
    tagMsg: 1,
    nonceSign:
    message: "SGVsbG8gV29ybGQh",
    signature: "6fVPatv0OBONtEGac6VUUkzbB8QbSa52dcZqOVLDMrXDiFMc7h2EFkdJGNQmHMMI+0KcTCiRLNzD+FV0V26FCA==",
    blockHASH: "Peq8fiHxO4+HEAa+Ryv0K7Oi4wTT1d77jpmwP7V/I2Y="
}
```

##### Functions

###### signMessage

```c
NAME
    signMessage - sign a message stored in message block strucutre

SYNOPSIS
    #include "control/control.h"
    
    int signMessage(msgBlock_t* messageBlock, unsigned char* secretKey);

DESCRIPTION
    This function sign the message in the strucure messageBlock using secretKey and store the signature in the field signature.
    secretKey variable must be in binary format with size of SECRETKEY_SIZE bytes

RETURN VALUE
    CONTROL_OK              Signed OK 	
    CONTROL_SECRET_KEY_ERR  Any problem with public key: size, format, etc
    CONTROL_NULL_MESSAGE    The message in messageBlock structure is null
    CONTROL_FAIL            Another error

NOTES
    Null message is not acceptable
    Message must be a string (null teriminated)
```

See example in `exSignMessage` folder

###### verfiySignature

```
NAME
    verfiySignature - verifiy signature of a message stored in message block strucutre

SYNOPSIS
    #include "control/control.h"
    
    int verfiySignature(msgBlock_t* msgBlock, unsigned char *pk);

DESCRIPTION
    This function verify if the signature of message in the strucure messageBlock was issued by publicKey owner.
    publicKey variable must be in binary format with size of PUBLICKEY_SIZE bytes

RETURN VALUE
    CONTROL_OK              Signature is OK
    CONTROL_WRONG_SIGNATURE The signature was not issued by publicKey owner.
    CONTROL_PUBLIC_KEY_ERR  Any problem with public key: size, format, etc
    CONTROL_FAIL            Another error
```
See example in `exSignMessage` folder

###### cypherMessage

```
NAME
    cypherMessage - cypher the message, and store it in the message block
SYNOPSIS
    #include "control/control.h"
    
    int cypherMessage(msgBlock_t* msgBlock, unsinged char* sk, unsigned char* message);

DESCRIPTION
    This function generate a Nonce of size (MESSAGE_NONCE_SIZE), attach the nonce at the begining of message pointed by message argument, cypher all message (nonce + message) and stored the nonce (uncyphered) and cyphered message in message block strucutre

RETURN VALUE
    CONTROL_OK              Cypher is OK
    CONTROL_FAIL            Another error
```
###### decypherMessage

```
NAME
    decypherMessage - decypher the message stored in message block, and return it in the address pointed by message argument
SYNOPSIS
    #include "control/control.h"
    
    int decypherMessage(msgBlock_t* msgBlock, unsigned char* pk, unsigned char* message);

DESCRIPTION
    This function split the first MESSAGE_NONCE_SIZE bytes of a message stored in message block pointed by msgBlock, decypher the rest of message using public key pk. Compare the nonce sent with first MESSAGE_NONCE_SIZE bytes of decyphered message to verify if the process was ok.
   
RETURN VALUE
    CONTROL_OK                 Decypher is OK
    CONTROL_DECYPHER_ERR_SIZE  Size of message less than MESSAGE_NONCE_SIZE
    CONTROL_DECYPHER_ERR_NONCE Nonce of message doesn't match with Nonce of decyphered message
    CONTROL_FAIL               Another error
```

###### hashBlock

```
NAME
    hashBlock - calculate the hash of a block message and store it in messageHash field.

SYNOPSIS
    #include "control/control.h"

    int hashBlock(msgBlock_t* msgBlock);

DESCRIPTION
    This function calculate hash of the message block pointed by msgBlock.

RETURN VALUE
    CONTROL_OK              Hash calculated and stored in the msgBlock structure
    CONTROL_FAIL            Another error
```
See example in `exHashBlock` folder

###### verifyHash

```
NAME
    verifyHash - verify the hash of a message block

SYNOPSIS
    #include "control/control.h"
    
    int verifyHash(msgBlock_t* msgBlock);

DESCRIPTION
    This function calculate hash of the message block pointed by msgBlock.

RETURN VALUE
    CONTROL_OK              Hash ok
    CONTROL_WRONG_HASH      The hash doesn't match
    CONTROL_FAIL            Another error
```

See example in `exHashBlock` folder

#### Feeds

C programs deal with feeds, through the feed Header in a structure:

```c
SYNOPSIS
    #include "control/control.h"
    #include "control/cryptograph.h"
    
FORMAT
    typedef struct
    {
        unsigned char   sender[PUBLICKEY_SIZE];
        unsigned char   pub[PUBLICKEY_SIZE];
        unsigned char   ticket[SIGNATURE_SIZE];
    } feedHeader_t;
```

##### Functions

###### storeFirstMsgBlock

```
NAME
    storeAffiliationRequest - Prepare and Store request affiliation messages in the database.
SYNOPSIS
	#include "control/control.h"
    
    storeAffiliationRequest(int* msgsStored, int* msgsError, 
    						unsigned char *pk, unsigned char *sk);
    
DESCRIPTION
    For all invites with status 'INVITE_RECEIVED' prepare a private message to the pub requesting the affiliation and store it in the Database.
    
    If the function returns CONTROL_OK, the number of messages created is returned in msgsStored and number of errors in invites is returned in msgsError.
    The errors could be due
    - Invalid Invite (size)
    - Feed already has a valid ticket
    - There is another invite without an answer for Pub
    
RETURN VALUE
    PERSISTENCE_OK               Messages stored and/or verified
    PERSISTENCE_NOT_OPEN  Cannot open database
    PERSISTENCE_NOT_READ  Cannot read the database content
    PERSISTENCE_NOT_WRITE Cannot write the database content
    PERSISTENCE_FAIL      Any other error happens
```

See example in `exStore1stMessage` folder. This example verify if there is some new invite

###### storeMsgBlock

```
NAME
    storeMsgBlock - verify integrity of message block, and store in the database
    
SYNOPSIS
	#include "control/control.h"
    
    int storeMsgBlock(feedHeader* feedHeader, msgBlock_t* msgBlock);
    
DESCRIPTION
    Verify if ticket and message signature are valid, after this calculate hash and store in the database.
    
RETURN VALUE
    CONTROL_OK              Message Stored
    CONTROL_FEED_EMPTY  	Feed is empty, it must have the first message
    CONTROL_NO_TICKET       There is no Ticket issued by Pub
    CONTROL_WRONG_TICKET    Ticket was not signed by the pub, or have wrong size
    CONTROL_NULL_MESSAGE    Null message is not allowed
    CONTROL_FAIL            Another error
```

###### verifyFeed

```
NAME
    verifyFeed - verify integrity of feed
    
SYNOPSIS
	#include "control/control.h"
    
    int verifyFeed(feedHeader* feedHeader, msgBlock_t* msgBlock, unsigned long startMsgBLock, unsigned long lastMsgBlock);
    
DESCRIPTION
    Verify if ticket and message signature are valid, after this calculate hash and store in the database.
    
    if feed is not OK, lastMsgBlock returns the sequence of last message block verified, otherwise, return sequence of message block with error.
    
RETURN VALUE
    CONTROL_OK              Feed is ok
    CONTROL_FEED_EMPTY  	Feed is empty, it must have the first message
    CONTROL_NO_TICKET       There is no Ticket issued by Pub, and more then one message stored
    CONTROL_WRONG_TICKET    Ticket was not signed by the pub, or have wrong size
    CONTROL_NULL_MESSAGE    Null message is not allowed
    CONTROL_WRONG_HASH		Hash doesn't match
    CONTROL_FAIL            Another error
```

