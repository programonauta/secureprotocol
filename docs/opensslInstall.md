# Installation of library openssl

## Debian

```
sudo apt-get install libssl-dev
```

## Compiling

```
g++ <file.cpp> -o <file> -lssl -lcrypto
```



# man page

https://www.openssl.org/docs/man1.1.1/man3/

