# Protocol Synchronization

- [Protocol Synchronization](#protocol-synchronization)
- [1. Get Feed Information for common Pubs](#1-get-feed-information-for-common-pubs)
- [2. Get Feed Details](#2-get-feed-details)
- [3. Synchronization of  Messages](#3-synchronization-of--messages)
  - [3.1. Sending messages](#31-sending-messages)
  - [3.2. Getting Messages](#32-getting-messages)
- [4. State Machine](#4-state-machine)

Once two peers connected and finish the handshake procedure, start the message synchronization.

It has three steps:

# 1. Get Feed Information for common Pubs

Server asks for Feeds Information stored in the client through command `GET_FEED_INFO`. When client receives this command, it answer, with command `POST_FEED_INFO` the information stored on table **Peers' feed information**. One feed per time.

When there is no more Feed Info to send, the client send the Pub feed with a NULL character.

The server insert Feed information that is not present in his own table Feeds Information. Validating the ticket. 

**Note**: The feed can have a NULL ticket. But it only happen if in the feed there is only message to the pub asking affiliation. This validation will occur further.


```json
{
    "command": GET_FEED_INFO
}

{
    "command": POST_FEED_INFO,
    "pubAddress": "Pub Address Base 64",
    "senderAddress": "Sender Address Base 64" ,
    "ticket": "Ticket Base 64"
}
```

When finish this step, the server will have the feeds table with information from client:


| Field      | Description    |
| ---------- | ------         |
| Pub        | Address of Pub |
| Sender     | Address (Public Key) of Peer that issue the feed |
| Ticket     | Ticket issued by Pub (Sender PK signed by the Pub|
| Feed Table | Name of feed table where messages blocks are stored|



# 2. Get Feed Details

Once the server has the complete feed information, it will start the message  synchronization.

For each register in the Feed Information table, the server ask the client through `GET_FEED_DETAIL` with the fields:

- Pub
- Sender
- Ticket

The first thing the client do is verify the ticket received. It must be the Sender PK signed by the PUB, or NULL. 

If the ticket stored in cliente database is NULL, and the ticket received is valid, the database must be updated. In the same way,  If ticket received is NULL and the ticket stored is valid, the correct ticket is send in the Post command. The client answer in the command `POST_FEED_DETAIL` with the fields:

- Pub
- Sender
- Ticket
- Last Sequence
- Last Hash

**Note**: If the client doesn't have this feed yet, it create a new register in the database.

The client can answer with the following return codes when.

- Error - Ticket Invalid
- Peer is not affiliated to the pub

```json
{
    "command": GET_FEED_DETAIL,
    "pubAddress": "Pub Address Base 64",
    "senderAddress": "Sender Address Base 64" ,
    "ticket": "Ticket Base 64"
}

{
    "command": POST_FEED_DETAIL,
    "pubAddress": "Pub Address Base 64",
    "senderAddress": "Sender Address Base 64" ,
    "ticket": "Ticket Base 64",
    "lastSequence": LongInteger,
    "lastHASH": "HASH Base 64"
}
```

# 3. Synchronization of  Messages

Once server receive `POST_FEED_DETAIL` it verify through sequence received from client if it must send or receive message blocks

## 3.1. Sending messages

For feed, the server sends all remaining messages through `POST_MESSAGE` command with the fields:

- Pub
- Sender
- Ticket
- Previous Hash
- Sequence
- Recipient
- tagMsg
- Signature
- Block Hash

The client verify the block message and return the `RETURN_MESSAGE` with codes:

- Message OK
- Peer is not affiliated to the pub
- Invalid Ticket
- Invalid Sequence 
- Invalid Previous Hash
- Invalid Signature
- NULL Ticket with message to a recipient different than Pub

```json
{
    "command": POST_MESSAGE,
    "pubAddress": "Pub Address Base 64",
    "senderAddress": "Sender Address Base 64" ,
    "ticket": "Ticket Base 64",
    "previousHASH": "HASH Base 64",
    "sequence": longInteger,
    "recipient": "Recipient Address Base 64",
    "tagMsg": 0/1,
    "nonceMessage": "Nonce Base 64",
    "message": "Message Base 64",
    "signature": "Signature Base 64",
    "blockHASH": "HASH Base 64"
}

{
    "comannd": RETURN_MESSAGE,
    "pubAddress": "Pub Address Base 64",
    "senderAddress": "Sender Address Base 64" ,
    "ticket": "Ticket Base 64",
    "sequence": "HASH Base 64",
    "returnCode": int
}
```

## 3.2. Getting Messages

For feed, the server request all remaining messages through `GET_MESSAGE` command with the fields:

- Pub
- Sender
- Ticket
- Previous Hash
- Sequence

The client return the block message through `POST_MESSAGE` command.

If there is no more messages, or any error return the message  `RETURN_GET_MESSAGE`:

- No more messages (There is no sequence in the feed)
- Invalid Previous Hash for the sequence requested

When server receives the Message, it answer with `RETURN_MESSAGE` command.

The figure below shows the whole synchronization process. The protocol is controlled by a Status Machine and their status are represented by `STATE_*` in the figure.

```json
{
    "command": GET_MESSAGE,
    "pubAddress": "Pub Address Base 64",
    "senderAddress": "Sender Address Base 64" ,
    "ticket": "Ticket Base 64",
    "sequence": longInteger
}

{
    "command": RETURN_GET_MESSAGE,
    "returnCode": int
}
```

# 4. State Machine

![](./imgs/protocol-sync.png)

