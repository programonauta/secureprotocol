# Architecture

- [Architecture](#architecture)
- [1. Structure](#1-structure)
  - [1.1. Nodes](#11-nodes)
  - [1.2. The Pubs](#12-the-pubs)
  - [1.3. The invite](#13-the-invite)
  - [1.4. Example](#14-example)
- [2. How to participate](#2-how-to-participate)
- [3. Modules](#3-modules)
- [4. Cryptography](#4-cryptography)
  - [4.1. Keys and Identity](#41-keys-and-identity)
- [5. Messages](#5-messages)
  - [5.1. Formats](#51-formats)
    - [5.1.1. Signature](#511-signature)
    - [5.1.2. Hash](#512-hash)
      - [5.1.2.1. Original Message](#5121-original-message)
      - [5.1.2.2. Converted Strings](#5122-converted-strings)
      - [5.1.2.3. Hash256](#5123-hash256)
      - [5.1.2.4. Notes](#5124-notes)

# 1. Structure

The proposed approach is decentralized – there is no central server and all the communication is done P2P. 

## 1.1. Nodes

Each device (node) in the ICS is required to have a pair of private and public keys. The public key identifies the node in the network and the private key authenticates the messages issued by a node and also encrypts private messages.

## 1.2. The Pubs

It incorporates a special type of node: the pub, which will act as an distributed identity management
registry for other nodes in the ICS. Simillarly to SSB, a pub has a previously known IP address and it keeps a record of member nodes. 

## 1.3. The invite

To become a pub member, a new node needs an invite. A pub has a pre-generated list of invite codes that the ICS operator distributes. Invite codes are single-use-only – this avoids the inclusion of any device in the ICS, which could become a vulnerability.

## 1.4. Example

In the hypothetical example shown in Figure below, there are two Pubs to group messages from Water Treatment Plants (WTP) and Industrial Facilities. The Water Treatment Plants 1 and 2 are connected via a private network, but without connection to the Internet, but. To exchange messages with other nodes, the mobile node simply synchronizes the messages with one of the WTPs. The SCADA server is affiliated to both Pubs, thus it’s possible control and monitor all plants and facilities.

![](../imgs/hypotheticalExample.jpg)

# 2. How to participate

Once a new node connects to the network, it should request an association to a pub with an invite code, through private message to ensure the invite code is not disclosed in the network. If the pub receives an
invite code through a public message, it will be denied. 

If the invite code is valid, the pub: 

1. Creates an association ticket with the node public key; 

2. Sends this ticket to the node, and; 

3. Broadcasts the new node information to all other current associated nodes. 

This  way, any other pub member node will be able to authenticate the new node.

# 3. Modules

To implement the protocol some functionalities must be present:

- Cryptography - Create key pairs, calculate hash codes, sign messages, and encrypt private messages
- Socket - Connect nodes through TCP and UDP connections
- JSON - Format and parse message exchange between nodes
- Persistence - Persist data on the devices
- Control - Main module. Control the message's flow, synchronization, create and store message blocks in the feed, verify feed integrity,  and so on.

![](../imgs/abstractArch.jpg)

# 4. Cryptography

The Cryptography module uses the submodules depicted in the image above

- **Ed25519 Keys** - Create Ed25519 Key Pair (Public and Secret) to be used to sign messages
- **Ed25529 Curve** - Create Ed25519 Curve Key Pair, from Ed25519 Key Pair to be used to cypher messages
- **Sign Messages**
  - Create Signature of a message using issuer Secret Key
  - Verify Signature of a message using issuer Public Key
- **Cypher Messages**
  - Encrypt a message using the public key of the message's recipient
  - Decrypt the received message using the secret key
- **Base64** - Encode and Decode Public Keys, nonces, messages to exchange messages between nodes   
- **SHA256** - Create Hash Code of Messages 

## 4.1. Keys and Identity

All nodes need an identity to exchange messages. In this protocol, the identity is defined by a Ed25519 key pair.

The key pair is stored in the internal database (Please see Persistence Document), and is not created automatically by protocol client. The key pair must be created or imported by an external program. Trough the same external program the user can export the key pairs.

The public key is used into protocol to identify the nodes and is transmitted through nodes using base64 format. 

# 5. Messages

![First Message](../imgs/feedFirstMessage.jpg)

First Message in the Feed

![Messages](../imgs/messages.jpg)

Feed

## 5.1. Formats

### 5.1.1. Signature

Messages must be converted to Base 64 format before sign.  

### 5.1.2. Hash

The message hash is calculated over the string formed by concatenation of fields. See the example below:

#### 5.1.2.1. Original Message

- **Previous Hash**: `0xc1f32900263a91b3b243c04274759f969105bbdec5262429990db97d87626b12`
- **Sequence**: `3403`
- **Receiver**: `e1d24cc6717804a96e5077941aa78bd5be63a12ec9b83009176e24e8d66f9dec`
- **Tag**: `1` 
- **Message**: `"Hello World!"`
- **Signature**: `0xe9f54f6adbf438138db4419a73a554524cdb07c41b49ae7675c66a3952c332b5c388531cee1d8416474918d4261cc308fb429c4c28912cdcc3f85574576e8508`

#### 5.1.2.2. Converted Strings

- **Previous Hash**: `"wfMpACY6kbOyQ8BCdHWflpEFu97FJiQpmQ25fYdiaxI="`[1]
- **Sequence**: `"3404"` [2]
- **Receiver**: `"4dJMxnF4BKluUHeUGqeL1b5joS7JuDAJF24k6NZvnew="`[1]
- **Tag**: `"1"` [3]
- **Message**: `"SGVsbG8gV29ybGQh"`[1]
- **Signature**: `"6fVPatv0OBONtEGac6VUUkzbB8QbSa52dcZqOVLDMrXDiFMc7h2EFkdJGNQmHMMI+0KcTCiRLNzD+FV0V26FCA=="`

#### 5.1.2.3. Hash256

- String to be hashed

  `"wfMpACY6kbOyQ8BCdHWflpEFu97FJiQpmQ25fYdiaxI=34044dJMxnF4BKluUHeUGqeL1b5joS7JuDAJF24k6NZvnew=1SGVsbG8gV29ybGQhSGVsbG8gV29ybGQh6fVPatv0OBONtEGac6VUUkzbB8QbSa52dcZqOVLDMrXDiFMc7h2EFkdJGNQmHMMI+0KcTCiRLNzD+FV0V26FCA=="`

- Hash

  `25b8ea7e736a785345a143f5323341692345d0b7785044490316eaaf068325fc` 

#### 5.1.2.4. Notes

[1] Converted to Base 64 format
[2] Don't use leading zeros
[3] this string must be `"1" for true` or `"0" for false` 

