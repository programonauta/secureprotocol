# Jansson Library Installation

Jansson is a C Jlibrary for encoding, decoding and manipulating JSON data.

[Documentation](https://jansson.readthedocs.io/en/2.13/)

## Install Unix-like systems

Clone this repo:

https://github.com/akheron/jansson.git

```
cmake .
make
sudo make install
```

### Include files installed in

```
/usr/local/include
```

### Library files installed in

```
/usr/local/lib
```

File `libjansson.a`

### To compile

```bash
gcc <file.c> -o <file> -ljansson
```