# Sodium Library Installation

Sodium is a modern, easy-to-use software library for encryption, decryption, signatures, password hashing and more.

[Documentation](https://doc.libsodium.org/)

## Linux 

### Install library

```
sudo apt-get install libsodium-dev
```

### Or download and compile source code

Download the stable version (`*.LATEST.tar.gz`) in  https://download.libsodium.org/libsodium/releases/

#### Unzip and compile

```bash
tar -zxvf LATEST.tar.gz
cd LATEST
./configure
make && make check
sudo make install
```

For Linux systems

```bash
sudo ldconfig
```

### Include files installed in

```
/usr/local/include/sodium
```

### Library files installed in

```bash
/usr/local/lib
```

Files `libsodium*`

### To compile

```bash
gcc <file.c> -o <file> -lsodium
```