/**
 * @file createMsg.c
 * @author Ricardo Brandão
 * @brief create a message and insert it on feed
 * @version 0.1
 * @date 2021-07-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>
#include <getopt.h>
#include <libgen.h>
#include <string.h>
#include "createMsg.h"

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char verbose;
  unsigned char *pubPkB64[PUBLICKEY_SIZE_B64];
  unsigned char *recipientPkB64[PUBLICKEY_SIZE_B64];
  char *message[MESSAGE_SIZE];
  char private;
} options_t;

char verbose = 0;

void helpMsg(char *prgName);

/**
 * @brief Main program 
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char *argv[])
{
  int sockFd;

  int opt, rc;
  int i;

  options_t options;
  options_t *ptrOptions = &options;

  myPub_t myPub, myPubAux;
  myPub_t *ptrMyPub = &myPub;

  node_t node;
  node_t *ptrNode = &node;

  msgBlock_t msgBlock;
  msgBlock_t *ptrMsgBlock = &msgBlock;

  feedHeader_t feedHeader;
  feedHeader_t *ptrFeedHeader = &feedHeader;

  sqlite3_stmt *pStmt;

  memset(ptrOptions, 0, sizeof(options));
  memset(ptrMyPub, 0, sizeof(myPub));
  memset(ptrMsgBlock, 0, sizeof(msgBlock));

  char *horLine = "----------------------------------------------------------------\n";
  opterr = 0;

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "\nERROR: Please inform the parameters\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  const char *short_opt = "hvr:k:m:p";
  struct option long_opt[] =
      {
          {"help", no_argument, NULL, 'h'},
          {"verbose", no_argument, NULL, 'v'},
          {"recipient", required_argument, NULL, 'r'},
          {"pubpk", required_argument, NULL, 'k'},
          {"message", required_argument, NULL, 'm'},
          {"private", no_argument, NULL, 'p'},
          {NULL, 0, NULL, 0}};

  while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
  {
    switch (opt)
    {
    case -1: /* no more arguments */
    case 0:  /* long options toggles */
      break;
    case 'k':
      strncpy(ptrMyPub->pubPkB64, optarg, sizeof(myPub.pubPkB64));
      strncpy((char *)ptrOptions->pubPkB64, optarg, sizeof(options.pubPkB64));
      break;
    case 'r':
      strncpy(ptrMsgBlock->recipientPkB64, optarg, sizeof(msgBlock.recipientPkB64));
      strncpy((char *)ptrOptions->recipientPkB64, optarg, sizeof(options.recipientPkB64));
      break;
    case 'm':
      strcpy((char *)ptrOptions->message, optarg);
      break;
    case 'p':
      options.private = 1;
      break;
    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    case ':':
    default:
    case '?':
      fprintf(stderr, "\nERROR: Invalid argument\n", (char)opt);
      helpMsg(argv[0]);
      return (-2);
    };
  };

  printf("%s", horLine);

  if (strlen(myPub.pubPkB64) == 0)
  {
    fprintf(stderr, "Please inform Public Key of Pub\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen(myPub.pubPkB64) != (PUBLICKEY_SIZE_B64 - 1))
  {
    fprintf(stderr, "Invalid Public Key of Pub\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen(msgBlock.recipientPkB64) == 0)
  {
    fprintf(stderr, "Please inform Public Key of Recipient\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen(msgBlock.recipientPkB64) != (PUBLICKEY_SIZE_B64 - 1))
  {
    fprintf(stderr, "Invalid Public Key of Recipient\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen((char *)options.message) == 0)
  {
    fprintf(stderr, "Please inform the Message\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen((char *)options.message) >= (MESSAGE_SIZE))
  {
    fprintf(stderr, "Message too long. Maximum message size is %d\n", MESSAGE_SIZE);
    helpMsg(argv[0]);
    return (-2);
  }

  // Convert all fiedls already populated with B64 to Bin
  msgBlockB642Bin(ptrMsgBlock);

  // Copy message to structure
  strcpy(ptrMsgBlock->message, (const char *)options.message);

  rc = readNode(&node);

  if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "ERROR: There is not a key pair stored in database. Use createKeys before.\n");
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    return (EXIT_FAILURE);
  }

  rc = verifyMyPub();

  myPubB642Bin(ptrMyPub);

  rc = readMyPub(ptrMyPub);
  if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "ERROR: There is not a Pub Affiliation in Database.\n");
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get Pub Affiliation - Error code %d\n", rc);
    return (EXIT_FAILURE);
  }

  if (strlen(ptrMyPub->pubTicket) == 0)
  {
    fprintf(stderr, "The ticket was not received from Pub %s\n", ptrMyPub->pubPkB64);
    return (EXIT_FAILURE);
  }

  memcpy(ptrFeedHeader->pubPk, ptrMyPub->pubPk, sizeof(feedHeader.pubPk));
  memcpy(ptrFeedHeader->senderPk, (&node)->pk, sizeof(feedHeader.senderPk));
  memcpy(ptrFeedHeader->pubTicket, ptrMyPub->pubTicket, sizeof(feedHeader.pubTicket));

  printf("Pub      : %s\n", ptrMyPub->pubPkB64);
  printf("Recipient: %s\n", ptrMsgBlock->recipientPkB64);
  printf("Message  : %s\n", options.message);
  printf("Private  : %s\n%s", (options.private ? "YES" : "NO"), horLine);

  char tableName[SIZE_TABLE_NAME];

  rc = verifyFeedHeader(ptrFeedHeader);
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "ERROR: Problem when try to verfify feedHeader table\n");
    return (EXIT_FAILURE);
  }

  rc = getLastMsgBlock((char *)feedTableName(tableName, ptrFeedHeader->pubPk, ptrFeedHeader->senderPk),
                       ptrMsgBlock);

  memcpy(ptrMsgBlock->previousHASH, ptrMsgBlock->blockHASH, sizeof(msgBlock.previousHASH));
  msgBlock.sequence++;

  rc = createMsgBlock(ptrMsgBlock, &node);

  ptrMsgBlock->confirmations = 0;

  rc = insertMsgBlock(tableName, ptrMsgBlock);

  rc = verifySignMsgBlock(ptrMsgBlock, ptrNode->pk);
  printf("Sign Verified: %s\n",(rc==CRYPTO_OK ? "YES" : "NO"));

  rc = verifyHashMsgBlock(ptrMsgBlock);
  printf("HASH Verified: %s\n",(rc==CRYPTO_OK ? "YES" : "NO"));

  return (EXIT_SUCCESS);
}

void helpMsg(char *prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -k <Pub Public Key> -r <Recipient Public Key> -m <message>\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h --help      Show this text\n");
  printf("    -k --pubpk     Public Key of Pub (Base64 format)\n");
  printf("    -r --recipient Public Key of Recipient (Base64 format)\n");
  printf("    -m --message   Message to be sent between quotes\n");
  printf("    -p --private   For private messages\n");
  printf("    -v --verbose   Show all messages\n");
  return;
}
