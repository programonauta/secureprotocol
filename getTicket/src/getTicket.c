/**
 * @file getTicket.c
 * @author Ricardo Brandão
 * @brief Connect a Pub to get the ticket
 * @version 0.1
 * @date 2021-05-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>
//#include <unistd.h>
#include <getopt.h>
#include <libgen.h>
#include <string.h>
#include "protocolCli.h"

#define PORT "8008" // the port client will be connecting to

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char verbose;
  unsigned char *pubPkB64[PUBLICKEY_SIZE_B64];
  char pubIPAddr[20];
} options_t;

char verbose = 0;

void helpMsg(char *prgName);

/**
 * @brief Main program to connect to server and get the ticket
 * 
 * @param argc 
 * @param argv hostname to connect
 * @return int 
 */
int main(int argc, char *argv[])
{
  int sockFd;
  enum ipFamily ipFamily;

  int opt, rc;
  int i;

  options_t options;

  options_t *ptrOptions = &options;

  sqlite3_stmt *pStmt;

  memset(ptrOptions, 0, sizeof(options));

  myPub_t myPub, myPubAux;
  myPub_t *ptrMyPub = &myPub;
  node_t node;

  opterr = 0;

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "\nERROR: Please inform the parameters\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  const char *short_opt = "hvp:k:";
  struct option long_opt[] =
      {
          {"help", no_argument, NULL, 'h'},
          {"verbose", no_argument, NULL, 'v'},
          {"ipaddr", required_argument, NULL, 'p'},
          {"pubpk", required_argument, NULL, 'k'},
          {NULL, 0, NULL, 0}};

  while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
  {
    switch (opt)
    {
    case -1: /* no more arguments */
    case 0:  /* long options toggles */
      break;

    case 'k':
      strncpy(ptrMyPub->pubPkB64, optarg, sizeof(myPub.pubPkB64));
      strncpy((char *)ptrOptions->pubPkB64, optarg, sizeof(options.pubPkB64));
      break;
    case 'p':
      strncpy((char *)ptrOptions->pubIPAddr, optarg, sizeof(options.pubIPAddr));
      printf("Pub IP Address: %s\n", options.pubIPAddr);
      break;
    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    case ':':
    default:
    case '?':
      fprintf(stderr, "\nERROR: Invalid argument\n", (char)opt);
      helpMsg(argv[0]);
      return (-2);
    };
  };

  rc = persistenceInit();
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database:\n");
    return (EXIT_FAILURE);
  }

  rc = readNode(&node);

  if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "ERROR: There is not a key pair stored in database. Use createKeys before.\n");
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    return (EXIT_FAILURE);
  }


  if (strlen(myPub.pubPkB64) == 0)
  {
    fprintf(stderr, "Please inform Public Key of Pub\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen((const char *)options.pubIPAddr) == 0)
  {
    fprintf(stderr, "Please inform IP Address of Pub\n");
    helpMsg(argv[0]);
    return (-2);
  }

  if (strlen(myPub.pubPkB64) + 1 != PUBLICKEY_SIZE_B64)
  {
    printf("Pub Public Key: %s\n", myPub.pubPkB64);
    fprintf(stderr, "Wrong size of Public Key informed\n");
    helpMsg(argv[0]);
    return (-2);
  }

  myPubB642Bin(ptrMyPub);

  /**
   * @brief Verify if already have ticket from this pub
   * 
   */

  char *horLine = "----------------------------------------------------------------\n";

  int inviteCount = 0;
  int countReceived = 0;
  int countGranted = 0;

  printf("Invites received from Pub %s\n", myPub.pubPkB64);
  printf(horLine);
  printf(" Seq. | Invites Recieved                 | Status              | \n");
  printf(horLine);

  rc = prepareMyPubByPub(&pStmt, ptrMyPub);

  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Error during database read\n");
    return (EXIT_FAILURE);
  }

  rc = nextMyPub(pStmt, &myPubAux);

  while (rc == PERSISTENCE_OK)
  {
    switch (myPubAux.inviteStatus)
    {
    case INVITE_RECEIVED_BY_NODE:
      ++countReceived;
      memcpy((char*)&myPub, (char*)&myPubAux, sizeof(myPub));
      break;
    case INVITE_GRANTED:
      ++countGranted;
      break;
    }
    printf(" %04d | %s | %s\n",
           ++inviteCount,
           myPubAux.pubInvite,
           getStrInviteStatus(myPubAux.inviteStatus));
    rc = nextMyPub(pStmt, &myPubAux);
  }

  printf(horLine);
  if (inviteCount > 0)
  {
    printf("Invite%s ready to be sent to Pub: %3d\n", countReceived == 1?"":"s", countReceived);
    printf("Invite%s already granted        : %3d\n", countGranted == 1? "":"s", countGranted);
    printf(horLine);
  }

  if (countReceived == 0)
  {
    printf("This node there is no invite\nPlease insert an invite before try to get the ticket from pub\n");
    return (EXIT_FAILURE);
  }

  if (countGranted > 0)
  {
    printf("This node has already a ticket from the Pub %s\n", myPub.pubPkB64);
    printf("No communication will be estabilished with Pub\n");
    return (EXIT_FAILURE);
  }

  printf("The Pub will be connect to get the ticket. Invite to be used: %s\n", myPub.pubInvite);

  ipFamily = ipFamilyAny;

  sockFd = tcpConnect(ptrOptions->pubIPAddr, PORT, ipFamily);

  if (sockFd < 0)
  {
    fprintf(stderr, "Error trying to contact server at IP: %s\n", options.pubIPAddr);
    return (EXIT_FAILURE);
  }

  char s[INET6_ADDRSTRLEN];
  getPeerIP(sockFd, s);
  printf("Client: got connection to server %s\n", s);

  protocolGetTicket(&node, &myPub, sockFd);

  close(sockFd);

  rc = persistenceFinish();

  return (EXIT_SUCCESS);
}

void helpMsg(char *prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -k <Pub Public Key> -p <Pub IP address>\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h --help    Show this text\n");
  printf("    -k --pubpk   Public Key of Pub (Base64 format)\n");
  printf("    -p --ipaddr  IP Address of Pub\n");
  printf("    -v --verbose Show all messages\n");
  return;
}
