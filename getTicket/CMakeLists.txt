cmake_minimum_required(VERSION 3.0.0)
project(getTicket
    VERSION 0.1.0)

    file(GLOB SOURCES "src/*.c" 
    "../modules/persistence/*.c"
    "../modules/cryptography/*.c"
    "../modules/dataStructure/*.c"
    "../modules/protocol/protocol.c"
    "../modules/protocol/protocolCli.c"
    "../modules/protocol/protocolCmds.c"
    "../modules/protocol/cmdGetTicket.c"
    "../modules/protocol/cmdPostTicket.c"
    "../modules/protocol/cmdGetFeedHeader.c"
    "../modules/protocol/cmdPostFeedHeader.c"
    "../modules/protocol/cmdGetFeedDetail.c"
    "../modules/protocol/cmdPostFeedDetail.c"
    "../modules/protocol/cmdPostMessage.c"
    "../modules/protocol/cmdGetMessage.c"
    "../modules/protocol/cmdReturnMessage.c"
    "../modules/json/*.c")

link_libraries(sqlite3 sodium jansson)

include_directories(../modules/persistence)
include_directories(../modules/cryptography)
include_directories(../modules/dataStructure)
include_directories(../modules/protocol)
include_directories(../modules/json)
include_directories(../modules)

#set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")

add_executable(getTicket ${SOURCES})
