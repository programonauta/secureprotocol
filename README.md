# Secure Protocol for ICS Networks

- [Overview](#overview)
- [Installation](./docs/installation.md)
- [Architecture](./docs/architecture.md)
- [Synchornization](./docs/protocolSynchronize.md)
- Technical Reference
  - [Command Line Programs](./docs/cmdLinePgms.md)
  - Functions and Headers (by module)
    - [control](./docs/modules/control.md)
    - [cryptography](./docs/modules/crypography.md)
    - [persistence](./docs/modules/persistence.md)
    - [protocol](./docs/modules/protocol.md)

## Overview

A blockchain-based communication protocol for ICS to deal with security issues and with the storage constraints of the system’s devices. The protocol can achieve the following advantages:

- A straightforward identification mechanism based on invitation; 
- A flexible signature-based authentication method;
- The use of a chained feed of encrypted private messages to support integrity and confidentiality of communication; 
- A pruning strategy to reduce device storage constraint problems.  

This work proposes a protocol based on the blockchain and SSB ([Secure Scuttlebutt](https://ssbc.github.io/scuttlebutt-protocol-guide/)) ideas, to overcome authentication, integrity, and confidentiality issues in ICS. It intends to be decentralized and distributed, providing auditing, offline capability, and immutability message exchange between devices. Moreover, it is concerned with the storage capacity of the system’s devices.

##### Functions




