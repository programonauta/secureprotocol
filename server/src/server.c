
/**
 * @file server.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 17-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "server.h"

#define PORT "8008"  // the port users will be connecting to

#define BACKLOG 10   // how many pending connections queue will hold

void sigchld_handler(int s)
{
  // waitpid() might overwrite errno, so we save and restore it:
  int saved_errno = errno;

  while(waitpid(-1, NULL, WNOHANG) > 0);

  errno = saved_errno;
}


int main(void)
{
  int sockfd, newFd;  // listen on sock_fd, new connection on newFd
  struct addrinfo *p;
  struct sockaddr_storage clientAddr; // connector's address information
  socklen_t sinSize;
  struct sigaction sa;

  p = bindServer(PORT, &sockfd, BACKLOG);

  if (p == NULL)
  {
    fprintf(stderr, "server: failed to bind\n");
    exit(1);
  }

  sa.sa_handler = sigchld_handler; // reap all dead processes
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  if (sigaction(SIGCHLD, &sa, NULL) == -1)
  {
    perror("sigaction");
    exit(1);
  }

  printf("server: waiting for connections...\n");

  while(1)    // main accept() loop
  {
    sinSize = sizeof clientAddr;
    newFd = accept(sockfd, (struct sockaddr *)&clientAddr, &sinSize);
    if (newFd == -1)
    {
      perror("accept");
      continue;
    }

    if (!fork())   // this is the child process
    {
      close(sockfd); // child doesn't need the listener
      protocolSrv(newFd);
      printf("closed socket\n");
      close(newFd);
      exit(0);
    }
    close(newFd);  // parent doesn't need this
  }

  return 0;
}
