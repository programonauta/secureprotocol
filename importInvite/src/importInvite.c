/**
 * @file importInvite.c
 * @author Ricardo Brandão (rbrandao@protonmail.com)
 * @brief importInvite issued by a pub and store it in table pubsAffiliated
 *  USAGE: importInvite [OPTIONS] -i FILENAME
 *  OPTIONS
 *  -h show this text
 *  -v verbose
 *  -i FILENAME invite issued by pub
 * @version 0.1
 * @date 18-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */


#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <libgen.h>
#include <string.h>
#include "persistence.h"

#define OPTSTR "hvi:" // : after option make argument not optional

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char verbose;
  FILE *inputInvite; // Public Key
} options_t;

void helpMsg(char *prgName);

int main(int argc, char *argv[])
{

  unsigned char pkB64[PUBLICKEY_SIZE_B64];
  unsigned char pubPkB64[PUBLICKEY_SIZE_B64];

  node_t node;
  myPub_t myPub, myPubAux;

  myPub_t *ptrMyPubAux = &myPubAux;
  myPub_t *ptrMyPub = &myPub;

  int opt, rc;
  int i;

  char fileNameInvite[81];
  options_t options = {0, NULL};

  opterr = 0;

  memset(fileNameInvite, 0, sizeof(fileNameInvite));
  memset(pkB64, 0, sizeof(pkB64));
  memset(pubPkB64, 0, sizeof(pkB64));
  memset(&node, 0, sizeof(node));
  memset(&myPub, 0, sizeof(myPub));
  memset(&myPubAux, 0, sizeof(myPub));

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "\nERROR: Please inform invite file\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt(argc, argv, OPTSTR)) != EOF)
  {
    switch (opt)
    {
    case 'i':
      if (strlen(optarg) > 80)
      {
        fprintf(stderr, "\nERROR: Invite File name must be maximum 80 characters\n");
        exit(EXIT_FAILURE);
      }

      strcpy(fileNameInvite, optarg);
      break;

    case 'v':
      options.verbose += 1;
      break;

    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    default:
      fprintf(stderr, "\nERROR: Invalid argument -%c\n", (char)opt);
      helpMsg(argv[0]);
      break;
    }
  }

  if (fileNameInvite[0] == 0)
  {
    fprintf(stderr, "\nERROR: -i argument is mandatory\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (sodium_init() < 0)
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_SUCCESS);
  }

  rc = persistenceInit();
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database\n");
    return (EXIT_FAILURE);
  }

  rc = readNode(&node);

  if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "ERROR: There is not a key pair stored in database. Use createKeys before.\n");
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    return (EXIT_FAILURE);
  }

  if (!(options.inputInvite = fopen(fileNameInvite, "r")))
  {
    fprintf(stderr, "\nERROR: Error trying to read file: %s\n", fileNameInvite);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  // convert to Base 64
  bin2b64((char*)pkB64, sizeof(pkB64), (char*) node.pk, sizeof(node.pk));

  printf("Public key of this node: %s\n", pkB64);

  fseek(options.inputInvite, 0, SEEK_END);
  long fsize = ftell(options.inputInvite);

  int expectSize = PUBLICKEY_SIZE_B64 + PUB_INVITE_SIZE_B64;

  if (fsize != expectSize)
  {
    fprintf(stderr, "ERROR: Public Key file (%s) has wrong size (%d bytes). Must be %d bytes\n",
            fileNameInvite, (int)fsize, expectSize);
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  fseek(options.inputInvite, 0, SEEK_SET);
  fread(pubPkB64, 1, PUBLICKEY_SIZE_B64 - 1, options.inputInvite);
  fgetc(options.inputInvite); // read '\n'
  fread(ptrMyPub->pubInvite, 1, PUB_INVITE_SIZE_B64 - 1, options.inputInvite);

  fclose(options.inputInvite);

  printf("Public key of Pub      : %s\n", pubPkB64);

  // Test if the invite is from the same pub
  if (strncmp((char *)pubPkB64, pkB64, sizeof(pkB64)) == 0)
  {
    //fprintf(stderr, "ERROR: This invite was issued by the own node\n");
    //exit(EXIT_FAILURE);
  }

  b642bin(ptrMyPub->pubPk, sizeof(myPub.pubPk), pubPkB64, sizeof(pubPkB64));
  // Test if the invite is already saved
  myPub.inviteStatus = INVITE_RECEIVED_BY_NODE;

  memcpy((char*) ptrMyPubAux, (char*) ptrMyPub, sizeof(myPub));

  rc = readMyPub(&myPubAux);
  if (rc == PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR: This invite (%s) was already saved in database\n", myPub.pubInvite);
    return (EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "\nERROR: When try to get Pub Information. Error Code # %d\n", rc);
    return (EXIT_FAILURE);
  }

  // TODO Verifiy if there is others invites from same Pub

  // Save invite on database


  rc = insertMyPub(&myPub);

  if (rc == PERSISTENCE_OK)
  {  
    bin2b64((char*)pubPkB64, sizeof(pubPkB64), (char*) myPub.pubPk, sizeof(myPub.pubPk));
    printf("Pub Address %s\nInvite %s\n", pubPkB64, myPub.pubInvite);
  }  
  else
  {
    fprintf(stderr, "\nERROR: When try to insert Invite. Error Code # %d\n", rc);
    return (EXIT_FAILURE);
  }

  rc = persistenceFinish();

  return EXIT_SUCCESS;
}

void helpMsg(char *prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -i FILENAME\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h             show this text\n");
  printf("    -v verbose\n");
  printf("    -i FILENAME invite issued by pub\n");
  /* NOTREACHED */
}
