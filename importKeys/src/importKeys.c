/**
 * @file importKeys.c
 * @author Ricardo Brandão (rbrandao@protonmail.com)
 * @brief Import keys 
 * 
 * USAGE: importKeys [OPTIONS] -p PUBLIC_KEY_FILENAME -s SECRET_KEY_FILENAME
 * 
 * OPTIONS
 *  -h             show this text
 *  -f [bin | b64] format: binary or base 64
 *  -v verbose
 *  -u force update keys
 *  -p PUBLIC_KEY_FILENAME
 *  -s SECRET_KEY_FILENAME
 * NOTE
 *  PUBLIC_KEY_FILENAME and SECRET_KEY_FILANME must be in the same format
 * @version 0.1
 * @date 18-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <libgen.h>
#include <string.h>
#include "persistence/persistence.h"
#include "cryptography/cryptography.h"


#define OPTSTR "hf:vus:p:t:"  // : after option make argument not optional

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char        verbose;
  char        binary;
  char        update;
  char        nodeType;
  FILE        *inputPK; // Public Key
  FILE        *inputSK; // Secret Key
} options_t;

void helpMsg(char* prgName);

int main(int argc, char *argv[])
{

  unsigned char pk[PUBLICKEY_SIZE];
  unsigned char sk[SECRETKEY_SIZE];

  unsigned char pkB64[PUBLICKEY_SIZE_B64];
  unsigned char skB64[SECRETKEY_SIZE_B64];

  int opt, rc, errCode;
  const char* errorMsg;
  int i, sum = 0;

  char fileNamePK[81];
  char fileNameSK[81];
  options_t options = { 0, 1, 0, 0, NULL, NULL };

  opterr = 0;

  memset(fileNamePK, 0, sizeof(fileNamePK));
  memset(fileNameSK, 0, sizeof(fileNameSK));

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "ERROR: Please inform files\n\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt(argc, argv, OPTSTR)) != EOF)
  {
    switch(opt)
    {
    case 'p':
      if (strlen(optarg) > 80)
      {
        fprintf(stderr, "ERROR: Public Key File name must be maximum 80 characters\n");
        exit(EXIT_FAILURE);
      }

      strcpy(fileNamePK, optarg);
      break;

    case 's':
      if (strlen(optarg) > 80)
      {
        fprintf(stderr, "ERROR: Secret Key File name must be maximum 80 characters\n");
        exit(EXIT_FAILURE);
      }

      strcpy(fileNameSK, optarg);
      break;

    case 'f':

      if (strcmp(optarg, "b64")==0)
        options.binary = 0;
      else if (strcmp(optarg, "bin")==0)
        options.binary = 1;
      else
      {
        fprintf(stderr, "ERROR: invalid argument for option -f (%s)\n", optarg);
        helpMsg(argv[0]);
        exit(EXIT_FAILURE);
      }
      break;
    case 't':

      if (strcmp(optarg, "r")==0)
        options.nodeType = 'r';
      else if (strcmp(optarg, "p")==0)
        options.nodeType = 'p';
      else
      {
        fprintf(stderr, "\nERROR: invalid argument for option -t (%s)\n", optarg);
        helpMsg(argv[0]);
        exit(EXIT_FAILURE);
      }
      break;
    case 'u':
      options.update = 1;
      break;
    case 'v':
      options.verbose += 1;
      break;

    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    default:
      fprintf(stderr, "\nERROR: Invalid argument -%c\n", (char)opt);
      helpMsg(argv[0]);
      break;
    }
  }

  if (fileNamePK[0] == 0)
  {
    fprintf(stderr, "\nERROR: -p argument is mandatory\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (fileNameSK[0] == 0)
  {
    fprintf(stderr, "\nERROR: -s argument is mandatory\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (sodium_init() < 0)
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_SUCCESS);
  }

  rc = persistenceInit(&errCode, &errorMsg);
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database: (%d-%d) %s\n", rc, errCode, errorMsg);
    return (EXIT_FAILURE);
  }

  rc = getKeys(pk, sk, sizeof(pk), sizeof(sk));

  if (rc)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    rc = persistenceFinish(&errCode, &errorMsg);
    return(EXIT_FAILURE);
  }

  // Verifiy if pk is empty.
  for (i = 0; i < sizeof(pk); i++)
    sum += pk[i];

  // if sum != 0 means there are keys stored
  if (sum > 0 && !options.update)
  {
    fprintf(stderr, "There is already keys in database\n");
    helpMsg(argv[0]);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
  }

  if (!(options.inputPK = fopen(fileNamePK, "r")) )
  {
    fprintf(stderr, "ERROR: Error trying to read file: %s\n", fileNamePK);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  fseek(options.inputPK, 0, SEEK_END);
  long fsize = ftell(options.inputPK);

  int expectSize = options.binary?PUBLICKEY_SIZE:PUBLICKEY_SIZE_B64;

  if (fsize != expectSize)
  {
    fprintf(stderr, "ERROR: Public Key file (%s) has wrong size (%d bytes). Must be %d bytes\n",
            fileNamePK, (int)fsize, expectSize);
    helpMsg(argv[0]);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  fseek(options.inputPK, 0, SEEK_SET);
  if (options.binary)
    fread(pk, 1, PUBLICKEY_SIZE, options.inputPK);
  else
    fread(pkB64, 1, PUBLICKEY_SIZE_B64, options.inputPK);

  fclose(options.inputPK);

  if (!(options.inputSK = fopen(fileNameSK, "r")) )
  {
    fprintf(stderr, "ERROR: Error trying to read file: %s\n", fileNameSK);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  fseek(options.inputSK, 0, SEEK_END);
  fsize = ftell(options.inputSK);
  expectSize = options.binary?SECRETKEY_SIZE:SECRETKEY_SIZE_B64;

  if (fsize != expectSize)
  {
    fprintf(stderr, "ERROR: Secret Key file (%s) has wrong size (%d bytes). Must be %d bytes\n",
            fileNameSK, (int)fsize, expectSize);
    helpMsg(argv[0]);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  fseek(options.inputSK, 0, SEEK_SET);
  if (options.binary)
    fread(sk, 1, SECRETKEY_SIZE, options.inputSK);
  else
    fread(skB64, 1, SECRETKEY_SIZE_B64, options.inputSK);

  fclose(options.inputSK);

  if (!options.binary)
  {
    sodium_base642bin(pk, PUBLICKEY_SIZE, (char *)pkB64, PUBLICKEY_SIZE_B64, "\n\r", NULL, NULL, sodium_base64_VARIANT_ORIGINAL);
    sodium_base642bin(sk, SECRETKEY_SIZE, (char *)skB64, SECRETKEY_SIZE_B64, "\n\r", NULL, NULL, sodium_base64_VARIANT_ORIGINAL);
  }

  // Verify Key Pair
  rc = verifyKeyPair(pk, sk);

  if (rc != CRYPTO_OK)
  {
    fprintf(stderr, "ERROR: Key Pair Invalid\n");
    rc = persistenceFinish(&errCode, &errorMsg);
    return (EXIT_FAILURE);
  }

  if (options.verbose)
    printf("Keys verified\n");

  // Save keys on database

  rc = saveKeys(pk, sk, sizeof(pk), sizeof(sk), (options.nodeType=='p'?1:0));
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "ERROR: Save Keys failed\n");
    rc = persistenceFinish(&errCode, &errorMsg);
    return (EXIT_FAILURE);
  }

  sodium_bin2base64((char*)pkB64, PUBLICKEY_SIZE_B64, pk, PUBLICKEY_SIZE, sodium_base64_VARIANT_ORIGINAL);
  if (options.verbose)
  {
    printf("Key pair imported!\n");
    printf("Public Key: %s\n", pkB64);
  }

  rc = persistenceFinish(&errCode, &errorMsg);

  return EXIT_SUCCESS;
}

void helpMsg(char* prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -p PUBLIC_KEY_FILENAME -s SECRET_KEY_FILENAME\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h             show this text\n");
  printf("    -f [bin | b64] format: binary or base 64\n");
  printf("    -v verbose\n");
  printf("    -u force update keys\n");
  printf("    -p PUBLIC_KEY_FILENAME\n");
  printf("    -s SECRET_KEY_FILENAME\n");
  printf("NOTE\n");
  printf("    PUBLIC_KEY_FILENAME and SECRET_KEY_FILANME must be in the same format\n");
  /* NOTREACHED */
}
