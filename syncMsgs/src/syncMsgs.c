/**
 * @file syncMsgs.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 31-Jul-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "syncMsgs.h"

#define PORT "8008" // the port client will be connecting to


extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char verbose;
  char pubIPAddr[20];
} options_t;

char verbose = 0;

void helpMsg(char *prgName);

/**
 * @brief Main program to connect to server and synchronize messages
 * 
 * @param argc 
 * @param argv hostname to connect
 * @return int 
 */
int main(int argc, char *argv[])
{
  int sockFd;
  enum ipFamily ipFamily;

  int opt, rc;
  int i;

  options_t options;

  options_t *ptrOptions = &options;

  sqlite3_stmt *pStmt;

  memset(ptrOptions, 0, sizeof(options));

  node_t node;

  opterr = 0;

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "\nERROR: Please inform the parameters\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  const char *short_opt = "hvp:";
  struct option long_opt[] =
      {
          {"help", no_argument, NULL, 'h'},
          {"verbose", no_argument, NULL, 'v'},
          {"ipaddr", required_argument, NULL, 'p'},
          {NULL, 0, NULL, 0}};

  while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
  {
    switch (opt)
    {
    case -1: /* no more arguments */
    case 0:  /* long options toggles */
      break;

    case 'p':
      strncpy((char *)ptrOptions->pubIPAddr, optarg, sizeof(options.pubIPAddr));
      printf("Pub IP Address: %s\n", options.pubIPAddr);
      break;
    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    case ':':
    default:
    case '?':
      fprintf(stderr, "\nERROR: Invalid argument\n", (char)opt);
      helpMsg(argv[0]);
      return (-2);
    };
  };

  rc = persistenceInit();
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database:\n");
    return (EXIT_FAILURE);
  }

  rc = readNode(&node);

  if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "ERROR: There is not a key pair stored in database. Use createKeys before.\n");
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    return (EXIT_FAILURE);
  }

  if (strlen((const char *)options.pubIPAddr) == 0)
  {
    fprintf(stderr, "Please inform IP Address of Pub\n");
    helpMsg(argv[0]);
    return (-2);
  }

  ipFamily = ipFamilyAny;

  sockFd = tcpConnect(ptrOptions->pubIPAddr, PORT, ipFamily);

  if (sockFd < 0)
  {
    fprintf(stderr, "Error trying to contact server at IP: %s\n", options.pubIPAddr);
    return (EXIT_FAILURE);
  }

  char s[INET6_ADDRSTRLEN];
  getPeerIP(sockFd, s);
  printf("Client: got connection to server %s\n", s);

  protocolSyncMsgs(&node, sockFd);

  close(sockFd);

  //rc = persistenceFinish();

  return (EXIT_SUCCESS);
}

void helpMsg(char *prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -p <Pub IP address>\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h --help    Show this text\n");
  printf("    -p --ipaddr  IP Address of node\n");
  printf("    -v --verbose Show all messages\n");
  return;
}
