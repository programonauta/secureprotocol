/*
 * exportKey
 *
 */
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <libgen.h>
#include <string.h>
#include "persistence/persistence.h"
#include "cryptography/cryptography.h"


#define OPTSTR "hf:vo:"  // : after option make argument not optional

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char        verbose;
  char        binary;
  FILE        *outputPK; // Public Key
  FILE        *outputSK; // Secret Key
} options_t;

void helpMsg(char* prgName);

int main(int argc, char *argv[])
{

  unsigned char pk[PUBLICKEY_SIZE];
  unsigned char sk[SECRETKEY_SIZE];

  unsigned char pkB64[PUBLICKEY_SIZE_B64];
  unsigned char skB64[SECRETKEY_SIZE_B64];

  int opt, rc, errCode;
  const char* errorMsg;
  int i;

  char fileName[80];
  char fileArg[71];
  options_t options = { 0, 1, NULL, NULL };

  opterr = 0;

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "ERROR: Please inform file name to export keys\n\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  memset(fileArg, 0, sizeof(fileArg));

  while ((opt = getopt(argc, argv, OPTSTR)) != EOF)
    switch(opt)
    {
    case 'o':
      if (strlen(optarg) > 70)
      {
        fprintf(stderr, "ERROR: File name must be maximum 70 characters\n");
        exit(EXIT_FAILURE);
      }

      strcpy(fileArg, optarg);
      break;

    case 'f':

      if (strcmp(optarg, "b64")==0)
        options.binary = 0;
      else if (strcmp(optarg, "bin")==0)
        options.binary = 1;
      else
      {
        fprintf(stderr, "ERROR: invalid argument for option -f (%s)\n", optarg);
        helpMsg(argv[0]);
        exit(EXIT_FAILURE);
      }
      break;

    case 'v':
      options.verbose += 1;
      break;

    case 'h':
      helpMsg(argv[0]);
      return(EXIT_SUCCESS);
    default:
      fprintf(stderr, "\nERROR: Invalid argument -%c\n", (char)opt);
      helpMsg(argv[0]);
      break;
    }

  if (fileArg[0] == 0)
  {
    fprintf(stderr, "\nERROR: -o argument is mandatory\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  if (sodium_init() < 0)
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_FAILURE);
  }

  rc = persistenceInit(&errCode, &errorMsg);
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database: (%d-%d) %s\n", rc, errCode, errorMsg);
    return (EXIT_FAILURE);
  }

  rc = getKeys(pk, sk, sizeof(pk), sizeof(sk));

  if (rc == PERSISTENCE_KEY_EMPTY)
  {
    fprintf(stderr, "ERROR: There is not a Public Key stored in database. Use createKeys before.\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    return(EXIT_FAILURE);
  }

  if (options.verbose)
    printf("Keys gathered from database\n");

  // Verify Key Pair
  rc = verifyKeyPair(pk, sk);

  if (rc != CRYPTO_OK)
  {
    fprintf(stderr, "ERROR: Key Pair Invalid\n");
    rc = persistenceFinish(&errCode, &errorMsg);
    return (EXIT_FAILURE);
  }

  if (options.verbose)
    printf("Keys verified\n");

  // Open / Create output files

  strcpy(fileName, fileArg);
  strcat(fileName, ".pk");

  if (!(options.outputPK = fopen(fileName, "w")) )
  {
    fprintf(stderr, "ERROR: Error trying to create file: %s\n", fileName);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  if (options.verbose)
    printf("Public Key file opened: %s\n", fileName);

  strcpy(fileName, fileArg);
  strcat(fileName, ".sk");

  if (!(options.outputSK = fopen(fileName, "w")) )
  {
    fprintf(stderr, "ERROR: Error trying to create file: %s\n", fileName);
    fclose(options.outputPK);
    rc = persistenceFinish(&errCode, &errorMsg);
    exit(EXIT_FAILURE);
    /* NOTREACHED */
  }

  if (options.verbose)
    printf("Secret Key file opened: %s\n", fileName);

  sodium_bin2base64((char*)pkB64, PUBLICKEY_SIZE_B64, pk, PUBLICKEY_SIZE, sodium_base64_VARIANT_ORIGINAL);
  sodium_bin2base64((char*)skB64, SECRETKEY_SIZE_B64, sk, SECRETKEY_SIZE, sodium_base64_VARIANT_ORIGINAL);

  if (options.verbose)
    printf("Public Key: %s\n", pkB64);


  if (options.binary)
  {
    fwrite(pk, 1, PUBLICKEY_SIZE, options.outputPK);
    fwrite(sk, 1, SECRETKEY_SIZE, options.outputSK);
  }
  else
  {
    fwrite(pkB64, 1, PUBLICKEY_SIZE_B64, options.outputPK);
    fwrite(skB64, 1, SECRETKEY_SIZE_B64, options.outputSK);
  }

  if (options.verbose)
    printf("Key pair exported!\n");

  fclose(options.outputPK);
  fclose(options.outputSK);

  rc = persistenceFinish(&errCode, &errorMsg);

  return EXIT_SUCCESS;
}

void helpMsg(char* prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -o FILENAME\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h             show this text\n");
  printf("    -f [bin | b64] format: binary or base 64\n");
  printf("    -v verbose\n");
  printf("    -o FILENAME    create two files\n");
  printf("           FILENAME.pk (public key)\n");
  printf("           FILENAME.sk (secret key)\n");
  /* NOTREACHED */
}
