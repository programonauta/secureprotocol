/**
 * @file showNodeInfo.c
 * @author Ricardo Brandão (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 18-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "showNodeInfo.h"

int main(int argc, char *argv[])
{

  node_t node;

  unsigned char pkB64[PUBLICKEY_SIZE_B64];

  int rc;
  int i;

  char *horLine = "------------------------------------------------"\
                  "------------------------------------------\n";

  if (sodium_init() < 0) // TODO remove this sodium_init
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_FAILURE);
  }

  rc = persistenceInit();
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database:\n");
    return (EXIT_FAILURE);
  }

  rc = readNode(&node);

  if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "ERROR: There is not a Public Key stored in database. Use createKeys before.\n");
    exit(EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    return (EXIT_FAILURE);
  }

  rc = verifyMyPub();

  printf("Keys gathered from database\n");

  bin2b64((char*)pkB64, sizeof(pkB64), (char*) node.pk, sizeof(node.pk));

  printf("--------------------------------------------------"
         "----------------------------------------\n");
  printf(" Node Type : %s\n", node.nodeType==NODE_PUB ? "Pub" : "Regular");
  printf(" Public Key: %s\n", pkB64);
  printf(horLine);
  if (node.nodeType==NODE_PUB)
    detailInvitesIssued();

  detailMyPubs();
/*
  detailPubsAffiliated();

  detailFeeds(pk, 1);

  detailFeeds(pk, 0);
*/
  rc = persistenceFinish();

  return (EXIT_SUCCESS);
}
