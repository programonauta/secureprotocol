/**
 * @file printInfos.c
 * @author Ricardo Brandão (rbrandao@protonmail.com)
 * @brief Functions to show infos from tables
 * @version 0.1
 * @date 18-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "showNodeInfo.h"

/**
 * @brief Print details gathered from invites table
 * 
 * @return * int PERSISTENCE_OK if ok
 *               PERSISTENCE_NOT_OPEN if any problem to open database
 *               PERSISTENCE_NOT_READ Doesn't get perform select statement  
 */
int detailInvitesIssued()
{
    node_t node;

    inviteIssued_t inviteIssued;
    inviteIssued_t *ptrInvite = &inviteIssued;

    unsigned char pkB64[PUBLICKEY_SIZE_B64];

    char *horLine = "------------------------------------------------"
                    "-------------------------------------------------\n";

    sqlite3_stmt *pStmt;

    int rc;
    int inviteCount = 0, pkSize;

    rc = prepareInviteIssued(&pStmt);

    if (rc != PERSISTENCE_OK)
        return rc;

    rc = nextInviteIssued(pStmt, ptrInvite);

    printf("\n Seq. | Invites Issued                   | Status              | Node\n");
    printf(horLine);
    while (rc == PERSISTENCE_OK)
    {
        memset(pkB64, 0, PUBLICKEY_SIZE_B64);
        bin2b64((char *)pkB64, sizeof(pkB64), ptrInvite->nodePk, sizeof(inviteIssued.nodePk));

        printf(" %04d | %s | %s | %s \n",
               ++inviteCount,
               inviteIssued.inviteB64,
               getStrInviteStatus(inviteIssued.inviteStatus),
               isEmptyKey(ptrInvite->nodePk, sizeof(inviteIssued.nodePk)) ? "" : (char *)pkB64);
        rc = nextInviteIssued(pStmt, ptrInvite);
    }
    if (inviteCount > 0)
        printf(horLine);

    printf("Total invites issued: %d\n%s", inviteCount, horLine);

    return PERSISTENCE_OK;
}

int detailMyPubs()
{
    myPub_t myPub;

    myPub_t *ptrMyPub = &myPub;

    unsigned char pkB64[PUBLICKEY_SIZE_B64];
    memset(pkB64, 0, PUBLICKEY_SIZE_B64);

    char *horLine = "-------------------------------------------------"
                    "-----------------------------------------\n";

    sqlite3_stmt *pStmt;

    int rc;
    int pubsCount = 0, pkSize;

    memset(ptrMyPub, 0, sizeof(myPub));

    rc = prepareMyPub(&pStmt);

    if (rc != PERSISTENCE_OK)
        return rc;

    rc = nextMyPub(pStmt, ptrMyPub);

    printf("\n Pubs Affiliated\n");
    printf(horLine);

    while (rc == PERSISTENCE_OK)
    {
        bin2b64(pkB64, sizeof(pkB64), ptrMyPub->pubPk, sizeof(myPub.pubPk));
        printf("[%3d]      Pub: %s\n", ++pubsCount, pkB64);
        printf("        Invite: %s\n", myPub.pubInvite);
        printf("        Status: %s\n", getStrInviteStatus(myPub.inviteStatus));
        printf(horLine);
        rc = nextMyPub(pStmt, ptrMyPub);
    }
    printf("Total pubs affiliated: %d\n%s", pubsCount, horLine);

    return PERSISTENCE_OK;
}

int detailFeeds(char *myPk, int myFeed)
{
    unsigned char pk[PUBLICKEY_SIZE];
    unsigned char pkB64[PUBLICKEY_SIZE_B64];
    /*
    memset(pk, 0, PUBLICKEY_SIZE);
    memset(pkB64, 0, PUBLICKEY_SIZE_B64);

    char *horLine = "------------------------------------------------"
                    "------------------------------------------\n";

    sqlite3_stmt *pStmt;

    int rc, errCode;
    const char *zErrMsg;
    int feedsCount = 0, pkSize;

    const char *sqlSelectMyFeeds = "Select pub, sender, feedTable from feedInfo where sender = ? order by 1";
    const char *sqlSelectFeeds = "Select pub, sender, feedTable from feedInfo where sender <> ? order by 1";

    // dbKeys is not opened
    if (dbFeeds == NULL)
    {
        rc = persistenceInit(&errCode, &zErrMsg);
        fprintf(stderr, "ERROR [%s] (%d-%d) %s\n", __func__, rc, errCode, zErrMsg);
        return PERSISTENCE_ERR_OPEN;
    }

    if (myFeed)
        rc = sqlite3_prepare_v2(dbPubs, sqlSelectMyFeeds, -1, &pStmt, 0);
    else
        rc = sqlite3_prepare_v2(dbPubs, sqlSelectFeeds, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {

        fprintf(stderr, "[%s] ERROR %s\n", __func__, sqlSelectFeeds);
        fprintf(stderr, "[%s] [%s] ERROR Failed to prepare statement\n", __func__, sqlite3_db_filename(dbKeys, "main"));
        fprintf(stderr, "[%s] [%s] ERROR Cannot read database: %s\n", __func__, sqlite3_db_filename(dbKeys, "main"), sqlite3_errmsg(dbKeys));
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    sqlite3_bind_blob(pStmt, 1, myPk, PUBLICKEY_SIZE, SQLITE_STATIC);

    // read first row in the select

    rc = sqlite3_step(pStmt);

    printf("\n Feeds Issued by node\n");
    printf(horLine);

    while (rc == SQLITE_ROW)
    {

        memset(pk, 0, PUBLICKEY_SIZE);
        memset(pkB64, 0, PUBLICKEY_SIZE_B64);
        pkSize = sqlite3_column_bytes(pStmt, 1);
        strncpy((char *)pk, sqlite3_column_blob(pStmt, 0), pkSize);
        if (pkSize == PUBLICKEY_SIZE)
            sodium_bin2base64((char *)pkB64, PUBLICKEY_SIZE_B64, pk, PUBLICKEY_SIZE, sodium_base64_VARIANT_ORIGINAL);
        else if (pkSize > 0)
            strcpy((char *)pkB64, "PUBLIC KEY INVALID");
        printf("[%3d]      Pub: %s\n", ++feedsCount, pkB64);

        if (!myFeed)
        {
            memset(pk, 0, PUBLICKEY_SIZE);
            memset(pkB64, 0, PUBLICKEY_SIZE_B64);
            pkSize = sqlite3_column_bytes(pStmt, 2);
            strncpy((char *)pk, sqlite3_column_blob(pStmt, 2), pkSize);
            if (pkSize == PUBLICKEY_SIZE)
                sodium_bin2base64((char *)pkB64, PUBLICKEY_SIZE_B64, pk, PUBLICKEY_SIZE, sodium_base64_VARIANT_ORIGINAL);
            else if (pkSize > 0)
                strcpy((char *)pkB64, "PUBLIC KEY INVALID");
            printf("        Sender: %s\n", ++feedsCount, pkB64);
        }

        printf("    Table Name: %s\n", sqlite3_column_text(pStmt, 2));
        printf(horLine);

        rc = sqlite3_step(pStmt);
    }
    printf("Total Feeds: %d\n%s", feedsCount, horLine);

    sqlite3_finalize(pStmt);
*/
    return PERSISTENCE_OK;
}
