#ifndef SHOWNODEINFO_H
#define SHOWNODEINFO_H
#include <stdio.h>
#include <string.h>
#include "persistence.h"

extern sqlite3 *dbKeys, *dbPubs, *dbFeeds;

int detailInvitesIssued();
int detailMyPubs();
int detailFeeds(char *myPk, int myFeed);

#endif /* SHOWNODEINFO_H */
