/**
 * @file cmdGetTicket.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 22-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef CMDGETTICKET_H
#define CMDGETTICKET_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char sender[PUBLICKEY_SIZE];
    unsigned char senderB64[PUBLICKEY_SIZE_B64];
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char nonceEncrypt[NONCE_SIZE];
    unsigned char nonceEncryptB64[NONCE_SIZE_B64];
    unsigned char nonceSign[NONCE_MSG_SIZE];
    unsigned char nonceSignB64[NONCE_MSG_SIZE_B64];
    char flatInvite[MESSAGE_SIZE];
    char encryptedInvite[MESSAGE_SIZE];
    char encryptedInviteB64[MESSAGE_SIZE_B64];
    unsigned char signature[SIGNATURE_SIZE];
    unsigned char signatureB64[SIGNATURE_SIZE_B64];
} cmdGetTicket_t;

void cmdGetTicketBin2B64(cmdGetTicket_t *cmd);
void cmdGetTicketB642Bin(cmdGetTicket_t *cmd);

int encryptInvite(cmdGetTicket_t *cmd, node_t *sender);
int decryptInvite(cmdGetTicket_t *cmd, node_t *recipient);

int signCmdGetTicket(cmdGetTicket_t *cmd, node_t *sender);
int verifySignCmdGetTicket(cmdGetTicket_t *cmd);

int sendCmdGetTicket(cmdGetTicket_t *cmd, int sockFd);
int loadCmdGetTicket(cmdGetTicket_t *cmd, json_t *jsonPayLoad);

#endif