/**
 * @file cmdGetMessage.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 25-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef CMDGETMESSAGE_H
#define CMDGETMESSAGE_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
    unsigned char pubTicket[SIGNATURE_SIZE];
    unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
    unsigned long sequence;
} cmdGetMessage_t;

void cmdGetMessageBin2B64(cmdGetMessage_t *cmd);
void cmdGetMessageB642Bin(cmdGetMessage_t *cmd);

int sendCmdGetMessage(cmdGetMessage_t *cmd, int sockFd);
int loadCmdGetMessage(cmdGetMessage_t *cmd, json_t *jsonPayLoad);

#endif