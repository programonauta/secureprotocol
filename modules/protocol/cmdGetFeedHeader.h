/**
 * @file cmdGetFeedHeader.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 15-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef CMDGETFEEDHEADER_H
#define CMDGETFEEDHEADER_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
} cmdGetFeedHeader_t;

void cmdGetFeedHeaderBin2B64(cmdGetFeedHeader_t *cmd);
void cmdGetFeedHeaderBin2B64(cmdGetFeedHeader_t *cmd);

int sendCmdGetFeedHeader(cmdGetFeedHeader_t *cmd, int sockFd);
int loadCmdGetFeedHeader(cmdGetFeedHeader_t *cmd, json_t *jsonPayLoad);


#endif