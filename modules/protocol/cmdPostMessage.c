/**
 * @file cmdPostMessage.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 25-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "cmdPostMessage.h"

/**
 * @brief Convert all fields to Base64
 * cmdPostMessageBin2B64
 * @param cmd 
 */
void cmdPostMessageBin2B64(cmdPostMessage_t *cmd)
{
    bin2b64(cmd->pubPkB64, sizeof(cmd->pubPkB64), cmd->pubPk, sizeof(cmd->pubPk));
    bin2b64(cmd->senderPkB64, sizeof(cmd->senderPkB64), cmd->senderPk, sizeof(cmd->senderPk));
    bin2b64(cmd->pubTicketB64, sizeof(cmd->pubTicketB64), cmd->pubTicket, sizeof(cmd->pubTicket));
    bin2b64(cmd->previousHASHB64, sizeof(cmd->previousHASHB64), cmd->previousHASH, sizeof(cmd->previousHASH));
    bin2b64(cmd->recipientPkB64, sizeof(cmd->recipientPkB64), cmd->recipientPk, sizeof(cmd->recipientPk));
    bin2b64(cmd->nonceSignB64, sizeof(cmd->nonceSignB64), cmd->nonceSign, sizeof(cmd->nonceSign));
    bin2b64(cmd->messageB64, sizeof(cmd->messageB64), cmd->message, strlen((char *)cmd->message));
    bin2b64(cmd->signatureB64, sizeof(cmd->signatureB64), cmd->signature, sizeof(cmd->signature));
    bin2b64(cmd->blockHASHB64, sizeof(cmd->blockHASHB64), cmd->blockHASH, sizeof(cmd->blockHASH));
}

void cmdPostMessageB642Bin(cmdPostMessage_t *cmd)
{
    b642bin(cmd->pubPk, sizeof(cmd->pubPk), cmd->pubPkB64, sizeof(cmd->pubPkB64));
    b642bin(cmd->senderPk, sizeof(cmd->senderPk), cmd->senderPkB64, sizeof(cmd->senderPkB64));
    b642bin(cmd->pubTicket, sizeof(cmd->pubTicket), cmd->pubTicketB64, sizeof(cmd->pubTicketB64));
    b642bin(cmd->previousHASH, sizeof(cmd->previousHASH), cmd->previousHASHB64, sizeof(cmd->previousHASHB64));
    b642bin(cmd->recipientPk, sizeof(cmd->recipientPk), cmd->recipientPkB64, sizeof(cmd->recipientPkB64));
    b642bin(cmd->nonceSign, sizeof(cmd->nonceSign), cmd->nonceSignB64, sizeof(cmd->nonceSignB64));
    b642bin(cmd->message, sizeof(cmd->message), cmd->messageB64, strlen((char *)cmd->messageB64));
    b642bin(cmd->signature, sizeof(cmd->signature), cmd->signatureB64, sizeof(cmd->signatureB64));
    b642bin(cmd->blockHASH, sizeof(cmd->blockHASH), cmd->blockHASHB64, sizeof(cmd->blockHASHB64));
}

int sendCmdPostMessage(cmdPostMessage_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];

    char *horLine = "------------------------------------------------\n";

    memset(msgSent, 0, strlen(msgSent));

    cmdPostMessageBin2B64(cmd);

    sprintf(msgSent, "{\"command\": %d, "
                     "\"pub\": \"%s\", "
                     "\"sender\": \"%s\", "
                     "\"ticket\": \"%s\" ,"
                     "\"previousHASH\": \"%s\" ,"
                     "\"sequence\": %lu, "
                     "\"recipient\": \"%s\" ,"
                     "\"tagMsg\": %lu, "
                     "\"nonceSign\": \"%s\" ,"
                     "\"message\": \"%s\" ,"
                     "\"signature\": \"%s\" ,"
                     "\"blockHASH\": \"%s\" ,"
                     "\"confirmations\": %lu }",
            CMD_POST_MESSAGE,
            cmd->pubPkB64,
            cmd->senderPkB64,
            cmd->pubTicketB64,
            cmd->previousHASHB64,
            cmd->sequence,
            cmd->recipientPkB64,
            cmd->tagMsg,
            cmd->nonceSignB64,
            cmd->messageB64,
            cmd->signatureB64,
            cmd->blockHASHB64,
            cmd->confirmations);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }

    printf("\n%sCommand Sent: CMD_POST_MESSAGE\n", horLine);

    printf("\tPub          : %s\n", cmd->pubPkB64);
    printf("\tSender       : %s\n", cmd->senderPkB64);
    printf("\tTicket       : %s\n", cmd->pubTicketB64);
    printf("\tpreviousHASH : %s\n", cmd->previousHASHB64);
    printf("\tsequence     : %lu\n", cmd->sequence);
    printf("\trecipient    : %s\n", cmd->recipientPkB64);
    printf("\ttagMsg       : %lu\n", cmd->tagMsg);
    printf("\tnonceSign    : %s\n", cmd->nonceSignB64);
    printf("\tmessage      : %s\n", (cmd->tagMsg ? cmd->messageB64 : cmd->message));
    printf("\tsignature    : %s\n", cmd->signatureB64);
    printf("\tblockHASH    : %s\n", cmd->blockHASHB64);
    printf("\tconfirmations: %lu\n", cmd->confirmations);

    return PROTOCOL_OK;
}

/**
 * @brief Load data from Json
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int PROTOCOL_OK or PROTOCOL_FAIL
 */
int loadCmdPostMessage(cmdPostMessage_t *cmd, json_t *jsonPayLoad)
{
    int errCode;

    memset(cmd, 0, sizeof(cmdPostMessage_t));

    strcpy(cmd->pubPkB64, jsonGetString(jsonPayLoad, "pub", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->senderPkB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->pubTicketB64, jsonGetString(jsonPayLoad, "ticket", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->previousHASHB64, jsonGetString(jsonPayLoad, "previousHASH", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmd->sequence = jsonGetInt(jsonPayLoad, "sequence", &errCode);
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->recipientPkB64, jsonGetString(jsonPayLoad, "recipient", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmd->tagMsg = jsonGetInt(jsonPayLoad, "tagMsg", &errCode);
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->nonceSignB64, jsonGetString(jsonPayLoad, "nonceSign", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->messageB64, jsonGetString(jsonPayLoad, "message", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->signatureB64, jsonGetString(jsonPayLoad, "signature", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->blockHASHB64, jsonGetString(jsonPayLoad, "blockHASH", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmd->confirmations = jsonGetInt(jsonPayLoad, "confirmations", &errCode);
    if (errCode != 0)
        return PROTOCOL_FAIL;

    cmdPostMessageB642Bin(cmd);

    return PROTOCOL_OK;
}
