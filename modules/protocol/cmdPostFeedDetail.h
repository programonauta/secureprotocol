/**
 * @file cmdPostFeedDetail.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 19-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef CMDPOSTFEEDDETAIL_H
#define CMDPOSTFEEDDETAIL_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
    unsigned char pubTicket[SIGNATURE_SIZE];
    unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
    unsigned long lastSequence;
    unsigned char lastHASH[HASH_SIZE];
    unsigned char lastHASHB64[HASH_SIZE_B64];
} cmdPostFeedDetail_t;

void cmdPostFeedDetailBin2B64(cmdPostFeedDetail_t *cmd);
void cmdPostFeedDetailB642Bin(cmdPostFeedDetail_t *cmd);

int sendCmdPostFeedDetail(cmdPostFeedDetail_t *cmd, int sockFd);
int loadCmdPostFeedDetail(cmdPostFeedDetail_t *cmd, json_t *jsonPayLoad);

#endif