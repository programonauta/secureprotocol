/**
 * @file protocolCmds.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 22-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "protocolCmds.h"

/**
 * @brief Wait for message sent by peer
 * 
 * @param sockFd  - socket previously opened
 * @param jsonObj - json message address. This function fill the json message received
 * @param command - command address. This function fill the command received
 * @return int - Return the operation result
 * PROTOCOL_OK       - Json received is valid
 * PROTOCOL_FAIL     - Error on socket during message receiving
 * PROTOCOL_CLOSED   - Connection closed by server
 * PROTOCOL_NOT_JSON - Message is not in Json format 
 * PROTOCOL_NOT_KEY  - Invalid command received
 */
int receiveJSON(int sockFd, json_t **jsonObj, int *command)
{

  int numBytes, error;
  char msgRecv[MAX_MESSAGE_SIZE];

  memset(msgRecv, 0, sizeof(msgRecv));

  if ((numBytes = recv(sockFd, msgRecv, sizeof(msgRecv) - 1, 0)) == -1)
  {
    fprintf(stderr, "[%s] Error on Recv\n", __func__);
    return PROTOCOL_FAIL;
  }

  if (numBytes == 0)
    return PROTOCOL_CLOSED;

#ifdef DEBUG
  printf("[%s] Message Received: %s\n", __func__, msgRecv);
#endif
  if ((*jsonObj = loadJSON(msgRecv)) == NULL)
  {
    fprintf(stderr, "[%s] Received message is not a JSON: (%s)\n", __func__, msgRecv);
    return PROTOCOL_NOT_JSON;
  }


  *command = jsonGetInt(*jsonObj, "command", &error);
  if (error > 0)
  {
    fprintf(stderr, "[%s] Invalid Command Recevied: (%s)\n", __func__, msgRecv);
    return PROTOCOL_NOT_KEY;
  }

  return PROTOCOL_OK;
}

/**
 * @brief Get string of command
 * 
 * @param command 
 * @return const char* 
 */
const char *getStrCommand(int command)
{
  switch (command)
  {
  case CMD_GET_TICKET:
    return "CMD_GET_TICKET";
  case CMD_POST_TICKET:
    return "CMD_POST_TICKET";
  case CMD_GET_FEED_HEADER:
    return "CMD_GET_FEED_HEADER";
  case CMD_POST_FEED_HEADER:
    return "CMD_POST_FEED_HEADER";
  case CMD_GET_FEED_DETAIL:
    return "CMD_GET_FEED_DETAIL";
  case CMD_POST_FEED_DETAIL:
    return "CMD_POST_FEED_DETAIL";
  case CMD_GET_MESSAGE:
    return "CMD_GET_MESSAGE";
  case CMD_POST_MESSAGE:
    return "CMD_POST_MESSAGE";
  case CMD_RETURN_MESSAGE:
    return "CMD_RETURN_MESSAGE";
  case CMD_CLOSE_CONNECTION:
    return "CMD_CLOSE_CONNECTION";
  default:
    return "UNKNOWN COMMAND";
  }
}

/**
 * @brief Print received command
 * 
 * @param command Command to be printed
 * @param level 0 or 1
 */
void printReceivedCommand(int command, int level)
{
  char *horLine = "------------------------------------------------\n";

  printf("%s"TEXT_YELLOW"Command Received %s"COLOR_RESET"\n", (level?"\n\t":horLine), getStrCommand(command));

}