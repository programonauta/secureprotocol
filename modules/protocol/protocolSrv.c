/**
 * @file protocolSrv.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 17-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "protocolSrv.h"

/**
 * @brief Bind the server
 * 
 * @param port 
 * @param pSockFd 
 * @param backlog 
 * @return struct addrinfo* 
 */
struct addrinfo *bindServer(char *port, int *pSockFd, int backlog)
{
  struct addrinfo hints, *servinfo, *p;
  int rv;
  int yes = 1;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = ipFamilyAny;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE; // use my IP

  if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0)
  {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return NULL;
  }

  // loop through all the results and bind to the first we can
  for (p = servinfo; p != NULL; p = p->ai_next)
  {
    if ((*pSockFd = socket(p->ai_family, p->ai_socktype,
                           p->ai_protocol)) == -1)
    {
      perror("server: socket");
      continue;
    }

    if (setsockopt(*pSockFd, SOL_SOCKET, SO_REUSEADDR, &yes,
                   sizeof(int)) == -1)
    {
      perror("setsockopt");
      exit(1);
    }

    if (bind(*pSockFd, p->ai_addr, p->ai_addrlen) == -1)
    {
      close(*pSockFd);
      perror("server: bind");
      continue;
    }

    break;
  }

  freeaddrinfo(servinfo); // all done with this structure

  if (listen(*pSockFd, backlog) == -1)
  {
    perror("listen");
    exit(1);
  }

  return p;
}

int protocolSrv(int sockFd)
{
  char s[INET6_ADDRSTRLEN];
  char *horLine = "------------------------------------------------\n";

  int ret, resultInvite, resultCommand, rc;

  json_t *jsonPayLoad = NULL;
  json_t *jsonGetFeedHeader = NULL;
  json_t *jsonReturnMessage = NULL;
  json_t *jsonGetFeedDetail = NULL;
  cmdGetTicket_t cmdGetTicket;
  inviteIssued_t invite;
  inviteIssued_t *ptrInvite = &invite;

  feedDetail_t localFeedDetail;
  feedDetail_t *ptrLocalFeedDetail = &localFeedDetail;

  cmdGetFeedHeader_t cmdGetFeedHeader;

  int currentState = STSRV_WAIT_COMMAND;

  getPeerIP(sockFd, s);
  printf("server: got connection from %s\n", s);

  while (currentState != STSRV_END)
  {
    switch (currentState)
    {
    case STSRV_WAIT_COMMAND:
      printf("Waiting for Command\n");
      currentState = stSrvWaitCommand(sockFd, &jsonPayLoad);
      break;
    case STSRV_INVITE_RECEIVED:
      printReceivedCommand(CMD_GET_TICKET, 0);
      currentState = stSrvInviteReceived(sockFd, jsonPayLoad, &cmdGetTicket);
      break;
    case STSRV_VERIFY_INVITE:
      resultInvite = stSrvVerifyInvite(sockFd, &cmdGetTicket);
      if (resultInvite == INVITE_GRANTED)
        currentState = STSRV_POST_TICKET;
      else
        currentState = STSRV_INVALID_INVITE;
      break;
    case STSRV_INVALID_COMMAND:
      rc = stSrvInvalidCommand(sockFd, &cmdGetTicket, resultCommand);
      currentState = STSRV_CLOSE_CONNECTION;
      break;
    case STSRV_POST_TICKET:
      rc = stSrvPostTicket(sockFd, &cmdGetTicket);
      currentState = (rc == PROTOCOL_OK ? STSRV_UPDATE_INVITE : STSRV_CLOSE_CONNECTION);
      break;
    case STSRV_INVALID_INVITE:
      rc = stSrvInvalidInvite(sockFd, &cmdGetTicket, resultInvite);
      currentState = STSRV_UPDATE_INVITE;
      break;
    case STSRV_UPDATE_INVITE:
      rc = stSrvUpdateInvite(&cmdGetTicket, resultInvite);
      currentState = STSRV_CLOSE_CONNECTION;
      break;
    case STSRV_DEAL_GET_FEED_HEADER:
      printReceivedCommand(CMD_GET_FEED_HEADER, 0);
      jsonGetFeedHeader = jsonPayLoad;
      currentState = stSrvDealGetFeedHeader(sockFd, jsonGetFeedHeader, &jsonPayLoad, &cmdGetFeedHeader);
      break;
    case STSRV_DEAL_GET_FEED_DETAIL:
      jsonGetFeedDetail = jsonPayLoad;
      printReceivedCommand(CMD_GET_FEED_DETAIL, 0);
      currentState = stSrvDealGetFeedDetail(sockFd, jsonGetFeedDetail, ptrLocalFeedDetail);
      break;
    case STSRV_DEAL_RETURN:
      printReceivedCommand(CMD_RETURN_MESSAGE, 0);
      jsonReturnMessage = jsonPayLoad;
      currentState = stSrvDealReturn(sockFd, jsonReturnMessage, &jsonPayLoad);
      break;
    case STSRV_WAIT_CMD_MESSAGE:
      currentState = stSrvWaitCmdMessage(sockFd, &jsonPayLoad);
      break;
    case STSRV_DEAL_POST_MESSAGE:
      printReceivedCommand(CMD_POST_MESSAGE, 0);
      currentState = stSrvDealPostMessage(sockFd, jsonPayLoad, ptrLocalFeedDetail);
      break;
    case STSRV_DEAL_GET_MESSAGE:
      printReceivedCommand(CMD_GET_MESSAGE, 0);
      currentState = stSrvDealGetMessage(sockFd, jsonPayLoad, ptrLocalFeedDetail);
      break;
    case STSRV_CLOSE_CONNECTION:
      printReceivedCommand(CMD_CLOSE_CONNECTION, 0);
      printf("Closing connection\n%s", horLine);
    default:
      currentState = STSRV_END;
      break;
    }
  }

  ret = PROTOCOL_OK;

  return ret;
}

/**
 * @brief Treat State machine STSRV_WAIT_COMMAND
 * 
 * @param sockFd 
 * @param jsonPayLoad 
 * @return int The next state machine 
 */
int stSrvWaitCommand(int sockFd, json_t **jsonPayLoad)
{
  int rc, command;

  int retState; // Return the next state

  rc = receiveJSON(sockFd, jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
    return STSRV_INVALID_COMMAND;
  }
  switch (command)
  {
  case CMD_GET_TICKET:
    return STSRV_INVITE_RECEIVED;
    break;
  case CMD_GET_FEED_HEADER:
    return STSRV_DEAL_GET_FEED_HEADER;
    break;
  default:
    showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
    return STSRV_INVALID_COMMAND;
    break;
  }
}

/**
 * @brief Treat state machine STSRV_INVITE_RECEIVED
 * 
 * @param sockFd 
 * @param jsonPayLoad 
 * @return int next Status
 */
int stSrvInviteReceived(int sockFd, json_t *jsonPayLoad, cmdGetTicket_t *cmdGetTicket)
{
  int rc;

  rc = loadCmdGetTicket(cmdGetTicket, jsonPayLoad);
  if (rc != PROTOCOL_OK)
  {
    return STSRV_INVALID_COMMAND;
  }

  rc = verifySignCmdGetTicket(cmdGetTicket);
  if (rc == PROTOCOL_FAIL)
    return STSRV_INVALID_SIGNATURE;

  return STSRV_VERIFY_INVITE;
}

/**
 * @brief Function to verify if Invite received is valid, and if encryption is ok
 * 
 * @param sockFd 
 * @param cmdGetTicket 
 * @return int return Invite status to be returned to node and updated
 */
int stSrvVerifyInvite(int sockFd, cmdGetTicket_t *cmdGetTicket)
{
  int rc;

  inviteIssued_t invite;
  inviteIssued_t *ptrInvite = &invite;

  node_t node;
  node_t *ptrNode = &node;

  rc = readNode(&node);

  rc = decryptInvite(cmdGetTicket, &node);

  if (rc == PROTOCOL_FAIL)
  {
    fprintf(stderr, "Invalid Encryption\n");
    return INVITE_DENIED_ENCRYPT;
  }

  strncpy((char *)ptrInvite->inviteB64, cmdGetTicket->flatInvite + 7, sizeof(ptrInvite->inviteB64));

  printf("Received Invite: %s\n", (char *)ptrInvite->inviteB64);

  rc = readInviteIssued(ptrInvite);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_WARNING, __func__, INVITE_DENIED_NOT_FOUND, "Invite Not found");
    return INVITE_DENIED_NOT_FOUND;
  }

  if (memcmp(ptrInvite->nodePk, cmdGetTicket->sender, sizeof(node.pk)) == 0)
  {
    if (ptrInvite->inviteStatus == INVITE_GRANTED)
    {
      fprintf(stderr, "Invite Already Granted for this node\n");
      return INVITE_DENIED_ALREADY_GRANTED;
    }
    else
    {
      fprintf(stderr, "Invite Already Denied for this node\n");
      return INVITE_DENIED_PREVIOUSLY;
    }
  }
  else if (!isEmptyKey(ptrInvite->nodePk, sizeof(ptrInvite->nodePk))) // Node is not empty (already granted for other node)
  {
    fprintf(stderr, "Invite Already sent by other node.\n\tInvite Status: %s",
            getStrInviteStatus(ptrInvite->inviteStatus));
    return INVITE_DENIED_OTHER_NODE;
  }

  return INVITE_GRANTED;
}

int stSrvInvalidCommand(int sockFd, cmdGetTicket_t *cmdGetTicket, int resultCommand) // TODO Fill the function
{
}

/**
 * @brief Send Command Return Message depending on the Invite Status
 * 
 * @param sockFd 
 * @param cmdGetTicket 
 * @param resultInvite 
 * @return int PROTOCOL_OK or PROTOCOL_FAIL
 */
int stSrvInvalidInvite(int sockFd, cmdGetTicket_t *cmdGetTicket, int resultInvite)
{
  cmdReturnMessage_t cmdReturnMessage;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturnMessage;

  int rc, retCode;

  memset(ptrCmdReturn, 0, sizeof(cmdReturnMessage_t));

  memcpy(ptrCmdReturn->senderPk, cmdGetTicket->pubPk, sizeof(ptrCmdReturn->senderPk));
  memcpy(ptrCmdReturn->recipientPk, cmdGetTicket->sender, sizeof(ptrCmdReturn->recipientPk));

  switch (resultInvite)
  {
  case INVITE_DENIED_ENCRYPT:
    retCode = RET_INVALID_ENCRYPT;
    break;
  case INVITE_DENIED_NOT_FOUND:
    retCode = RET_INVITE_ERR_NOT_FOUND;
    break;
  case INVITE_DENIED_OTHER_NODE:
    retCode = RET_INVITE_ERR_OTHER_NODE;
    break;
  case INVITE_DENIED_ALREADY_GRANTED:
    retCode = RET_INVITE_ALREADY_GRANTED;
    break;
  case INVITE_DENIED_PREVIOUSLY:
    retCode = RET_INVITE_ALREADY_GRANTED;
    break;
  }

  ptrCmdReturn->returnCode = retCode;

  cmdReturnMessageBin2B64(ptrCmdReturn);

  rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command");
    return PROTOCOL_FAIL;
  }

  return PROTOCOL_OK;
}

/**
 * @brief Send Command CMD_POST_TICKET and wait for Command CMD_RETURN_MESSAGE
 * 
 * @param sockFd 
 * @param cmdGetTicket 
 * @return int PROTOCOL_OK if code received = RET_INVITE_GRANTED else PROTOCOL_FAIL 
 */
int stSrvPostTicket(int sockFd, cmdGetTicket_t *cmdGetTicket)
{

  cmdPostTicket_t cmdPostTicket;
  cmdPostTicket_t *ptrPostTicket = &cmdPostTicket;

  node_t node;

  int rc, command;

  json_t *jsonPayLoad = NULL;

  memset(ptrPostTicket, 0, sizeof(cmdPostTicket));

  memcpy(ptrPostTicket->pubPk, cmdGetTicket->pubPk, sizeof(cmdPostTicket.pubPk));
  memcpy(ptrPostTicket->recipient, cmdGetTicket->sender, sizeof(cmdPostTicket.recipient));

  rc = readNode(&node);

  rc = createTicket(ptrPostTicket, &node);

  printf("Ticket Created for Node %s\n", ptrPostTicket->recipientB64);

  cmdPostTicketBin2B64(ptrPostTicket);

  rc = sendCmdPostTicket(ptrPostTicket, sockFd);
  if (rc != PROTOCOL_OK)
    return PROTOCOL_FAIL;

  rc = receiveJSON(sockFd, &jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
    return STSRV_INVALID_COMMAND;
  }
  switch (command)
  {
  case CMD_RETURN_MESSAGE: // TODO Deal Return Message
    return PROTOCOL_OK;
    break;
  default:
    showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
    return PROTOCOL_FAIL;
    break;
  }

  return PROTOCOL_OK;
}

int stSrvUpdateInvite(cmdGetTicket_t *cmdGetTicket, int inviteStatus)
{
  inviteIssued_t invite;
  inviteIssued_t *ptrInvite = &invite;

  memcpy(ptrInvite->inviteB64, cmdGetTicket->flatInvite + 7, sizeof(ptrInvite->inviteB64));
  memcpy(ptrInvite->nodePk, cmdGetTicket->sender, sizeof(ptrInvite->nodePk));
  invite.inviteStatus = inviteStatus;

  printf("Invite is updated with status: %s\n", getStrInviteStatus(inviteStatus));

  int rc = updateInviteIssued(ptrInvite);

  return PROTOCOL_OK;
}

/**
 * @brief Deal state STSRV_DEAL_GET_FEED_HEADER
 *        At first time access Feed Header table
 *        While there is Feed Header
 *          Send command CMD_POST_FEED_HEADER and wait for Response 
 *          That must be CMD_GET_FEED_HEADER
 *        If no more Feed Header send CMD_POST_FEED_HEADER with NULL
 * 
 *        Wait for next command from client that could be:
 *          CMD_GET_FEED_DETAIL or CMD_CLOSE_CONNECTION
 * 
 * @param sockFd 
 * @param jsonGetFeedHeader Json with CMD_GET_FEED_HEADER
 * @param jsonPayLoad Return the Json with received command
 * @param cmdGetFeedHeader 
 * @return int Next state: 
 *          STSRV_DEAL_GET_FEED_DETAIL if received CMD_GET_FEED_DETAIL
 *          STSRV_DEAL_RETURN if received CMD_RETURN_MESSAGE
 *          STSRV_CLOSE_CONNECTION if received CMD_CLOSE_CONNECTION or
 *            any other unexepected Command
 */
int stSrvDealGetFeedHeader(int sockFd, json_t *jsonGetFeedHeader, json_t **jsonPayLoad, cmdGetFeedHeader_t *cmdGetFeedHeader)
{
  int rc, command;
  static sqlite3_stmt *pStmt = NULL;

  cmdReturnMessage_t cmdReturn;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturn;

  cmdPostFeedHeader_t cmdPost;
  cmdPostFeedHeader_t *ptrCmdPost = &cmdPost;

  feedHeader_t feedHeader;
  feedHeader_t *ptrFeedHeader = &feedHeader;

  loadCmdGetFeedHeader(cmdGetFeedHeader, jsonGetFeedHeader);

  printf("\tSender: %s\n", cmdGetFeedHeader->senderPkB64);

  if (pStmt == NULL)
  {
    rc = prepareFeedHeader(&pStmt);

    if (rc != PERSISTENCE_OK)
    {
      pStmt = NULL;
      return (EXIT_FAILURE);
    }
  }
  rc = nextFeedHeader(pStmt, ptrFeedHeader);

  switch (rc)
  {
  case (PERSISTENCE_OK):
    // Populate Post Feed Header
    memcpy(ptrCmdPost->pubPk, ptrFeedHeader->pubPk, sizeof(cmdPost.pubPk));
    memcpy(ptrCmdPost->senderPk, ptrFeedHeader->senderPk, sizeof(cmdPost.senderPk));
    memcpy(ptrCmdPost->pubTicket, ptrFeedHeader->pubTicket, sizeof(cmdPost.pubTicket));
    rc = sendCmdPostFeedHeader(ptrCmdPost, sockFd);
    printf("\tSend Command CMD_POST_FEED_HEADER\n");
    printf("\t---------------------------------\n");
    printf("\tPub   : %s\n", ptrCmdPost->pubPkB64);
    printf("\tSender: %s\n", ptrCmdPost->senderPkB64);
    printf("\tTicket: %s\n", ptrCmdPost->pubTicketB64);
    if (rc != PROTOCOL_OK)
      return STSRV_CLOSE_CONNECTION;
    break;
  case PERSISTENCE_NO_ROWS:
    pStmt = NULL;
    printf("\tSend Return Message with RET_NO_MORE_FEEDS\n");
    memset(ptrCmdReturn, 0, sizeof(cmdReturn));
    memcpy(ptrCmdReturn->recipientPk, cmdGetFeedHeader->senderPk, sizeof(cmdGetFeedHeader->senderPk));
    ptrCmdReturn->returnCode = RET_NO_MORE_FEEDS;

    cmdReturnMessageBin2B64(ptrCmdReturn);

    rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
    if (rc != PROTOCOL_OK)
    {
      showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command");
      return STSRV_CLOSE_CONNECTION;
    }
    break;
  case PERSISTENCE_ERR_READ:
  default:
    pStmt = NULL;
    return STSRV_CLOSE_CONNECTION;
  }

  rc = receiveJSON(sockFd, jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
    return STSRV_INVALID_COMMAND;
  }
  switch (command)
  {
  case CMD_GET_FEED_HEADER:
    return STSRV_DEAL_GET_FEED_HEADER;
    break;
  case CMD_GET_FEED_DETAIL:
    pStmt = NULL;
    return STSRV_DEAL_GET_FEED_DETAIL;
    break;
  case CMD_RETURN_MESSAGE:
    return STSRV_DEAL_RETURN;
    break;
  default:
    showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
    return STSRV_CLOSE_CONNECTION;
    break;
  }
}

/**
 * @brief Deal state STSRV_DEAL_RETURN
 *        The state machine reachs this state when receive a CMD_RETURN_MESSAGE
 *        after send a CMD_POST_FEED_HEADER
 *        The return command could be: RET_INVALID_TICKET
 * 
 * @param sockFd 
 * @param jsonPayLoad 
 * @param jsonPayLoadRecv 
 * @return int 
 */
int stSrvDealReturn(int sockFd, json_t *jsonGetReturnMsg, json_t **jsonPayLoad)
{
  int rc, command;

  rc = receiveJSON(sockFd, jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
    return STSRV_INVALID_COMMAND;
  }
  switch (command)
  {
  case CMD_GET_FEED_HEADER:
    return STSRV_DEAL_GET_FEED_HEADER;
    break;
  default:
    showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
    return STSRV_CLOSE_CONNECTION;
    break;
  }
}

/**
 * @brief Deal with command received GET_FEED_DETAIL
 *        If node belongs to Pub received, send POST_FEED_DETAIL
 *           and go to STSRV_WAIT_CMD_MESSAGE status
 *        Else if does not belong to same Pub
 *           Send CMD_RETURN_MESSAGE with RET_NOT_SAME_PUB
 *              and wait for next command that could be
 *                 GET_FEED_DETAIL or CMD_CLOSE_CONNECTION
 *                 if received GET_FEED_DETAIL, update it 
 * 
 * @param sockFd 
 * @param jsonGetFeedDetail 
 * @return int next Machine State:
 *             STSRV_WAIT_CMD_MESSAGE, if CMD_POST_FEED_DETAIL was sent
 *             STSRV_DEAL_GET_FEED_DETAIL, if does not belong to Pub and 
 *                                         received another GET_FEED_DETAIL
 *                                         command
 *             STSRV_STSRV_CLOSE_CONNECTION if received an unxpected command or
 *                                          if does not belong to Pub and 
 *                                         received CMD_CLOSE_CONNECTION
 */
int stSrvDealGetFeedDetail(int sockFd, json_t *jsonCmd, feedDetail_t *localFeedDetail)
{
  int rc;

  cmdGetFeedDetail_t cmdGetFeedDetail;
  cmdGetFeedDetail_t *ptrCmd = &cmdGetFeedDetail;

  myPub_t myPub;
  myPub_t *ptrMyPub = &myPub;

  feedHeader_t feedHeader;
  feedHeader_t *ptrFeedHeader = &feedHeader;

  cmdReturnMessage_t cmdReturn;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturn;

  cmdPostFeedDetail_t cmdPostFeedDetail;
  cmdPostFeedDetail_t *ptrCmdPostFeedDetail = &cmdPostFeedDetail;

  sqlite3_stmt *pStmt;

  unsigned char nonceSign[NONCE_MSG_SIZE];

  memset(nonceSign, 0, sizeof(nonceSign));

  rc = loadCmdGetFeedDetail(ptrCmd, jsonCmd);
  if (rc != PROTOCOL_OK)
    return STSRV_CLOSE_CONNECTION;

  printf("\tPub   : %s\n", ptrCmd->pubPkB64);
  printf("\tSender: %s\n", ptrCmd->senderPkB64);
  printf("\tTicket: %s\n\n", ptrCmd->pubTicketB64);

  memset(ptrCmdReturn, 0, sizeof(cmdReturn));
  memcpy(ptrCmdReturn->recipientPk, ptrCmd->senderPk, sizeof(cmdReturn.recipientPk));
  cmdReturnMessageBin2B64(ptrCmdReturn);

  memset(ptrMyPub, 0, sizeof(myPub));

  // Verify if the pub affiliation

  memcpy(ptrMyPub->pubPk, ptrCmd->pubPk, sizeof(ptrCmd->pubPk));

  memset(localFeedDetail, 0, sizeof(localFeedDetail));

  rc = prepareMyPubByPub(&pStmt, ptrMyPub);

  if (rc != PERSISTENCE_OK)
    return STSRV_CLOSE_CONNECTION;

  rc = nextMyPub(pStmt, ptrMyPub);
  if (rc != PERSISTENCE_OK)
    return STSRV_CLOSE_CONNECTION;

  if (rc == PERSISTENCE_NO_ROWS)
  {
    cmdReturn.returnCode = RET_NOT_SAME_PUB;
    printf("\tSend Return Message: RET_NOT_SAME_PUB\n");
    rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
    if (rc != PROTOCOL_OK)
      return STSRV_CLOSE_CONNECTION;
    else
      return STSRV_WAIT_CMD_MESSAGE;
  }

  rc = verifySignature(ptrCmd->senderPk, nonceSign, ptrCmd->pubPk, ptrCmd->pubTicket, sizeof(ptrCmd->senderPk));

  if (rc != CRYPTO_OK)
  {
    cmdReturn.returnCode = RET_INVALID_TICKET;
    printf("\tSend Return Message: RET_INVALID_TICKET\n");
    rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
    if (rc != PROTOCOL_OK)
      return STSRV_CLOSE_CONNECTION;
    else
      return STSRV_WAIT_CMD_MESSAGE;
  }

  memcpy(ptrFeedHeader->pubPk, ptrCmd->pubPk, sizeof(ptrCmd->pubPk));
  memcpy(ptrFeedHeader->senderPk, ptrCmd->senderPk, sizeof(ptrCmd->senderPk));
  memcpy(ptrFeedHeader->pubTicket, ptrCmd->pubTicket, sizeof(ptrCmd->pubTicket));

  rc = verifyFeedHeader(ptrFeedHeader);
  if (rc != PERSISTENCE_OK)
    return STSRV_CLOSE_CONNECTION;

  memset(localFeedDetail, 0, sizeof(feedDetail_t));

  memcpy(localFeedDetail->pubPk, ptrCmd->pubPk, sizeof(localFeedDetail->pubPk));
  memcpy(localFeedDetail->senderPk, ptrCmd->senderPk, sizeof(localFeedDetail->senderPk));

  rc = getLastFeedDetail(localFeedDetail);

  if (rc != PERSISTENCE_OK && rc != PERSISTENCE_NO_ROWS)
    return STSRV_CLOSE_CONNECTION;

  memset(ptrCmdPostFeedDetail, 0, sizeof(cmdPostFeedDetail));

  memcpy(ptrCmdPostFeedDetail->pubPk, localFeedDetail->pubPk, sizeof(localFeedDetail->pubPk));
  memcpy(ptrCmdPostFeedDetail->senderPk, localFeedDetail->senderPk, sizeof(localFeedDetail->senderPk));
  memcpy(ptrCmdPostFeedDetail->pubTicket, localFeedDetail->pubTicket, sizeof(localFeedDetail->pubTicket));
  memcpy(ptrCmdPostFeedDetail->lastHASH, localFeedDetail->lastHASH, sizeof(localFeedDetail->lastHASH));
  cmdPostFeedDetail.lastSequence = localFeedDetail->lastSequence;

  cmdPostFeedDetailBin2B64(ptrCmdPostFeedDetail); // TODO Print Command sent

  printf("\tSent command CMD_POST_FEED_DETAIL\n");
  printf("\tPub      : %s\n", ptrCmdPostFeedDetail->pubPkB64);
  printf("\tSender   : %s\n", ptrCmdPostFeedDetail->senderPkB64);
  printf("\tTicket   : %s\n", ptrCmdPostFeedDetail->pubTicketB64);
  printf("\tLast Hash: %s\n", ptrCmdPostFeedDetail->lastHASHB64);
  printf("\tLast Seq : %d\n", ptrCmdPostFeedDetail->lastSequence);

  rc = sendCmdPostFeedDetail(ptrCmdPostFeedDetail, sockFd);

  if (rc != PROTOCOL_OK)
    return STSRV_CLOSE_CONNECTION;
  else
    return STSRV_WAIT_CMD_MESSAGE;
}

/**
 * @brief Deal state machine Wait Cmd Message
 *        Wait one of the command message: Get or Post, that send or ask
 *        for detail message to synchronize 
 * 
 *        It waits for message and analyze the command received
 * 
 * @param sockFd 
 * @param jsonGetFeedDetail 
 * @param jsonGetOrPostMessage 
 * @return int STSRV_DEAL_POST_MESSAGE if received the CMD_POST_MESSAGE
 *             STSRV_DEAL_GET_MESSAGE if received the CMD_GET_MESSAGE
 *             STSRV_DEAL_GET_FEED_DETAIL if received CMD_GET_FEED_DETAIL
 *             STSRV_CLOSE_CONNECTION if received CMD_CLOSE_CONNECTION
 */
int stSrvWaitCmdMessage(int sockFd, json_t **jsonPayLoad)
{
  int rc, command;

  cmdReturnMessage_t cmdReturn;

  rc = receiveJSON(sockFd, jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
    return STSRV_INVALID_COMMAND;
  }

  switch (command)
  {
  case CMD_GET_MESSAGE:
    return STSRV_DEAL_GET_MESSAGE;
    break;
  case CMD_POST_MESSAGE:
    return STSRV_DEAL_POST_MESSAGE;
    break;
  case CMD_GET_FEED_DETAIL:
    return STSRV_DEAL_GET_FEED_DETAIL;
    break;
  case CMD_CLOSE_CONNECTION:
    return STSRV_CLOSE_CONNECTION;
    break;
  case CMD_RETURN_MESSAGE:
    printReceivedCommand(command, 1);
    loadCmdReturnMessage(&cmdReturn, *jsonPayLoad);
    printf("\t\tReturn Code: (%d) %s\n", cmdReturn.returnCode, getStrRetCode(cmdReturn.returnCode));
    return STSRV_WAIT_CMD_MESSAGE;
    break;
  default:
    showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
    return STSRV_CLOSE_CONNECTION;
    break;
  }
}

/**
 * @brief Deal Message Received
 *        If Message is OK Return CMD_RETURN_MESSAGE with RET_OK
 *        Else Return CMD_RETURN_MESSAGE with Error Code
 * 
 * @param sockFd 
 * @param jsonGetOrPostMessage 
 * @return int 
 */
int stSrvDealPostMessage(int sockFd, json_t *jsonPostMessage, feedDetail_t *locaFeedDetail)
{
  int rc, retCode;

  cmdPostMessage_t cmd;
  cmdPostMessage_t *ptrCmd = &cmd;

  cmdReturnMessage_t cmdReturn;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturn;

  msgBlock_t msgBlock;
  msgBlock_t *ptrMsgBlock = &msgBlock;

  feedDetail_t feedDetail;
  feedDetail_t *ptrFeedDetail = &feedDetail;

  char tableName[SIZE_TABLE_NAME];

  rc = loadCmdPostMessage(ptrCmd, jsonPostMessage);
  if (rc != PROTOCOL_OK)
  {
    return STSRV_CLOSE_CONNECTION;
  }
  printf("\tPub          : %s\n", ptrCmd->pubPkB64);
  printf("\tSender       : %s\n", ptrCmd->senderPkB64);
  printf("\tTicket       : %s\n", ptrCmd->pubTicketB64);
  printf("\tpreviousHASH : %s\n", ptrCmd->previousHASHB64);
  printf("\tsequence     : %lu\n", ptrCmd->sequence);
  printf("\trecipient    : %s\n", ptrCmd->recipientPkB64);
  printf("\ttagMsg       : %lu\n", ptrCmd->tagMsg);
  printf("\tnonceMsg     : %s\n", ptrCmd->nonceSignB64);
  printf("\tmessage      : %s\n", ptrCmd->messageB64);
  printf("\tsignature    : %s\n", ptrCmd->signatureB64);
  printf("\tblockHASH    : %s\n", ptrCmd->blockHASHB64);
  printf("\tconfirmations: %lu\n", ptrCmd->confirmations);

  memcpy(ptrMsgBlock->previousHASH, ptrCmd->previousHASH, sizeof(msgBlock.previousHASH));
  msgBlock.sequence = cmd.sequence;
  memcpy(ptrMsgBlock->recipientPk, ptrCmd->recipientPk, sizeof(msgBlock.recipientPk));
  msgBlock.tagMsg = cmd.tagMsg;
  memcpy(ptrMsgBlock->nonceSign, ptrCmd->nonceSign, sizeof(msgBlock.nonceSign));
  memcpy(ptrMsgBlock->message, ptrCmd->message, sizeof(msgBlock.message));
  memcpy(ptrMsgBlock->signature, ptrCmd->signature, sizeof(msgBlock.signature));
  memcpy(ptrMsgBlock->blockHASH, ptrCmd->blockHASH, sizeof(msgBlock.blockHASH));
  msgBlock.confirmations = cmd.confirmations;

  memset(ptrCmdReturn, 0, sizeof(cmdReturnMessage_t));

  memcpy(ptrFeedDetail->pubPk, ptrCmd->pubPk, sizeof(ptrFeedDetail->pubPk));
  memcpy(ptrFeedDetail->senderPk, ptrCmd->senderPk, sizeof(ptrFeedDetail->senderPk));
  ptrFeedDetail->lastSequence = ptrCmd->sequence;

  rc = verifyMsgBlock(ptrMsgBlock, ptrFeedDetail);

  switch (rc)
  {
  case MSG_BLOCK_OK:
    msgBlock.confirmations++;
    feedTableName(tableName, ptrCmd->pubPk, ptrCmd->senderPk);
    insertMsgBlock(tableName, ptrMsgBlock);
    retCode = RET_OK;
    break;
  case MSG_BLOCK_INVALID_SIGNATURE:
    retCode = RET_INVALID_SIGNATURE;
    break;
  case MSG_BLOCK_INVALID_ENCRYPT:
    retCode = RET_INVALID_ENCRYPT;
    break;
  case MSG_BLOCK_INVALID_HASH:
    retCode = RET_INVALID_HASH;
    break;
  case MSG_BLOCK_INVALID_PREVIOUS_HASH:
    retCode = RET_INVALID_PREVIOUS_HASH;
    break;
  case MSG_BLOCK_INVALID_SEQUENCE:
    retCode = RET_INVALID_SEQUENCE;
    break;
  }

  cmdReturn.returnCode = retCode;

  cmdReturnMessageBin2B64(ptrCmdReturn);

  rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command");
    return STSRV_CLOSE_CONNECTION;
  }

  return STSRV_WAIT_CMD_MESSAGE;
}

/**
 * @brief Deal Message Received
 *        If Message Received is OK, send CMD_POST_MESSAGE
 *           If No more messages, send CMD_RETURN_MESSAGE with code
 *                                     RET_NO_NOME_MESSAGES
 *           Wait for CMD_RETURN_MESSAGE with OK or Error Code
 *        If any error
 *           send CMD_RETURN_MESSAGE with error code
 * 
 * @param sockFd 
 * @param jsonGetMessage 
 * @param locaFeedDetail 
 * @return int 
 */
int stSrvDealGetMessage(int sockFd, json_t *jsonGetMessage, feedDetail_t *locaFeedDetail)
{
  int rc, command;

  char *horLine = "------------------------------------------------\n";

  char tableName[SIZE_TABLE_NAME];

  sqlite3_stmt *pStmt;

  json_t *jsonPayLoad;

  cmdPostMessage_t cmdPostMessage;
  cmdPostMessage_t *ptrCmdPost = &cmdPostMessage;

  msgBlock_t msgBlock;
  msgBlock_t *ptrMsgBlock = &msgBlock;

  cmdGetMessage_t cmdGetMessage;
  cmdGetMessage_t *ptrCmdGet = &cmdGetMessage;

  cmdReturnMessage_t cmdReturn;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturn;

  feedDetail_t feedDetailRemote;
  feedDetail_t *ptrFeedDetailRemote = &feedDetailRemote;

  rc = loadCmdGetMessage(ptrCmdGet, jsonGetMessage);
  if (rc != PROTOCOL_OK)
  {
    return STSRV_CLOSE_CONNECTION;
  }

  printf("\tPub          : %s\n", ptrCmdGet->pubPkB64);
  printf("\tSender       : %s\n", ptrCmdGet->senderPkB64);
  printf("\tTicket       : %s\n", ptrCmdGet->pubTicketB64);
  printf("\tsequence     : %lu\n", ptrCmdGet->sequence);

  memcpy(ptrFeedDetailRemote->pubPk, ptrCmdGet->pubPk, sizeof(ptrCmdGet->pubPk));
  memcpy(ptrFeedDetailRemote->senderPk, ptrCmdGet->senderPk, sizeof(ptrCmdGet->senderPk));
  ptrFeedDetailRemote->lastSequence = ptrCmdGet->sequence;

  rc = prepareMsgBlockBySequence(ptrFeedDetailRemote, &pStmt);

  if (rc != PERSISTENCE_OK)
  {
    return STSRV_CLOSE_CONNECTION;
  }
  rc = nextMsgBlock(pStmt, ptrMsgBlock);

  if (rc != PERSISTENCE_OK && rc != PERSISTENCE_NO_ROWS)
  {
    return STSRV_CLOSE_CONNECTION;
  }

  memset(ptrCmdReturn, 0, sizeof(cmdReturnMessage_t));

  if (rc == PERSISTENCE_NO_ROWS) // Didn't find message
    cmdReturn.returnCode = RET_NO_MORE_MSGS;
  else if (ptrCmdGet->sequence != ptrMsgBlock->sequence) // There is message with greater sequence
    cmdReturn.returnCode = RET_INVALID_SEQUENCE;

  if (cmdReturn.returnCode)
  {
    cmdReturnMessageBin2B64(ptrCmdReturn);

    rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
    if (rc != PROTOCOL_OK)
    {
      showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command");
      return STSRV_CLOSE_CONNECTION;
    }
  }
  if (ptrCmdGet->sequence == ptrMsgBlock->sequence)
  {
    memcpy(ptrCmdPost->pubPk, ptrFeedDetailRemote->pubPk, sizeof(ptrCmdPost->pubPk));
    memcpy(ptrCmdPost->senderPk, ptrFeedDetailRemote->senderPk, sizeof(ptrCmdPost->senderPk));
    memcpy(ptrCmdPost->pubTicket, ptrFeedDetailRemote->pubTicket, sizeof(ptrCmdPost->pubTicket));
    memcpy(ptrCmdPost->previousHASH, ptrMsgBlock->previousHASH, sizeof(ptrCmdPost->previousHASH));
    ptrCmdPost->sequence = ptrMsgBlock->sequence;
    memcpy(ptrCmdPost->recipientPk, ptrMsgBlock->recipientPk, sizeof(ptrCmdPost->recipientPk));
    ptrCmdPost->tagMsg = ptrMsgBlock->tagMsg;
    memcpy(ptrCmdPost->nonceSign, ptrMsgBlock->nonceSign, sizeof(ptrCmdPost->nonceSign));
    memcpy(ptrCmdPost->message, ptrMsgBlock->message, sizeof(ptrCmdPost->message));
    memcpy(ptrCmdPost->signature, ptrMsgBlock->signature, sizeof(ptrCmdPost->signature));
    memcpy(ptrCmdPost->blockHASH, ptrMsgBlock->blockHASH, sizeof(ptrCmdPost->blockHASH));
    ptrCmdPost->confirmations = ptrMsgBlock->confirmations;

    cmdPostMessageBin2B64(ptrCmdPost);

    rc = sendCmdPostMessage(ptrCmdPost, sockFd);
    if (rc != PROTOCOL_OK)
      return STSRV_CLOSE_CONNECTION;

    rc = receiveJSON(sockFd, &jsonPayLoad, &command);
    if (rc != PROTOCOL_OK)
    {
      showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
      return STSRV_CLOSE_CONNECTION;
    }

    printReceivedCommand(command, 0);

    if (command != CMD_RETURN_MESSAGE)
    {
      showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
      return STSRV_CLOSE_CONNECTION;
    }
    loadCmdReturnMessage(&cmdReturn, jsonPayLoad); // TODO create a function to return string of code

    printf("\tReturn Code: (%d) %s\n", cmdReturn.returnCode, getStrRetCode(cmdReturn.returnCode));

    if (cmdReturn.returnCode == RET_OK)
    {
      msgBlock.confirmations++;
      feedTableName(tableName, ptrCmdPost->pubPk, ptrCmdPost->senderPk);
      updateMsgBlock(tableName, ptrMsgBlock);
    }
  }
  return STSRV_WAIT_CMD_MESSAGE;
}
