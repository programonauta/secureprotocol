/**
 * @file cmdPostMessage.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 25-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef CMDPOSTMESSAGE_H
#define CMDPOSTMESSAGE_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
    unsigned char pubTicket[SIGNATURE_SIZE];
    unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
    unsigned char previousHASH[HASH_SIZE];
    unsigned char previousHASHB64[HASH_SIZE_B64];
    unsigned long sequence;
    unsigned char recipientPk[PUBLICKEY_SIZE];
    unsigned char recipientPkB64[PUBLICKEY_SIZE_B64];
    unsigned char tagMsg;
    unsigned char nonceSign[NONCE_MSG_SIZE];
    unsigned char nonceSignB64[NONCE_MSG_SIZE_B64];
    unsigned char message[MESSAGE_SIZE];
    unsigned char messageB64[MESSAGE_SIZE_B64];
    unsigned char signature[SIGNATURE_SIZE];
    unsigned char signatureB64[SIGNATURE_SIZE_B64];
    unsigned char blockHASH[HASH_SIZE];
    unsigned char blockHASHB64[HASH_SIZE_B64];
    unsigned long confirmations;
} cmdPostMessage_t;

void cmdPostMessageBin2B64(cmdPostMessage_t *cmd);
void cmdPostMessageB642Bin(cmdPostMessage_t *cmd);

int sendCmdPostMessage(cmdPostMessage_t *cmd, int sockFd);
int loadCmdPostMessage(cmdPostMessage_t *cmd, json_t *jsonPayLoad);

#endif