/**
 * @file protocol.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 17-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "protocol.h"

/**
 * @brief Get the Peer IP address
 * 
 * @param sockFd 
 * @param strIP 
 */
void getPeerIP(int sockFd, char *strIP)
{
  struct sockaddr_in addr;
  socklen_t addr_size = sizeof(struct sockaddr_in);
  if (getpeername(sockFd, (struct sockaddr *)&addr, &addr_size) == 0)
    strcpy(strIP, inet_ntoa(addr.sin_addr));
  else
    strIP = NULL;

  return;
}

const char *getStrProtocolRetCode(int protRetCode)
{
  switch (protRetCode)
  {
  case PROTOCOL_OK:
    return "OK";
  case PROTOCOL_FAIL:
    return "Fail";
  case PROTOCOL_NOT_JSON:
    return "Not a Json object";
  case PROTOCOL_NOT_KEY:
    return "Not a Key";
  case PROTOCOL_NO_MORE_FEED:
    return "No more Feed";
  case PROTOCOL_CLOSED:
    return "Communication Closed";
  default:
    return "UNKNOWN CODE";
  }
}

void showProtocolMsg(int msgType, const char *funcName, int codeMsg, char *customMsg)
{
  fprintf(stderr, "[%s] PROTOCOL %s: (%d) %s\n\t%s\n",
          funcName,
          msgType == MSG_ERROR ? "ERROR" : msgType == MSG_WARNING ? "WARNING"
                                                                  : "MESSAGE",
          codeMsg,
          getStrProtocolRetCode(codeMsg),
          customMsg);
  return;
}