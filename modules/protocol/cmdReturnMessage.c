/**
 * @file cmdReturnMessage.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 02-Jul-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "cmdReturnMessage.h"

void cmdReturnMessageBin2B64(cmdReturnMessage_t *cmd)
{
    bin2b64(cmd->senderPkB64, sizeof(cmd->senderPkB64), cmd->senderPk, sizeof(cmd->senderPk));
    bin2b64(cmd->recipientPkB64, sizeof(cmd->recipientPkB64), cmd->recipientPk, sizeof(cmd->recipientPk));
}

void cmdReturnMessageB642Bin(cmdReturnMessage_t *cmd)
{
    b642bin(cmd->senderPk, sizeof(cmd->senderPk), cmd->senderPkB64, sizeof(cmd->senderPkB64));
    b642bin(cmd->recipientPk, sizeof(cmd->recipientPk), cmd->recipientPkB64, sizeof(cmd->recipientPkB64));
}

int sendCmdReturnMessage(cmdReturnMessage_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];
    char *horLine = "------------------------------------------------\n";

    node_t node;
    int rc;

    rc = readNode(&node);

    memset(msgSent, 0, strlen(msgSent));

    sprintf(msgSent, "{\"command\": %d, "
                     "\"sender\": \"%s\", "
                     "\"recipient\": \"%s\", "
                     "\"returnCode\": %d }",
            CMD_RETURN_MESSAGE, node.pkB64, cmd->recipientPkB64, cmd->returnCode);

    printf("\n%sCommand Sent: CMD_RETURN_MESSAGE\n", horLine);
    printf("\tReturn Code: (%d) %s\n", cmd->returnCode, getStrRetCode(cmd->returnCode));

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

int loadCmdReturnMessage(cmdReturnMessage_t *cmd, json_t *jsonPayLoad)
{
    int errCode;
    memset(cmd, 0, sizeof(cmdReturnMessage_t));
    strcpy(cmd->senderPkB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->recipientPkB64, jsonGetString(jsonPayLoad, "recipient", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmd->returnCode = jsonGetInt(jsonPayLoad, "returnCode", &errCode);
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmdReturnMessageB642Bin(cmd);
    return PROTOCOL_OK;
}

/**
 * @brief Get string of command
 * 
 * @param command 
 * @return const char* 
 */
const char *getStrRetCode(int retCode)
{
  switch (retCode)
  {

  case RET_OK:
    return TEXT_GREEN"RET_OK"COLOR_RESET;
  case RET_INVALID_COMMAND:
    return TEXT_RED"RET_INVALID_COMMAND"COLOR_RESET;
  case RET_INVALID_SIGNATURE:
    return TEXT_RED"RET_INVALID_SIGNATURE"COLOR_RESET;
  case RET_INVALID_ENCRYPT:
    return TEXT_RED"RET_INVALID_ENCRYPT"COLOR_RESET;
  case RET_INVALID_HASH:
    return TEXT_RED"RET_INVALID_HASH"COLOR_RESET;
  case RET_INVALID_PREVIOUS_HASH:
    return TEXT_RED"RET_INVALID_HASH"COLOR_RESET;
  case RET_INVALID_SEQUENCE:
    return TEXT_RED"RET_INVALID_SEQUENCE"COLOR_RESET;
  case RET_INVALID_TICKET:
    return TEXT_RED"RET_INVALID_TICKET"COLOR_RESET;
  case RET_NOT_SAME_PUB:
    return TEXT_GREEN"RET_NOT_SAME_PUB"COLOR_RESET;
  case RET_NO_MORE_FEEDS:
    return TEXT_GREEN"RET_NO_MORE_FEEDS"COLOR_RESET;
  case RET_NO_MORE_MSGS:
    return TEXT_GREEN"RET_NO_MORE_MSGS"COLOR_RESET;
  case RET_INVITE_GRANTED:
    return TEXT_GREEN"RET_INVITE_GRANTED"COLOR_RESET;
  case RET_INVITE_ERR_NOT_FOUND:
    return TEXT_RED"RET_INVITE_ERR_NOT_FOUND"COLOR_RESET;
  case RET_INVITE_ERR_OTHER_NODE:
    return TEXT_RED"RET_INVITE_ERR_OTHER_NODE"COLOR_RESET;
  case RET_INVITE_ALREADY_GRANTED:
    return TEXT_RED"RET_INVITE_ALREADY_GRANTED"COLOR_RESET;
  default:
    return TEXT_RED"UNKNOWN CODE"COLOR_RESET;
  }
}
