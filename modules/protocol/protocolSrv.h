#ifndef PROTOCOL_SRV_H
#define PROTOCOL_SRV_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "protocol.h"
#include "protocolCmds.h"
#include "cmdGetTicket.h"
#include "cmdPostTicket.h"
#include "cmdReturnMessage.h"
#include "cmdGetFeedHeader.h"
#include "cmdPostFeedHeader.h"
#include "cmdGetFeedDetail.h"
#include "cmdPostFeedDetail.h"
#include "cmdGetMessage.h"
#include "cmdPostMessage.h"

enum stateProtocolCli
{
  STSRV_WAIT_COMMAND,
  STSRV_INVITE_RECEIVED,
  STSRV_INVALID_COMMAND,
  STSRV_INVALID_SIGNATURE,
  STSRV_VERIFY_INVITE,
  STSRV_POST_TICKET,
  STSRV_INVALID_INVITE,
  STSRV_UPDATE_INVITE,
  STSRV_DEAL_GET_FEED_HEADER,
  STSRV_DEAL_GET_FEED_DETAIL,
  STSRV_DEAL_RETURN,
  STSRV_WAIT_CMD_MESSAGE,
  STSRV_DEAL_POST_MESSAGE,
  STSRV_DEAL_GET_MESSAGE,
  STSRV_CLOSE_CONNECTION,
  STSRV_END
};


// Connection Functions
struct addrinfo* bindServer(char* port, int* pSockFd, int backlog);
void *getInAddr(struct sockaddr *sa);

// Functions to deal with communication protocol
int protocolSrv(int sockFd);

// Functions to manage state machine

int stSrvWaitCommand(int sockFd, json_t **jsonPayLoad);
int stSrvInviteReceived(int sockFd, json_t *jsonPayLoad, cmdGetTicket_t *cmdGetTicket);
int stSrvInvalidCommand(int sockFd, cmdGetTicket_t *cmdGetTicket ,int resultCommand);
int stSrvVerifyInvite(int sockFd, cmdGetTicket_t *cmdGetTicket);
int stSrvPostTicket(int sockFd, cmdGetTicket_t *cmdGetTicket);
int stSrvInvalidInvite(int sockFd, cmdGetTicket_t *cmdGetTicket , int resultInvite); 
int stSrvUpdateInvite(cmdGetTicket_t *cmdGetTicket, int inviteStatus);

int stSrvDealGetFeedHeader(int sockFd, json_t *jsonGetFeedHeader, json_t **jsonPayLoad, cmdGetFeedHeader_t *cmdGetFeedHeader);
int stSrvDealReturn(int sockFd, json_t *jsonGetReturnMsg, json_t **jsonPayLoad);
int stSrvDealGetFeedDetail(int sockFd, json_t *jsonCmd, feedDetail_t *localFeedDetail);
int stSrvWaitCmdMessage(int sockFd, json_t **jsonPayLoad);
int stSrvDealPostMessage(int sockFd, json_t *jsonPostMessage, feedDetail_t *locaFeedDetail);
int stSrvDealGetMessage(int sockFd, json_t *jsonGetMessage, feedDetail_t *locaFeedDetail);

#endif
