#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <err.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "persistence.h"
#include "cryptography.h"
#include "dataStructure.h"
#include "json.h"
#include "printColors.h"

enum ipFamily
{
  ipFamilyIpv4 = AF_INET,
  ipFamilyIpv6 = AF_INET6,
  ipFamilyAny = AF_UNSPEC
};

// Return Codes
enum protocolReturns
{
    PROTOCOL_OK,
    PROTOCOL_FAIL,
    PROTOCOL_NOT_JSON,
    PROTOCOL_NOT_KEY,
    PROTOCOL_NO_MORE_FEED,
    PROTOCOL_CLOSED
};

void getPeerIP(int sockFd, char* strIP);

const char *getStrProtocolRetCode(int protRetCode);
void showProtocolMsg(int msgType, const char *funcName, int codeMsg, char *customMsg);

#endif
