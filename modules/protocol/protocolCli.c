/**
 * @file protocolCli.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Protcol Functions of client side
 * @version 0.1
 * @date 10-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "protocolCli.h"

/**
 * @brief Establishes a connection with the <host>, using port <port>
 * 
 * @param host - IP Adress to connect 
 * @param port - Connection port
 * @param ipFamily - IP Family
 * @return int - File Description if connection was established, or a number less than zero
 * if any error ocurred
 */
int tcpConnect(const char *host, const char *port, enum ipFamily ipFamily)
{
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  int s;
  int fd;
  int err;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = ipFamily;
  hints.ai_protocol = IPPROTO_TCP;

  s = getaddrinfo(host, port, &hints, &result);
  if (s < 0)
    errx(1, "unable to resolve host: %s", gai_strerror(s));

  for (rp = result; rp; rp = rp->ai_next)
  {
    fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (fd < 0)
      continue;
    if (connect(fd, rp->ai_addr, rp->ai_addrlen) == 0)
      break;
    err = errno;
    close(fd);
    errno = err;
  }
  if (rp == NULL)
    fd = -1;

  freeaddrinfo(result);

  return fd;
}

/**
 * @brief Manage State Machine of Get Ticket process
 * 
 * @param node 
 * @param myPub 
 * @param sockFd 
 * @return int 
 *      PROTOCOL_OK if got the ticket
 *      PROTOCOL_FAIL if not
 */
int protocolGetTicket(node_t *node, myPub_t *myPub, int sockFd)
{
  cmdGetTicket_t cmdGetTicket;
  cmdGetTicket_t *ptrCmdGetTicket = &cmdGetTicket;

  cmdPostTicket_t cmdPostTicket;
  cmdPostTicket_t *ptrCmdPostTicket = &cmdPostTicket;

  char *horLine = "------------------------------------------------\n";

  int rc, commandReceived, retCode, inviteStatus;
  json_t *jsonPayLoad = NULL;

  int currentState = STCLI_GET_TICKET;

  /**
   * @brief Populate cmdGetTicket structure
   * 
   */
  memset(ptrCmdGetTicket, 0, sizeof(cmdGetTicket));
  memcpy(cmdGetTicket.sender, node->pk, sizeof(node->pk));
  memcpy(cmdGetTicket.pubPk, myPub->pubPk, sizeof(myPub->pubPk));
  sprintf(ptrCmdGetTicket->flatInvite, "Invite:%s", myPub->pubInvite);
  createNonceEncrypt(ptrCmdGetTicket->nonceEncrypt);
  createNonceSign(ptrCmdGetTicket->nonceSign);
  encryptInvite(ptrCmdGetTicket, node);
  signCmdGetTicket(ptrCmdGetTicket, node);

  /**
   * @brief Populate all Base64 fields
   * 
   */
  cmdGetTicketBin2B64(ptrCmdGetTicket);

  while (currentState != STCLI_END)
  {
    switch (currentState)
    {
    case STCLI_GET_TICKET:
      printf("%s", horLine);
      commandReceived = stCliGetTicket(sockFd, ptrCmdGetTicket, &jsonPayLoad);
      switch (commandReceived)
      {
      case CMD_POST_TICKET:
        currentState = STCLI_DEAL_POSTTICKET;
        break;
      case CMD_RETURN_MESSAGE:
        currentState = STCLI_DEAL_RETURN;
        break;
      default:
        showProtocolMsg(MSG_ERROR, __func__, commandReceived, "Unexpected Command received");
        retCode = RET_INVALID_COMMAND;
        currentState = STCLI_SEND_RETURN;
        break;
      }
      break;
    case STCLI_DEAL_POSTTICKET:
      retCode = stCliDealPostTicket(ptrCmdPostTicket, jsonPayLoad);
      currentState = STCLI_SEND_RETURN;
      break;
    case STCLI_SEND_RETURN:
      currentState = stCliSendReturn(sockFd, retCode, ptrCmdGetTicket->pubPk, ptrCmdGetTicket->sender);
      break;
    case STCLI_DEAL_RETURN:
      retCode = stCliDealReturn(jsonPayLoad);
      printReceivedCommand(CMD_RETURN_MESSAGE, 0);
      printf("\tReturn Code: (%d) %s\n", retCode, getStrRetCode(retCode));
      switch (retCode)
      {
      case RET_INVALID_COMMAND:
      case RET_INVALID_SIGNATURE:
        currentState = STCLI_END;
        break;
      case RET_INVITE_GRANTED:
      case RET_INVALID_ENCRYPT:
      case RET_INVITE_ERR_NOT_FOUND:
      case RET_INVITE_ERR_OTHER_NODE:
        currentState = STCLI_UPDATE_MYPUB;
        break;
      default:
        currentState = STCLI_END;
      }
      break;
    case STCLI_UPDATE_MYPUB:
      switch (retCode)
      {
      case RET_OK:
        inviteStatus = INVITE_GRANTED;
        break;
      case RET_INVITE_ERR_NOT_FOUND:
        inviteStatus = INVITE_DENIED_NOT_FOUND;
        break;
      case RET_INVALID_ENCRYPT:
        inviteStatus = INVITE_DENIED_ENCRYPT;
      case RET_INVITE_ERR_OTHER_NODE:
        inviteStatus = INVITE_DENIED_OTHER_NODE;
        break;
      default:
        inviteStatus = INVITE_DENIED_NOT_FOUND;
        break;
      }
      currentState = stCliUpdateMyPub(inviteStatus, myPub->pubInvite, ptrCmdPostTicket->ticket);
      break;
    default:
      currentState = STCLI_END;
      break;
    }
  }
  printf("Closing Connection\n%s", horLine);
  return PROTOCOL_OK;
}

/**
 * @brief Manage State Machine of Get Ticket process
 * 
 * @param node 
 * @param sockFd 
 * @return int 
 */
int protocolSyncMsgs(node_t *node, int sockFd)
{

  cmdGetFeedHeader_t cmdGetFeedHeader;
  cmdGetFeedHeader_t *ptrCmdGetFeedHeader = &cmdGetFeedHeader;

  cmdPostFeedHeader_t cmdPostFeedHeader;
  cmdPostFeedHeader_t *ptrCmdPostFeedHeader = &cmdPostFeedHeader;

  feedDetail_t feedDetailRemote;
  feedDetail_t *ptrFeedDetailRemote = &feedDetailRemote;

  memset(ptrFeedDetailRemote, 0, sizeof(feedDetailRemote));

  int currentState = STCLI_SYNC_MSGS;
  int retCode;
  int commandReceived;

  memset(ptrCmdGetFeedHeader, 0, sizeof(cmdGetFeedHeader));

  memcpy(ptrCmdGetFeedHeader->senderPk, node->pk, sizeof(node->pk));
  cmdGetFeedHeaderBin2B64(ptrCmdGetFeedHeader);

  json_t *jsonPayLoad = NULL;

  char *horLine = "------------------------------------------------\n";

  while (currentState != STCLI_END)
  {
    switch (currentState)
    {
    case STCLI_SYNC_MSGS:

      commandReceived = stCliSyncMsgs(sockFd, ptrCmdGetFeedHeader, &jsonPayLoad);
      printReceivedCommand(commandReceived, 0);
      switch (commandReceived)
      {
      case CMD_POST_FEED_HEADER:
        currentState = STCLI_DEAL_POST_FEED_HEADER;
        break;
      case CMD_RETURN_MESSAGE:
        currentState = STCLI_DEAL_RETURN;
        break;
      case -1: // Error on Receive Message
        currentState = STCLI_END;
        break;
      default:
        showProtocolMsg(MSG_ERROR, __func__, commandReceived, "Unexpected Command received");
        retCode = RET_INVALID_COMMAND;
        currentState = STCLI_SEND_RETURN;
        break;
      }
      break;
    case STCLI_DEAL_POST_FEED_HEADER:
      currentState = stCliDealPostFeedHeader(ptrCmdPostFeedHeader, jsonPayLoad, &retCode);
      break;
    case STCLI_SYNC_MSGBLOCK:
      currentState = stCliSyncMsgBlock(sockFd, jsonPayLoad, &feedDetailRemote);
      break;
    case STCLI_SEND_RETURN:
      currentState = stCliSendReturn(sockFd, retCode, NULL, NULL);
      break;
    case STCLI_DEAL_RETURN:
      retCode = stCliDealReturn(jsonPayLoad);
      switch (retCode)
      {
      case RET_NO_MORE_FEEDS:
      case RET_NOT_SAME_PUB:
        currentState = STCLI_SYNC_MSGBLOCK;
        break;
      case RET_INVALID_COMMAND:
      case RET_INVALID_SIGNATURE:
      default:
        currentState = STCLI_END;
      }
      break;
    case STCLI_POST_MESSAGE:
      currentState = stCliPostMessage(sockFd, jsonPayLoad, &feedDetailRemote);
      break;
    case STCLI_GET_MESSAGE:
      currentState = stCliGetMessage(sockFd, jsonPayLoad, &feedDetailRemote);
      break;
    default:
      currentState = STCLI_END;
    }
  }

  return PROTOCOL_OK;
}

/**
 * @brief Deal State STCLI_GET_TICKET
 * 
 * @param sockFd 
 * @param ptrCmdGetTicket 
 * @param jsonPayLoad 
 * @return int Command returned by server (POST_TICKET or RETURN_MESSAGE) 
 *                     or -1 if any error
 */
int stCliGetTicket(int sockFd, cmdGetTicket_t *ptrCmdGetTicket, json_t **jsonPayLoad)
{
  int rc, command;

  rc = sendCmdGetTicket(ptrCmdGetTicket, sockFd);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command GET_TICKET");
    return -1;
  }

  rc = receiveJSON(sockFd, jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON on STCLI_GET_TICKET state");
    return -1;
  }

  return command;
}

/**
 * @brief Analyse The ticket received 
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int The return code to be sent back to server
 *        could be: RET_OK if Command is ok and signature is valid
 *                  RET_INVALID_SIGNATURE 
 *                  RET_INVALID_COMMAND
 */
int stCliDealPostTicket(cmdPostTicket_t *cmd, json_t *jsonPayLoad)
{
  int rc = loadCmdPostTicket(cmd, jsonPayLoad);

  if (rc != PROTOCOL_OK)
    return RET_INVALID_COMMAND;

  rc = verifySignCmdPostTicket(cmd);

  printf("Ticket Received from server\n");
  if (rc != PROTOCOL_OK)
    printf("Invalid Signature\n");

  return (rc != PROTOCOL_OK ? RET_INVALID_SIGNATURE : RET_OK);
}

/**
 * @brief Send Command CMD_RETURN_MSG to oht
 * 
 * @param sockFd 
 * @param retCode 
 * @return int Return the next State
 *             STCLI_UPDATE_MYPUB if invite was granted
 *             STCLI_SYNC_MSGS if Invalid ticket is received
 *             STCLI_END if Invalid Signature or Received an expected command
 */
int stCliSendReturn(int sockFd, int retCode, unsigned char *pubPk, unsigned char *sender)
{
  cmdReturnMessage_t cmdReturnMessage;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturnMessage;

  int rc;

  memset(ptrCmdReturn, 0, sizeof(cmdReturnMessage_t));

  if (sender != NULL)
    memcpy(ptrCmdReturn->senderPk, sender, sizeof(ptrCmdReturn->senderPk));
  if (pubPk != NULL)
    memcpy(ptrCmdReturn->recipientPk, pubPk, sizeof(ptrCmdReturn->recipientPk));
  ptrCmdReturn->returnCode = retCode;

  cmdReturnMessageBin2B64(ptrCmdReturn);

  rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command");
    return STCLI_END;
  }

  printf("Send Return to Server: ");
  switch (retCode)
  {
  case RET_OK:
    printf("Ticket received OK\n");
    return STCLI_UPDATE_MYPUB;
    break;
  case RET_INVALID_SIGNATURE:
    printf("Invalid ticekt (signature) received from server\n");
    break;
  case RET_INVALID_COMMAND:
    printf("Invalid Command received from server\n");
    break;
  case RET_INVALID_TICKET:
    printf("Invalid Ticket received from GET_FEED_HEADER\n");
    return STCLI_SYNC_MSGS;
  case RET_NOT_SAME_PUB:
    printf("RET_NOT_SAME_PUB\n");
    return STCLI_SYNC_MSGS;

  default:
    printf("Unkown return Code received\n");
    return STCLI_END;
  }
  return STCLI_END;
}

/**
 * @brief Deal the Command CMD_RETURN_MESSAGE received from server
 *        If receive this command the invite sent was not accepted by server
 *        or there was any problem with command or signature
 * 
 * @param jsonPayLoad 
 * @return int The code received in the command
 */
int stCliDealReturn(json_t *jsonPayLoad)
{
  int retCodeReceived;
  cmdReturnMessage_t cmdReturn;

  loadCmdReturnMessage(&cmdReturn, jsonPayLoad); 

  printf("\tReturn Code: (%d) %s\n", cmdReturn.returnCode, getStrRetCode(cmdReturn.returnCode));

  retCodeReceived = cmdReturn.returnCode;

  switch (retCodeReceived)
  {
  case RET_INVALID_TICKET:
    printf("RET_INVALID_TICKET");
    break;
  case RET_NO_MORE_FEEDS:
    printf("RET_NO_MORE_FEEDS");
    break;
  case RET_INVALID_COMMAND:
    printf("RET_INVALID_COMMAND");
    break;
  default:
    printf("Unexpected Command");
  }

  printf("\n");

  return retCodeReceived;
}

/**
 * @brief Update Database with ticket received by Pub
 * 
 * @param ptrCmd 
 * @param pubInvite 
 * @return int Next state. Normally STCLI_END
 */
int stCliUpdateMyPub(int inviteStatus, unsigned char *pubInvite, unsigned char *pubTicket)
{
  myPub_t myPub;
  myPub_t *ptrMyPub = &myPub;

  memcpy(ptrMyPub->pubInvite, pubInvite, sizeof(ptrMyPub->pubInvite));
  memcpy(ptrMyPub->pubTicket, pubTicket, sizeof(ptrMyPub->pubTicket));
  myPub.inviteStatus = inviteStatus;

  printf("Invite is updated with status: %s\n", getStrInviteStatus(inviteStatus));

  int rc = updateMyPub(ptrMyPub);

  return STCLI_END;
}

/**
 * @brief Deal state STCLI_SYNC_MSGS
 * 
 * @param sockFd 
 * @param ptrCmdGetTicket 
 * @param jsonPayLoad 
 * @return int command Received
 */

int stCliSyncMsgs(int sockFd, cmdGetFeedHeader_t *ptrCmd, json_t **jsonPayLoad)
{
  int rc, command;

  rc = sendCmdGetFeedHeader(ptrCmd, sockFd);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command GET_FEED_HEADER");
    return -1;
  }

  rc = receiveJSON(sockFd, jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON on STCLI_SYNC_MSGS state");
    return -1;
  }

  return command;
}

/**
 * @brief For each pair Sender x Pub, verifiy if node is affiliated 
 *        to the Pub.
 *        If yes, create (or not) the table for store messages
 * @param cmd 
 * @param jsonPayLoad 
 * @return int next State: STCLI_SYNC_MSGS (there is more feed Header)
 *                         STCLI_GET_FEED_DETAIL (Know the sync point)
 */
int stCliDealPostFeedHeader(cmdPostFeedHeader_t *cmd, json_t *jsonPayLoad, int *retCode)
{
  int rc;
  unsigned char nonceSign[NONCE_MSG_SIZE];

  myPub_t myPub;
  myPub_t *ptrMyPub = &myPub;

  feedHeader_t feedHeader;
  feedHeader_t *ptrFeedHeader = &feedHeader;

  sqlite3_stmt *pStmt;

  memset(ptrMyPub, 0, sizeof(myPub));

  rc = loadCmdPostFeedHeader(cmd, jsonPayLoad);
  if (rc != PROTOCOL_OK)
  {
    return STCLI_END;
  }
  printf("\tPub   : %s\n", cmd->pubPkB64);
  printf("\tSender: %s\n", cmd->senderPkB64);
  printf("\tTicket: %s\n\n", cmd->pubTicketB64);

  memset(nonceSign, 0, sizeof(nonceSign));

  rc = verifySignature(cmd->senderPk, nonceSign, cmd->pubPk, cmd->pubTicket, sizeof(cmd->senderPk));

  if (rc != CRYPTO_OK)
  {
    *retCode = RET_INVALID_TICKET;
    return STCLI_SEND_RETURN;
  }

  // Verify if the pub affiliation

  memcpy(ptrMyPub->pubPk, cmd->pubPk, sizeof(cmd->pubPk));

  rc = prepareMyPubByPub(&pStmt, ptrMyPub);
  if (rc != PERSISTENCE_OK)
    return STCLI_END;

  rc = nextMyPub(pStmt, ptrMyPub);
  if (rc != PERSISTENCE_OK && rc != PERSISTENCE_NO_ROWS)
    return STCLI_END;

  if (rc == PERSISTENCE_NO_ROWS)
  {
    *retCode = RET_NOT_SAME_PUB;
    return STCLI_SEND_RETURN;
  }

  memcpy(ptrFeedHeader->pubPk, cmd->pubPk, sizeof(cmd->pubPk));
  memcpy(ptrFeedHeader->senderPk, cmd->senderPk, sizeof(cmd->senderPk));
  memcpy(ptrFeedHeader->pubTicket, cmd->pubTicket, sizeof(cmd->pubTicket));

  rc = verifyFeedHeader(ptrFeedHeader);

  if (rc != PERSISTENCE_OK)
    return STCLI_END;

  return STCLI_SYNC_MSGS;
}

/**
 * @brief This function deals state STCLI_SYNC_MSGBLOCK
 *        For each Feed Header first send CMD_GET_FEED_DEAIL comand
 *            It receives CMD_POST_FEED_DETAIL or CMD_RETURN_MESSAGE
 *            Verify the integrity 
 *            Analyse the sequence of server and synchronize messages 
 *              sending msgs (CMD_POST_MESSAGE) or receiving msgs (CMD_GET_MESSAGE)
 *            If receive an error (after CMD_POST_MESSAGE) 
 *            or detect an error (after CMD_GET_MESSAGE) move to next Feed Header 
 * 
 * @param SockFd 
 * @return int Next Status, and the feedDetail filled with last command received
 */
int stCliSyncMsgBlock(int sockFd, json_t *jsonPayLoad, feedDetail_t *feedDetailRemote)
{
  int rc, command;
  static sqlite3_stmt *pStmt = NULL;

  unsigned char nonceSign[NONCE_MSG_SIZE];

  feedHeader_t feedHeader;
  feedHeader_t *ptrFeedHeader = &feedHeader;

  cmdGetFeedDetail_t cmdGetFeedDetail;
  cmdGetFeedDetail_t *ptrCmdGet = &cmdGetFeedDetail;

  cmdReturnMessage_t cmdReturn;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturn;

  cmdPostFeedDetail_t cmdPost;
  cmdPostFeedDetail_t *ptrCmdPost = &cmdPost;

  feedDetail_t feedDetLoc; // Local Feed detail
  feedDetail_t *ptrFeedDetLoc = &feedDetLoc;

  char *horLine = "------------------------------------------------\n";

  if (pStmt == NULL)
  {
    rc = prepareFeedHeader(&pStmt);

    if (rc != PERSISTENCE_OK)
    {
      pStmt = NULL;
      return STCLI_END;
    }
  }
  rc = nextFeedHeader(pStmt, ptrFeedHeader);

  switch (rc)
  {
  case (PERSISTENCE_OK):
    // Populate Get Feed Detail
    memcpy(ptrCmdGet->pubPk, ptrFeedHeader->pubPk, sizeof(ptrCmdGet->pubPk));
    memcpy(ptrCmdGet->senderPk, ptrFeedHeader->senderPk, sizeof(ptrCmdGet->senderPk));
    memcpy(ptrCmdGet->pubTicket, ptrFeedHeader->pubTicket, sizeof(ptrCmdGet->pubTicket));
    cmdGetFeedDetailBin2B64(ptrCmdGet);
    rc = sendCmdGetFeedDetail(ptrCmdGet, sockFd);
    if (rc != PROTOCOL_OK)
      return STCLI_END;
    break;
  case PERSISTENCE_NO_ROWS:
  case PERSISTENCE_ERR_READ:
  default:
    pStmt = NULL;
    return STCLI_END;
  }

  rc = receiveJSON(sockFd, &jsonPayLoad, &command);
  if (rc != PROTOCOL_OK)
  {
    showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
    return STCLI_END;
  }

  printReceivedCommand(command, 0);

  switch (command)
  {
  case CMD_POST_FEED_DETAIL:
    break;
  case CMD_RETURN_MESSAGE:
    return STCLI_DEAL_RETURN;
    break;
  default:
    showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
    return STCLI_END;
    break;
  }

  memset(ptrCmdPost, 0, sizeof(cmdPost));
  rc = loadCmdPostFeedDetail(ptrCmdPost, jsonPayLoad);
  if (rc == PROTOCOL_FAIL)
    return STCLI_END;

  printf("\tPub      : %s\n", ptrCmdPost->pubPkB64);
  printf("\tSender   : %s\n", ptrCmdPost->senderPkB64);
  printf("\tTicket   : %s\n", ptrCmdPost->pubTicketB64);
  printf("\tLast Hash: %s\n", ptrCmdPost->lastHASHB64);
  printf("\tLast Seq : %lu\n", ptrCmdPost->lastSequence);

  memset(nonceSign, 0, sizeof(nonceSign));

  rc = verifySignature(ptrCmdPost->senderPk,
                       nonceSign,
                       ptrCmdPost->pubPk,
                       ptrCmdPost->pubTicket,
                       sizeof(ptrCmdPost->senderPk));

  if (rc != CRYPTO_OK)
  {
    memset(ptrCmdReturn, 0, sizeof(cmdReturn));
    ptrCmdReturn->returnCode = RET_INVALID_TICKET;

    rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
    return STCLI_SYNC_MSGBLOCK;
  }

  memset(feedDetailRemote, 0, sizeof(feedDetailRemote));
  memset(ptrFeedDetLoc, 0, sizeof(feedDetLoc));

  memcpy(feedDetailRemote->pubPk, ptrCmdPost->pubPk, sizeof(feedDetailRemote->pubPk));
  memcpy(feedDetailRemote->senderPk, ptrCmdPost->senderPk, sizeof(feedDetailRemote->senderPk));
  memcpy(feedDetailRemote->pubTicket, ptrCmdPost->pubTicket, sizeof(feedDetailRemote->pubTicket));
  feedDetailRemote->lastSequence = ptrCmdPost->lastSequence;
  memcpy(feedDetailRemote->lastHASH, ptrCmdPost->lastHASH, sizeof(feedDetailRemote->lastHASH));

  memcpy(ptrFeedDetLoc->pubPk, ptrCmdPost->pubPk, sizeof(cmdPost.pubPk));
  memcpy(ptrFeedDetLoc->senderPk, ptrCmdPost->senderPk, sizeof(cmdPost.senderPk));

  rc = getLastFeedDetail(ptrFeedDetLoc);
  if (rc != PERSISTENCE_OK && rc != PERSISTENCE_NO_ROWS)
    return STCLI_END;

  if (ptrFeedDetLoc->lastSequence == feedDetailRemote->lastSequence)
  {
    if (memcmp(ptrFeedDetLoc->lastHASH, feedDetailRemote->lastHASH, sizeof(feedDetailRemote->lastHASH)) != 0)
    {
      memset(ptrCmdReturn, 0, sizeof(cmdReturn));
      ptrCmdReturn->returnCode = RET_INVALID_HASH;

      rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
      return STCLI_SYNC_MSGBLOCK;
    }

    return STCLI_SYNC_MSGBLOCK;
  }
  else if (ptrFeedDetLoc->lastSequence > feedDetailRemote->lastSequence)
    return STCLI_POST_MESSAGE;
  else
    return STCLI_GET_MESSAGE;

  return STCLI_END;
}

/**
 * @brief Send command CMD_POST_MESSAGE from sequence received in feedDetail
 * 
 * @param sockFd 
 * @param jsonPayLoad 
 * @param feedDetailRemote 
 * @return int Next Status
 */
int stCliPostMessage(int sockFd, json_t *jsonPayLoad, feedDetail_t *feedDetailRemote)
{
  int rc, command;

  char *horLine = "------------------------------------------------\n";

  char tableName[SIZE_TABLE_NAME];

  sqlite3_stmt *pStmt;

  cmdPostMessage_t cmd;
  cmdPostMessage_t *ptrCmd = &cmd;

  cmdReturnMessage_t cmdReturn;

  msgBlock_t msgBlock;
  msgBlock_t *ptrMsgBlock = &msgBlock;

  feedDetailRemote->lastSequence++;

  rc = prepareMsgBlockBySequence(feedDetailRemote, &pStmt);

  if (rc != PERSISTENCE_OK)
  {
    pStmt = NULL;
    return STCLI_END;
  }
  rc = nextMsgBlock(pStmt, ptrMsgBlock);

  while (rc == PERSISTENCE_OK)
  {
    memcpy(ptrCmd->pubPk, feedDetailRemote->pubPk, sizeof(ptrCmd->pubPk));
    memcpy(ptrCmd->senderPk, feedDetailRemote->senderPk, sizeof(ptrCmd->senderPk));
    memcpy(ptrCmd->pubTicket, feedDetailRemote->pubTicket, sizeof(ptrCmd->pubTicket));
    memcpy(ptrCmd->previousHASH, ptrMsgBlock->previousHASH, sizeof(ptrCmd->previousHASH));
    ptrCmd->sequence = ptrMsgBlock->sequence;
    memcpy(ptrCmd->recipientPk, ptrMsgBlock->recipientPk, sizeof(ptrCmd->recipientPk));
    ptrCmd->tagMsg = ptrMsgBlock->tagMsg;
    memcpy(ptrCmd->nonceSign, ptrMsgBlock->nonceSign, sizeof(ptrCmd->nonceSign));
    memcpy(ptrCmd->message, ptrMsgBlock->message, sizeof(ptrCmd->message));
    memcpy(ptrCmd->signature, ptrMsgBlock->signature, sizeof(ptrCmd->signature));
    memcpy(ptrCmd->blockHASH, ptrMsgBlock->blockHASH, sizeof(ptrCmd->blockHASH));
    ptrCmd->confirmations = ptrMsgBlock->confirmations;

    cmdPostMessageBin2B64(ptrCmd);

    rc = sendCmdPostMessage(ptrCmd, sockFd);
    if (rc != PROTOCOL_OK)
      return STCLI_END;

    rc = receiveJSON(sockFd, &jsonPayLoad, &command);
    if (rc != PROTOCOL_OK)
    {
      showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
      return STCLI_END;
    }

    printReceivedCommand(command, 0);

    switch (command)
    {
    case CMD_RETURN_MESSAGE:
      break;
    default:
      showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
      return STCLI_END;
      break;
    }

    loadCmdReturnMessage(&cmdReturn, jsonPayLoad); 

    printf("\tReturn Code: (%d) %s\n", cmdReturn.returnCode, getStrRetCode(cmdReturn.returnCode));

    if (cmdReturn.returnCode != RET_OK)
      break;

    msgBlock.confirmations++;

    feedTableName(tableName, ptrCmd->pubPk, ptrCmd->senderPk);

    updateMsgBlock(tableName, ptrMsgBlock);

    rc = nextMsgBlock(pStmt, ptrMsgBlock);
  }

  return STCLI_SYNC_MSGBLOCK;
}

/**
 * @brief Send CMD_GET_MESSAGE from sequence received at feedDetailRemote
 *        If there is some message, will receive CMD_POST_MESSAGE
 *          If any error at message, send CMD_RETURN with error
 *          Else insert message
 *        When there is no more messages from other side 
 *        will receive CMD_RETURN_MESSAGE with RET_NO_MORE_MESSAGES
 *         
 * @param sockFd 
 * @param jsonPayLoad 
 * @param feedDetailRemote 
 * @return int 
 */
int stCliGetMessage(int sockFd, json_t *jsonPayLoad, feedDetail_t *feedDetailRemote)
{
  int rc, command, retCode;

  char *horLine = "------------------------------------------------\n";

  char tableName[SIZE_TABLE_NAME];

  feedDetail_t feedDetailLoc;
  feedDetail_t *ptrFeedDetailLoc = &feedDetailLoc;

  cmdGetMessage_t cmdGet;
  cmdGetMessage_t *ptrCmdGet = &cmdGet;

  cmdPostMessage_t cmdPost;
  cmdPostMessage_t *ptrCmdPost = &cmdPost;

  cmdReturnMessage_t cmdReturn;
  cmdReturnMessage_t *ptrCmdReturn = &cmdReturn;

  msgBlock_t msgBlock;
  msgBlock_t *ptrMsgBlock = &msgBlock;

  json_t *jsonPostMessage;

  char sendGetMessage = 1;

  while (sendGetMessage)
  {

    memcpy(ptrFeedDetailLoc, feedDetailRemote, sizeof(feedDetail_t));

    getLastFeedDetail(ptrFeedDetailLoc);

    memcpy(ptrCmdGet->pubPk, ptrFeedDetailLoc->pubPk, sizeof(ptrCmdGet->pubPk));
    memcpy(ptrCmdGet->senderPk, ptrFeedDetailLoc->senderPk, sizeof(ptrCmdGet->pubPk));
    memcpy(ptrCmdGet->pubTicket, ptrFeedDetailLoc->pubTicket, sizeof(ptrCmdGet->pubTicket));
    ptrCmdGet->sequence = ptrFeedDetailLoc->lastSequence + 1;

    cmdGetMessageBin2B64(ptrCmdGet);

    rc = sendCmdGetMessage(ptrCmdGet, sockFd);
    if (rc != PROTOCOL_OK)
      return STCLI_END;

    rc = receiveJSON(sockFd, &jsonPayLoad, &command);
    if (rc != PROTOCOL_OK)
    {
      showProtocolMsg(MSG_ERROR, __func__, rc, "Receiving JSON");
      return STCLI_END;
    }
    printReceivedCommand(command, 0);

    switch (command)
    {
    case CMD_POST_MESSAGE:
      break;
    case CMD_RETURN_MESSAGE:
      loadCmdReturnMessage(&cmdReturn, jsonPayLoad); // TODO create a function to return string of code
      printf("\tReturn Code: (%d)\n", cmdReturn.returnCode, getStrRetCode(cmdReturn.returnCode));

      sendGetMessage = 0;
      continue;
      break;
    default:
      showProtocolMsg(MSG_ERROR, __func__, command, "Unexpected Command received");
      return STCLI_END;
      break;
    }

    rc = loadCmdPostMessage(ptrCmdPost, jsonPayLoad);
    if (rc != PROTOCOL_OK)
    {
      return STCLI_END;
    }
    printf("\tPub          : %s\n", ptrCmdPost->pubPkB64);
    printf("\tSender       : %s\n", ptrCmdPost->senderPkB64);
    printf("\tTicket       : %s\n", ptrCmdPost->pubTicketB64);
    printf("\tpreviousHASH : %s\n", ptrCmdPost->previousHASHB64);
    printf("\tsequence     : %lu\n", ptrCmdPost->sequence);
    printf("\trecipient    : %s\n", ptrCmdPost->recipientPkB64);
    printf("\ttagMsg       : %lu\n", ptrCmdPost->tagMsg);
    printf("\tnonceMsg     : %s\n", ptrCmdPost->nonceSignB64);
    printf("\tmessage      : %s\n", ptrCmdPost->messageB64);
    printf("\tsignature    : %s\n", ptrCmdPost->signatureB64);
    printf("\tblockHASH    : %s\n", ptrCmdPost->blockHASHB64);
    printf("\tconfirmations: %lu\n", ptrCmdPost->confirmations);

    memcpy(ptrMsgBlock->previousHASH, ptrCmdPost->previousHASH, sizeof(msgBlock.previousHASH));
    msgBlock.sequence = cmdPost.sequence;
    memcpy(ptrMsgBlock->recipientPk, ptrCmdPost->recipientPk, sizeof(msgBlock.recipientPk));
    msgBlock.tagMsg = cmdPost.tagMsg;
    memcpy(ptrMsgBlock->nonceSign, ptrCmdPost->nonceSign, sizeof(msgBlock.nonceSign));
    memcpy(ptrMsgBlock->message, ptrCmdPost->message, sizeof(msgBlock.message));
    memcpy(ptrMsgBlock->signature, ptrCmdPost->signature, sizeof(msgBlock.signature));
    memcpy(ptrMsgBlock->blockHASH, ptrCmdPost->blockHASH, sizeof(msgBlock.blockHASH));
    msgBlock.confirmations = cmdPost.confirmations;

    memset(ptrCmdReturn, 0, sizeof(cmdReturnMessage_t));

    memcpy(ptrFeedDetailLoc->pubPk, ptrCmdPost->pubPk, sizeof(ptrFeedDetailLoc->pubPk));
    memcpy(ptrFeedDetailLoc->senderPk, ptrCmdPost->senderPk, sizeof(ptrFeedDetailLoc->senderPk));
    ptrFeedDetailLoc->lastSequence = ptrCmdPost->sequence;

    rc = verifyMsgBlock(ptrMsgBlock, ptrFeedDetailLoc);

    switch (rc)
    {
    case MSG_BLOCK_OK:
      msgBlock.confirmations++;
      feedTableName(tableName, ptrCmdPost->pubPk, ptrCmdPost->senderPk);
      insertMsgBlock(tableName, ptrMsgBlock);
      retCode = RET_OK;
      break;
    case MSG_BLOCK_INVALID_SIGNATURE:
      retCode = RET_INVALID_SIGNATURE;
      break;
    case MSG_BLOCK_INVALID_ENCRYPT:
      retCode = RET_INVALID_ENCRYPT;
      break;
    case MSG_BLOCK_INVALID_HASH:
      retCode = RET_INVALID_HASH;
      break;
    case MSG_BLOCK_INVALID_PREVIOUS_HASH:
      retCode = RET_INVALID_PREVIOUS_HASH;
      break;
    case MSG_BLOCK_INVALID_SEQUENCE:
      retCode = RET_INVALID_SEQUENCE;
      break;
    }

    cmdReturn.returnCode = retCode;

    cmdReturnMessageBin2B64(ptrCmdReturn);

    rc = sendCmdReturnMessage(ptrCmdReturn, sockFd);
    if (rc != PROTOCOL_OK)
    {
      showProtocolMsg(MSG_ERROR, __func__, rc, "During send Command");
      return STCLI_END;
    }

    sendGetMessage = (retCode == RET_OK);
  }
}