/**
 * @file cmdPostFeedHeader.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 11-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "cmdPostFeedHeader.h"

/**
 * @brief Convert all fields to Base64
 * 
 * @param cmd 
 */
void cmdPostFeedHeaderBin2B64(cmdPostFeedHeader_t *cmd)
{
    bin2b64(cmd->pubPkB64, sizeof(cmd->pubPkB64), cmd->pubPk, sizeof(cmd->pubPk));
    bin2b64(cmd->senderPkB64, sizeof(cmd->senderPkB64), cmd->senderPk, sizeof(cmd->senderPk));
    bin2b64(cmd->pubTicketB64, sizeof(cmd->pubTicketB64), cmd->pubTicket, sizeof(cmd->pubTicket));
}

void cmdPostFeedHeaderB642Bin(cmdPostFeedHeader_t *cmd)
{
    b642bin(cmd->pubPk, sizeof(cmd->pubPk), cmd->pubPkB64, sizeof(cmd->pubPkB64));
    b642bin(cmd->senderPk, sizeof(cmd->senderPk), cmd->senderPkB64, sizeof(cmd->senderPkB64));
    b642bin(cmd->pubTicket, sizeof(cmd->pubTicket), cmd->pubTicketB64, sizeof(cmd->pubTicketB64));
}

int sendCmdPostFeedHeader(cmdPostFeedHeader_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];

    memset(msgSent, 0, strlen(msgSent));

    cmdPostFeedHeaderBin2B64(cmd);

    sprintf(msgSent, "{\"command\": %d, "
                     "\"pub\": \"%s\", "
                     "\"sender\": \"%s\", "
                     "\"ticket\": \"%s\" }",
            CMD_POST_FEED_HEADER, 
            cmd->pubPkB64, 
            cmd->senderPkB64, 
            cmd->pubTicketB64);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

/**
 * @brief Load data from Json
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int 
 */
int loadCmdPostFeedHeader(cmdPostFeedHeader_t *cmd, json_t *jsonPayLoad)
{
    int errCode;


    memset(cmd, 0, sizeof(cmdPostFeedHeader_t));

    strcpy(cmd->pubPkB64, jsonGetString(jsonPayLoad, "pub", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->senderPkB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->pubTicketB64, jsonGetString(jsonPayLoad, "ticket", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;

    cmdPostFeedHeaderB642Bin(cmd);

    return PROTOCOL_OK;
}
