/**
 * @file cmdGetFeedDetail.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 19-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef CMDGETFEEDDETAIL_H
#define CMDGETFEEDDETAIL_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
    unsigned char pubTicket[SIGNATURE_SIZE];
    unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
} cmdGetFeedDetail_t;

void cmdGetFeedDetailBin2B64(cmdGetFeedDetail_t *cmd);
void cmdGetFeedDetailBin2B64(cmdGetFeedDetail_t *cmd);

int sendCmdGetFeedDetail(cmdGetFeedDetail_t *cmd, int sockFd);
int loadCmdGetFeedDetail(cmdGetFeedDetail_t *cmd, json_t *jsonPayLoad);

#endif