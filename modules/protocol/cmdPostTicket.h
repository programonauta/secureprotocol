/**
 * @file cmdPostTicket.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 30-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef CMDPOSTTICKET_H
#define CMDPOSTTICKET_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char recipient[PUBLICKEY_SIZE];
    unsigned char recipientB64[PUBLICKEY_SIZE_B64];
    unsigned char ticket[SIGNATURE_SIZE];
    unsigned char ticketB64[SIGNATURE_SIZE_B64];
} cmdPostTicket_t;

void cmdPostTicketBin2B64(cmdPostTicket_t *cmd);
void cmdPostTicketB642Bin(cmdPostTicket_t *cmd);
int createTicket(cmdPostTicket_t *cmd, node_t *nodePub); 
int verifySignCmdPostTicket(cmdPostTicket_t *cmd);

int sendCmdPostTicket(cmdPostTicket_t *cmd, int sockFd);
int loadCmdPostTicket(cmdPostTicket_t *cmd, json_t *jsonPayLoad);

#endif