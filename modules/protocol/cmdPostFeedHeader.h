/**
 * @file cmdPostFeedHeader.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 22-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef CMDPOSTFEEDHEADER_H
#define CMDPOSTFEEDHEADER_H

#include "protocolCmds.h"
#include "cryptography.h"

typedef struct
{
    unsigned char pubPk[PUBLICKEY_SIZE];
    unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
    unsigned char pubTicket[SIGNATURE_SIZE];
    unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
} cmdPostFeedHeader_t;

void cmdPostFeedHeaderBin2B64(cmdPostFeedHeader_t *cmd);
void cmdPostFeedHeaderB642Bin(cmdPostFeedHeader_t *cmd);

int sendCmdPostFeedHeader(cmdPostFeedHeader_t *cmd, int sockFd);
int loadCmdPostFeedHeader(cmdPostFeedHeader_t *cmd, json_t *jsonPayLoad);

#endif