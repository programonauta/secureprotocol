/**
 * @file cmdGetFeedHeader.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 11-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "cmdGetFeedHeader.h"

void cmdGetFeedHeaderBin2B64(cmdGetFeedHeader_t *cmd)
{
    bin2b64(cmd->senderPkB64, sizeof(cmd->senderPkB64), cmd->senderPk, sizeof(cmd->senderPk));
}

void cmdGetFeedHeaderB642Bin(cmdGetFeedHeader_t *cmd)
{
    b642bin(cmd->senderPk, sizeof(cmd->senderPk), cmd->senderPkB64, sizeof(cmd->senderPkB64));
}


int sendCmdGetFeedHeader(cmdGetFeedHeader_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];
    char *horLine = "------------------------------------------------\n";

    node_t node;
    int rc;

    rc = readNode(&node); 

    memset(msgSent, 0, strlen(msgSent));

    cmdGetFeedHeaderBin2B64(cmd);

    sprintf(msgSent, "{\"command\": %d, "
                     "\"sender\": \"%s\" }",
            CMD_GET_FEED_HEADER, 
            node.pkB64);

    printf("\n%sCommand Sent: CMD_GET_FEED_HEADER\n", horLine);
    printf("\tSender: %s\n", cmd->senderPkB64);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

/**
 * @brief Load data from Json
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int 
 */
int loadCmdGetFeedHeader(cmdGetFeedHeader_t *cmd, json_t *jsonPayLoad)
{
    int errCode;

    memset(cmd, 0, sizeof(cmdGetFeedHeader_t));

    strcpy(cmd->senderPkB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;

    cmdGetFeedHeaderB642Bin(cmd);

    return PROTOCOL_OK;
}

