/**
 * @file cmdGetFeedDetail.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 19-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "cmdGetFeedDetail.h"

char *horLine = "------------------------------------------------\n";

void cmdGetFeedDetailBin2B64(cmdGetFeedDetail_t *cmd)
{
    bin2b64(cmd->pubPkB64, sizeof(cmd->pubPkB64), cmd->pubPk, sizeof(cmd->pubPk));
    bin2b64(cmd->senderPkB64, sizeof(cmd->senderPkB64), cmd->senderPk, sizeof(cmd->senderPk));
    bin2b64(cmd->pubTicketB64, sizeof(cmd->pubTicketB64), cmd->pubTicket, sizeof(cmd->pubTicket));
}

void cmdGetFeedDetailB642Bin(cmdGetFeedDetail_t *cmd)
{
    b642bin(cmd->pubPk, sizeof(cmd->pubPk), cmd->pubPkB64, sizeof(cmd->pubPkB64));
    b642bin(cmd->senderPk, sizeof(cmd->senderPk), cmd->senderPkB64, sizeof(cmd->senderPkB64));
    b642bin(cmd->pubTicket, sizeof(cmd->pubTicket), cmd->pubTicketB64, sizeof(cmd->pubTicketB64));
}


int sendCmdGetFeedDetail(cmdGetFeedDetail_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE]; 
    char *horLine = "------------------------------------------------\n";

    memset(msgSent, 0, strlen(msgSent));

    cmdGetFeedDetailBin2B64(cmd);

    sprintf(msgSent, "{\"command\": %d, "
                     "\"pub\": \"%s\" ,"
                     "\"sender\": \"%s\", "
                     "\"ticket\": \"%s\"} ",
            CMD_GET_FEED_DETAIL,
            cmd->pubPkB64,
            cmd->senderPkB64,
            cmd->pubTicketB64);

    printf("\n%sCommand Sent: CMD_GET_FEED_DETAIL\n",horLine);
    printf("\tPub   : %s\n", cmd->pubPkB64);
    printf("\tSender: %s\n", cmd->senderPkB64);
    printf("\tTicket: %s\n", cmd->pubTicketB64);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

/**
 * @brief Load data from Json
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int PROTOCOL_OK or PROTOCOL_FAIL
 */
int loadCmdGetFeedDetail(cmdGetFeedDetail_t *cmd, json_t *jsonPayLoad)
{
    int errCode;

    memset(cmd, 0, sizeof(cmdGetFeedDetail_t));

    strcpy(cmd->pubPkB64, jsonGetString(jsonPayLoad, "pub", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->senderPkB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->pubTicketB64, jsonGetString(jsonPayLoad, "ticket", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;

    cmdGetFeedDetailB642Bin(cmd);

    return PROTOCOL_OK;
}

