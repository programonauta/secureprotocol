/**
 * @file cmdReturnMsg.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 24-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef CMDRETURNMESSAGE_H
#define CMDRETURNMESSAGE_H

#include "protocolCmds.h"
#include "cryptography.h"
#include "json.h"

// Return Codes
enum cmdReturnMessageCodes
{
  RET_OK,
  RET_INVALID_COMMAND,
  RET_INVALID_SIGNATURE,
  RET_INVALID_ENCRYPT,
  RET_INVALID_HASH,
  RET_INVALID_PREVIOUS_HASH,
  RET_INVALID_SEQUENCE,
  RET_INVALID_TICKET,
  RET_NOT_SAME_PUB,
  RET_NO_MORE_FEEDS,
  RET_NO_MORE_MSGS,
  RET_INVITE_GRANTED,
  RET_INVITE_ERR_NOT_FOUND,
  RET_INVITE_ERR_OTHER_NODE,
  RET_INVITE_ALREADY_GRANTED
};

typedef struct
{
    unsigned char senderPk[PUBLICKEY_SIZE];
    unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
    unsigned char recipientPk[PUBLICKEY_SIZE];
    unsigned char recipientPkB64[PUBLICKEY_SIZE_B64];
    char returnCode;
} cmdReturnMessage_t;

void cmdReturnMessageBin2B64(cmdReturnMessage_t *cmd);
void cmdReturnMessageB642Bin(cmdReturnMessage_t *cmd);

int sendCmdReturnMessage(cmdReturnMessage_t *cmd, int sockFd);
int loadCmdReturnMessage(cmdReturnMessage_t *cmd, json_t* jsonPayLoad);

const char *getStrRetCode(int retCode);

#endif