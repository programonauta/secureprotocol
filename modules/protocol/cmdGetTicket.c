/**
 * @file cmdGetTicket.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 22-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "cmdGetTicket.h"

void cmdGetTicketBin2B64(cmdGetTicket_t *cmd)
{
    //int msgLen = strlen(cmd->encryptedInvite);
    int msgLen = 55;

    bin2b64(cmd->senderB64, sizeof(cmd->senderB64), cmd->sender, sizeof(cmd->sender));
    bin2b64(cmd->pubPkB64, sizeof(cmd->pubPkB64), cmd->pubPk, sizeof(cmd->pubPk));
    bin2b64(cmd->nonceEncryptB64, sizeof(cmd->nonceEncryptB64), cmd->nonceEncrypt, sizeof(cmd->nonceEncrypt));
    bin2b64(cmd->nonceSignB64, sizeof(cmd->nonceSignB64), cmd->nonceSign, sizeof(cmd->nonceSign));
    bin2b64(cmd->signatureB64, sizeof(cmd->signatureB64), cmd->signature, sizeof(cmd->signature));
    bin2b64(cmd->encryptedInviteB64, sizeof(cmd->encryptedInviteB64), cmd->encryptedInvite, msgLen);
}

void cmdGetTicketB642Bin(cmdGetTicket_t *cmd)
{
    int msgLen = strlen(cmd->encryptedInviteB64);

//    printf("xxxxxxxxxxxxxxxxxxx\n(%s)Msg Len of InviteB64: %d\n", cmd->encryptedInviteB64, msgLen);

    b642bin(cmd->sender, sizeof(cmd->sender), cmd->senderB64, sizeof(cmd->senderB64));
    b642bin(cmd->pubPk, sizeof(cmd->pubPk), cmd->pubPkB64, sizeof(cmd->pubPkB64));
    b642bin(cmd->nonceEncrypt, sizeof(cmd->nonceEncrypt), cmd->nonceEncryptB64, sizeof(cmd->nonceEncryptB64));
    b642bin(cmd->nonceSign, sizeof(cmd->nonceSign), cmd->nonceSignB64, sizeof(cmd->nonceSignB64));
    b642bin(cmd->signature, sizeof(cmd->signature), cmd->signatureB64, sizeof(cmd->signatureB64));
    b642bin(cmd->encryptedInvite, sizeof(cmd->encryptedInvite), cmd->encryptedInviteB64, msgLen);

//    printf("xxxxxxxxxxxxxxxxxxx\nMsg Len of Encrypted Invite: %d\n", strlen(cmd->encryptedInvite));

}

int encryptInvite(cmdGetTicket_t *cmd, node_t *sender)
{
    int rc;
    int msgLen = strlen(cmd->flatInvite);

//    printf("xxxxxxxxxxxxxxxxxxx\n(%s)Msg Len of Flat: %d\n", cmd->flatInvite, msgLen);

    memset(cmd->encryptedInvite, 0, sizeof(cmd->encryptedInvite));
    rc = encryptMsg(cmd->flatInvite, msgLen, cmd->encryptedInvite,
                    cmd->pubPk, sender->sk, cmd->nonceEncrypt);

    return (rc == CRYPTO_OK ? PROTOCOL_OK : PROTOCOL_FAIL);
}

/**
 * @brief Decrypt invite received on GET_TICKET command
 * 
 * @param cmd 
 * @param recipient 
 * @return int PROTOCOL_OK or PROTOCOL_FAIL
 */
int decryptInvite(cmdGetTicket_t *cmd, node_t *recipient)
{

    //int msgLen = strlen(cmd->encryptedInvite)-TAG_SIZE;
    int msgLen = 55-TAG_SIZE;

    int rc = decryptMsg(cmd->flatInvite, msgLen, cmd->encryptedInvite,
                        cmd->sender, recipient->sk, cmd->nonceEncrypt);

    return (rc == CRYPTO_OK ? PROTOCOL_OK : PROTOCOL_FAIL);
}

int signCmdGetTicket(cmdGetTicket_t *cmd, node_t *sender)
{
    size_t msgLen = strlen(cmd->encryptedInvite);    
    int rc = signMsg(cmd->encryptedInvite, cmd->nonceSign, sender->sk, cmd->signature, msgLen);
    return (rc == CRYPTO_OK ? PROTOCOL_OK : PROTOCOL_FAIL);
}

/**
 * @brief Vefify the signature of command
 * 
 * @param cmd 
 * @return int PROTOCOL_OK or PROTOCOL_FAIL
 */
int verifySignCmdGetTicket(cmdGetTicket_t *cmd)
{
    size_t msgLen = strlen(cmd->encryptedInvite);
    int rc = verifySignature(cmd->encryptedInvite, cmd->nonceSign, cmd->sender, cmd->signature, msgLen);
    return (rc == CRYPTO_OK ? PROTOCOL_OK : PROTOCOL_FAIL);
}

int sendCmdGetTicket(cmdGetTicket_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];

    memset(msgSent, 0, strlen(msgSent));

    sprintf(msgSent, "{\"command\": %d, "
                     "\"sender\": \"%s\", "
                     "\"pubPk\": \"%s\", "
                     "\"nonceEncrypt\": \"%s\", "
                     "\"nonceSign\": \"%s\", "
                     "\"invite\": \"%s\", "
                     "\"signature\": \"%s\" }",
            CMD_GET_TICKET, cmd->senderB64, cmd->pubPkB64, cmd->nonceEncryptB64, cmd->nonceSignB64,
            cmd->encryptedInviteB64, cmd->signatureB64);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

/**
 * @brief Load data from Json to cmdGetTicket strucuture
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int 
 */
int loadCmdGetTicket(cmdGetTicket_t *cmd, json_t *jsonPayLoad)
{
    int errCode;

    memset(cmd, 0, sizeof(cmdGetTicket_t));

    strcpy(cmd->senderB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->pubPkB64, jsonGetString(jsonPayLoad, "pubPk", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->nonceEncryptB64, jsonGetString(jsonPayLoad, "nonceEncrypt", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->nonceSignB64, jsonGetString(jsonPayLoad, "nonceSign", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->encryptedInviteB64, jsonGetString(jsonPayLoad, "invite", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->signatureB64, jsonGetString(jsonPayLoad, "signature", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;

    cmdGetTicketB642Bin(cmd);

    return PROTOCOL_OK;
}
