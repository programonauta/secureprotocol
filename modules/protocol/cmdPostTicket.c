/**
 * @file cmdPostTicket.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 30-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "cmdPostTicket.h"

void cmdPostTicketBin2B64(cmdPostTicket_t *cmd)
{
    bin2b64(cmd->pubPkB64, sizeof(cmd->pubPkB64), cmd->pubPk, sizeof(cmd->pubPk));
    bin2b64(cmd->recipientB64, sizeof(cmd->recipientB64), cmd->recipient, sizeof(cmd->recipient));
    bin2b64(cmd->ticketB64, sizeof(cmd->ticketB64), cmd->ticket, sizeof(cmd->ticket));
}

void cmdPostTicketB642Bin(cmdPostTicket_t *cmd)
{
    b642bin(cmd->pubPk, sizeof(cmd->pubPk), cmd->pubPkB64, sizeof(cmd->pubPkB64));
    b642bin(cmd->recipient, sizeof(cmd->recipient), cmd->recipientB64, sizeof(cmd->recipientB64));
    b642bin(cmd->ticket, sizeof(cmd->ticket), cmd->ticketB64, sizeof(cmd->ticketB64));
}

int createTicket(cmdPostTicket_t *cmd, node_t *nodePub)
{
    unsigned char nonceSign[NONCE_MSG_SIZE];

    memset(&nonceSign, 0, sizeof(nonceSign));

    int rc = signMsg(cmd->recipient, nonceSign, nodePub->sk, cmd->ticket, sizeof(cmd->recipient));
    return (rc == CRYPTO_OK ? PROTOCOL_OK : PROTOCOL_FAIL);
}

int verifySignCmdPostTicket(cmdPostTicket_t *cmd)
{
    unsigned char nonceSign[NONCE_SIZE];

    memset(&nonceSign, 0, sizeof(nonceSign));

    int rc = verifySignature(cmd->recipient, nonceSign, cmd->pubPk, cmd->ticket, sizeof(cmd->recipient));
    return (rc == CRYPTO_OK ? PROTOCOL_OK : PROTOCOL_FAIL);
}

int sendCmdPostTicket(cmdPostTicket_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];

    memset(msgSent, 0, strlen(msgSent));

    sprintf(msgSent, "{\"command\": %d, "
                     "\"pubPk\": \"%s\", "
                     "\"recipient\": \"%s\", "
                     "\"ticket\": \"%s\" }",
            CMD_POST_TICKET, cmd->pubPkB64, cmd->recipientB64, cmd->ticketB64);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

int loadCmdPostTicket(cmdPostTicket_t *cmd, json_t *jsonPayLoad)
{
    int errCode;
    memset(cmd, 0, sizeof(cmdPostTicket_t));
    strcpy(cmd->pubPkB64, jsonGetString(jsonPayLoad, "pubPk", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->recipientB64, jsonGetString(jsonPayLoad, "recipient", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->ticketB64, jsonGetString(jsonPayLoad, "ticket", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmdPostTicketB642Bin(cmd);
    return PROTOCOL_OK;
}
