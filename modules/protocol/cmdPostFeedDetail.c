/**
 * @file cmdPostFeedDetail.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 19-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "cmdPostFeedDetail.h"

/**
 * @brief Convert all fields to Base64
 * 
 * @param cmd 
 */
void cmdPostFeedDetailBin2B64(cmdPostFeedDetail_t *cmd)
{
    bin2b64(cmd->pubPkB64, sizeof(cmd->pubPkB64), cmd->pubPk, sizeof(cmd->pubPk));
    bin2b64(cmd->senderPkB64, sizeof(cmd->senderPkB64), cmd->senderPk, sizeof(cmd->senderPk));
    bin2b64(cmd->lastHASHB64, sizeof(cmd->lastHASHB64), cmd->lastHASH, sizeof(cmd->lastHASH));
    bin2b64(cmd->pubTicketB64, sizeof(cmd->pubTicketB64), cmd->pubTicket, sizeof(cmd->pubTicket));
}

void cmdPostFeedDetailB642Bin(cmdPostFeedDetail_t *cmd)
{
    b642bin(cmd->pubPk, sizeof(cmd->pubPk), cmd->pubPkB64, sizeof(cmd->pubPkB64));
    b642bin(cmd->senderPk, sizeof(cmd->senderPk), cmd->senderPkB64, sizeof(cmd->senderPkB64));
    b642bin(cmd->pubTicket, sizeof(cmd->pubTicket), cmd->pubTicketB64, sizeof(cmd->pubTicketB64));
    b642bin(cmd->lastHASH, sizeof(cmd->lastHASH), cmd->lastHASHB64, sizeof(cmd->lastHASHB64));
}

int sendCmdPostFeedDetail(cmdPostFeedDetail_t *cmd, int sockFd)
{
    char msgSent[MAX_MESSAGE_SIZE];

    memset(msgSent, 0, strlen(msgSent));

    cmdPostFeedDetailBin2B64(cmd);

    sprintf(msgSent, "{\"command\": %d, "
                     "\"pub\": \"%s\", "
                     "\"sender\": \"%s\", "
                     "\"ticket\": \"%s\" ,"
                     "\"lastSeq\": %lu ,"
                     "\"lastHASH\": \"%s\" }",
            CMD_POST_FEED_DETAIL, 
            cmd->pubPkB64, 
            cmd->senderPkB64, 
            cmd->pubTicketB64,
            cmd->lastSequence,
            cmd->lastHASHB64);

#ifdef DEBUG
    showProtocolMsg(MSG_LOG, __func__, PROTOCOL_OK, msgSent);
#endif

    if (send(sockFd, msgSent, strlen(msgSent), 0) == -1)
    {
        showProtocolMsg(MSG_ERROR, __func__, PROTOCOL_FAIL, "During send Command");
        return PROTOCOL_FAIL;
    }
    return PROTOCOL_OK;
}

/**
 * @brief Load data from Json
 * 
 * @param cmd 
 * @param jsonPayLoad 
 * @return int PROTOCOL_OK or PROTOCOL_FAIL
 */
int loadCmdPostFeedDetail(cmdPostFeedDetail_t *cmd, json_t *jsonPayLoad)
{
    int errCode;

    memset(cmd, 0, sizeof(cmdPostFeedDetail_t));

    strcpy(cmd->pubPkB64, jsonGetString(jsonPayLoad, "pub", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->senderPkB64, jsonGetString(jsonPayLoad, "sender", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->pubTicketB64, jsonGetString(jsonPayLoad, "ticket", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;
    cmd->lastSequence = jsonGetInt(jsonPayLoad, "lastSeq", &errCode);
    if (errCode != 0)
        return PROTOCOL_FAIL;
    strcpy(cmd->lastHASHB64, jsonGetString(jsonPayLoad, "lastHASH", &errCode));
    if (errCode != 0)
        return PROTOCOL_FAIL;

    cmdPostFeedDetailB642Bin(cmd);

    return PROTOCOL_OK;
}
