#ifndef PROTOCOL_CLI_H
#define PROTOCOL_CLI_H

#include "protocol.h"
#include "protocolCmds.h"
#include "cryptography.h"
#include "cmdGetTicket.h"
#include "cmdPostTicket.h"
#include "cmdReturnMessage.h"
#include "cmdGetFeedHeader.h"
#include "cmdPostFeedHeader.h"
#include "cmdGetFeedDetail.h"
#include "cmdPostFeedDetail.h"
#include "cmdGetMessage.h"
#include "cmdPostMessage.h"
#include "dataStructure.h"

// Connection Functions
int tcpConnect(const char *host, const char *port, enum ipFamily ipFamily);

enum stateProtocolCli
{
  STCLI_GET_TICKET,
  STCLI_DEAL_POSTTICKET,
  STCLI_UPDATE_MYPUB,
  STCLI_SYNC_MSGS,
  STCLI_DEAL_POST_FEED_HEADER,
  STCLI_SYNC_MSGBLOCK,
  STCLI_POST_MESSAGE,
  STCLI_GET_MESSAGE,
  STCLI_DEAL_RETURN,
  STCLI_SEND_RETURN,
  STCLI_END
};

int protocolGetTicket(node_t *node, myPub_t *myPub, int sockFd);
int protocolSyncMsgs(node_t *node, int sockFd);

int stCliGetTicket(int sockFd, cmdGetTicket_t *ptrCmd, json_t **jsonPayLoad);
int stCliDealPostTicket(cmdPostTicket_t *cmd, json_t *jsonPayLoad);
int stCliSendReturn(int sockFd, int retCode, unsigned char *pubPk, unsigned char *sender);
int stCliDealReturn(json_t *jsonPayLoad);
int stCliUpdateMyPub(int inviteStatus, unsigned char *pubInvite, unsigned char *pubTicket);

int stCliSyncMsgs(int sockFd, cmdGetFeedHeader_t *ptrCmd, json_t **jsonPayLoad);
int stCliDealPostFeedHeader(cmdPostFeedHeader_t *cmd, json_t *jsonPayLoad, int *retCode);
int stCliSyncMsgBlock(int sockFd, json_t *jsonPayLoad, feedDetail_t *feedDetailRemote);
int stCliPostMessage(int sockFd, json_t *jsonPayLoad, feedDetail_t *feedDetailRemote);
int stCliGetMessage(int sockFd, json_t *jsonPayLoad, feedDetail_t *feedDetailRemote);

#endif