#ifndef PROTOCOL_CMDS_H
#define PROTOCOL_CMDS_H

#include "protocol.h"
#include "cryptography.h"

enum protocolCmds_e
{
  CMD_GET_TICKET,
  CMD_POST_TICKET,
  CMD_GET_FEED_HEADER,
  CMD_POST_FEED_HEADER,
  CMD_GET_FEED_DETAIL,
  CMD_POST_FEED_DETAIL,
  CMD_GET_MESSAGE,
  CMD_POST_MESSAGE,
  CMD_RETURN_MESSAGE,
  CMD_CLOSE_CONNECTION
};

void printReceivedCommand(int command, int level);
int receiveJSON(int sockFd, json_t **jsonObj, int *command);
const char *getStrCommand(int command);

#endif
