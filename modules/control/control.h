#ifndef CONTROL_H
#define CONTROL_H
/*
 * This module is responsible for protocol functionalities, like structure,
 * store, send and receive messages, calculate hash, sign, encrypt messages and so on.
 *
 */

#include <sodium.h>
#include "cryptography/cryptography.h"

#define CONTROL_OK                 0
#define CONTROL_FAIL               1
#define CONTROL_SECRET_KEY_ERR     2  // Any problem with public key: size, format, etc
#define CONTROL_PUBLIC_KEY_ERR     3  // Any problem with public key: size, format, etc
#define CONTROL_WRONG_SIGNATURE    4  // The signature was not issued by publicKey owner
#define CONTROL_NULL_MESSAGE       5  // Try to sign a NULL message
#define CONTROL_WRONG_HASH         6  // The hash doesn't match
#define CONTROL_INVALID_INVITE     7  // Invalid Size of Invite
#define CONTROL_FEED_NOT_EMPTY     8  // Return this message if feed has message blocks
#define CONTROL_FEED_EMPTY         9  // Feed need a affiliation request message
#define CONTROL_NO_TICKET          10 // There is no Ticket issued by Pub
#define CONTROL_WRONG_TICKET       11 // Ticket was not signed by the pub, or have wrong size
#define CONTROL_DECRYPT_ERR_SIZE   12 // Size of message less than MESSAGE_NONCE_SIZE
#define CONTROL_DECRYPT_ERR_NONCE  13 // Nonce of message doesn't match with Nonce of decyphered message


// Signatures functions

int signMessage(msgBlock_t* msgBlock, unsigned char* sk);
int verfiySignature(msgBlock_t* msgBlock, unsigned char *pk);

// Encrypt functions
int encryptMsgBlock(msgBlock_t* msgBlock, unsigned char* sk, unsigned char* flatMsg, size_t msgLen);
int decryptMsgBlock(msgBlock_t* msgBlock, unsigned char* senderPk, unsigned char* recipientSk, unsigned char* flatMsg);

// Hash functions

int hashBlock(msgBlock_t* msgBlock);
int verifyHash(msgBlock_t* msgBlock);

// Feed functions
void resetMsgBlock(msgBlock_t* msgBlock);

int verifyFeed(feedHeader_t* feedHeader, msgBlock_t* msgBlock, unsigned long startMsgBLock, unsigned long lastMsgBlock);

#endif
