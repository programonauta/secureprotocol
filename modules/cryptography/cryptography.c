/*
 * Crypograph.c
 *
 * Used to create key pairs, calculate hash codes, sign messages, and encrypt private messages
 *
 *
 */

#define __USE_MINGW_ANSI_STDIO
#include <string.h>
#include "cryptography.h"

int createKeyPair(unsigned char *pk, unsigned char *sk)
{
  crypto_sign_ed25519_keypair(pk, sk);
  return CRYPTO_OK;
}

/**
 * @brief Verify if pk and sk are valid keys
 * 
 * @param *pk 
 * @param *sk 
 * @return int CRYPTO_OK if OK, CRYPTO_FAIL if there are problems
 */
int verifyKeyPair(unsigned char *pk, unsigned char *sk)
{
  const unsigned char* message = (unsigned char*)"Testing sign procedure";

  size_t msgLen;
  // Signature
  unsigned char sig[crypto_sign_BYTES];

  msgLen = strlen((char*)message);

  // Sign message using secret key
  crypto_sign_detached(sig, NULL, message, msgLen, sk);

  // Verify message using public key
  if (crypto_sign_verify_detached(sig, message, msgLen, pk) != 0)
    return CRYPTO_FAIL;

  return CRYPTO_OK;

}

/**
 * @brief Verify if an array is zero
 * 
 * @param pk address of array
 * @param keySize size of array
 * @return int 
 */
int isEmptyKey(unsigned char *pk, int keySize)
{
int i, sum = 0;
for (i = 0; i < keySize; ++i) 
{
  sum += *pk++;
}

return (sum == 0);

}

/**
 * @brief Create a random nonce for encrypt messages
 * 
 * @param nonce 
 */
void createNonceEncrypt(unsigned char* nonce)
{  
  randombytes_buf(nonce, NONCE_SIZE);
}


void createNonceSign(unsigned char* nonce)
{  
  randombytes_buf(nonce, NONCE_MSG_SIZE);
}

/**
 * @brief Encrypt the message flatMsg of msgLen size using
 * recipient Public Key and sender Secret Key
 * 
 * return encrypted message on encrypMsg variable
 * 
 * @param flatMsg - Message to be encrypted
 * @param msgLen - Size of messeage to be encrypted
 * @param encryptMsg - Address of encrypted message
 * @param recipientPk - Recipient Public Key
 * @param senderSk - Sender Secrect Key
 * @param nonce - Nonce of message, must be defined first
 * @return int 
 */
int encryptMsg( unsigned char* flatMsg, size_t msgLen, unsigned char* encryptMsg, 
                unsigned char* recipientPk, unsigned char* senderSk, unsigned char* nonce)
{
  int rc;

  unsigned char encryptPk[PUBLICKEY_BOX_SIZE];
  unsigned char encryptSk[SECRETKEY_BOX_SIZE];

  // Convert keys to curve

  rc = crypto_sign_ed25519_pk_to_curve25519(encryptPk, recipientPk);
  rc = crypto_sign_ed25519_sk_to_curve25519(encryptSk, senderSk);

#ifdef DEBUG
  int i;

  printf("[%s] ---------- Recipient ----------\nPK   : ", __func__);
  for (i = 0; i < PUBLICKEY_SIZE; i++)
    printf("%02x", recipientPk[i]);
  printf("\nEncPk: ");
  for (i = 0; i < PUBLICKEY_BOX_SIZE; i++)
    printf("%02x", encryptPk[i]);
  printf("\n[%s] ---------- Sender ----------\nSK   : ", __func__);
  for (i = 0; i < SECRETKEY_SIZE; i++)
    printf("%02x", senderSk[i]);
  printf("\nEncSk: ");
  for (i = 0; i < SECRETKEY_BOX_SIZE; i++)
    printf("%02x", encryptSk[i]);
  printf("\nMessage Len: %lu\n", msgLen);
  printf("[%s] ---------- Message To Be Encrypted ----------\nFlat : ", __func__);
  for (i = 0; i < msgLen; i++)
    printf("%02x", flatMsg[i]);
  printf("\nFlat : ");
  printf("%s\n", flatMsg);
  

#endif

  rc = crypto_box_easy(encryptMsg, flatMsg, msgLen, nonce, encryptPk, encryptSk);

#ifdef DEBUG
  printf("[%s] ---------- Message Encrypted ----------\nHex  : ", __func__);
  for (i = 0; i < msgLen+TAG_SIZE; i++)
    printf("%02x", encryptMsg[i]);
  printf("[%s] ---------- END ----------\n\n", __func__);

#endif

  if (!rc)
    return CRYPTO_ENCRYPT_ERR;
  else
    return CRYPTO_OK;
}

int decryptMsg(unsigned char* flatMsg, size_t msgLen, unsigned char* encryptMsg,
               unsigned char* senderPk, unsigned char* recipientSk, unsigned char *nonce)
{
  int rc;

  unsigned char encryptPk[PUBLICKEY_BOX_SIZE];
  unsigned char encryptSk[SECRETKEY_BOX_SIZE];

  // Convert keys to curve

  rc = crypto_sign_ed25519_pk_to_curve25519(encryptPk, senderPk);
  rc = crypto_sign_ed25519_sk_to_curve25519(encryptSk, recipientSk);

#ifdef DEBUG
  int i;

  printf("[%s] ---------- Sender ----------\nPK   : ", __func__);
  for (i = 0; i < PUBLICKEY_SIZE; i++)
    printf("%02x", senderPk[i]);
  printf("\nEncSk: ");
  for (i = 0; i < PUBLICKEY_BOX_SIZE; i++)
    printf("%02x", encryptPk[i]);
  printf("\n[%s] ---------- Recipient ----------\nSK   : ", __func__);
  for (i = 0; i < SECRETKEY_SIZE; i++)
    printf("%02x", recipientSk[i]);
  printf("\nEncSk: ");
  for (i = 0; i < SECRETKEY_BOX_SIZE; i++)
    printf("%02x", encryptSk[i]);
  printf("\n[%s] ---------- Message Encrypted ----------\n", __func__);
  printf("Message Len: %lu\n", msgLen);
  for (i=0; i< msgLen; i++)
    printf("%02x", encryptMsg[i]);
  printf("\n");

#endif
  if ((rc = crypto_box_open_easy(flatMsg, encryptMsg, msgLen+TAG_SIZE, nonce, encryptPk, encryptSk )) !=0)
    return CRYPTO_ENCRYPT_ERR;
  else
    return CRYPTO_OK;
}

/**
 * @brief Sign a message using the nonce and return in signature address
 * 
 * @param message 
 * @param nonceSign 
 * @param sk 
 * @param signature 
 * @param msgSize 
 * @return int CRYPTO_NULL_MESSAGE or CRYPTO_OK
 */
int signMsg(unsigned char* message, unsigned char* nonceSign, unsigned char* sk, unsigned char* signature, size_t msgSize)
{
  unsigned char msgWithNonce[NONCE_MSG_SIZE+msgSize];

  if (msgSize == 0)
    return CRYPTO_NULL_MESSAGE;

  memset(msgWithNonce, 0, sizeof(msgWithNonce));

  memcpy(msgWithNonce, nonceSign, NONCE_MSG_SIZE);
  memcpy(&msgWithNonce[NONCE_MSG_SIZE], message, msgSize);

  crypto_sign_detached(signature, NULL, msgWithNonce, sizeof(msgWithNonce), sk);
  return CRYPTO_OK;
}

/**
 * @brief Verify signature using message, private key and nonce
 *        return CRYPTO_NULL_MESSAGE, CRYPTO_OK or CRYPTO_WRONG_SIGNATURE
 * 
 * @param message 
 * @param nonceSign 
 * @param pk 
 * @param signature 
 * @param msgSize 
 * @return int CRYPTO_OK or CRYPTO_WRONG_SIGNATURE
 */
int verifySignature(unsigned char* message, unsigned char* nonceSign, unsigned char *pk, unsigned char* signature, size_t msgSize)
{
  int rc;

  if (msgSize == 0)
    return CRYPTO_NULL_MESSAGE;

  unsigned char msgWithNonce[NONCE_MSG_SIZE+msgSize];

  memset(msgWithNonce, 0, sizeof(msgWithNonce));

  memcpy(msgWithNonce, nonceSign, NONCE_MSG_SIZE);
  memcpy(&msgWithNonce[NONCE_MSG_SIZE], message, msgSize);

  rc = crypto_sign_verify_detached(signature, msgWithNonce, sizeof(msgWithNonce), pk);

  return (rc==0?CRYPTO_OK:CRYPTO_WRONG_SIGNATURE);
}


// Invite Functions

// Base 64 Functions

void bin2b64(char* destB64, size_t sizeB64, unsigned char* bin, size_t sizeBin)
{
  sodium_bin2base64(destB64, sizeB64, bin, sizeBin, sodium_base64_VARIANT_ORIGINAL);
}

void b642bin(unsigned char* destBin, size_t sizeBin, char* b64, size_t sizeB64)
{
  sodium_base642bin(destBin, sizeBin, b64, sizeB64, "\n\r ", NULL, NULL, sodium_base64_VARIANT_ORIGINAL);
}

// SHA256 Functions

void hashSHA256(unsigned char* out, unsigned char* in, size_t sizeIn)
{
  crypto_hash_sha256(out, in, sizeIn);
}

// Hex Functions
void bin2hex(char* strHex, unsigned char* srcBin, size_t sizeBin)
{
  int i;
  for (i = 0; i < sizeBin; i++)
    sprintf(strHex + (i * 2), "%02x", srcBin[i]);
}

const char* strPk2Hex(char *destHex, unsigned char*pk)
{
  bin2hex(destHex, pk, PUBLICKEY_SIZE);
  return destHex;
}

const char* strSk2Hex(char *destHex, unsigned char*sk)
{
  bin2hex(destHex, sk, SECRETKEY_SIZE);
  return destHex;
}

const char* strSignature2Hex(char *destHex, unsigned char* signature)
{
  bin2hex(destHex, signature, SIGNATURE_SIZE);
  return destHex;
}

