#ifndef CRYPTOGRAPHY_H
#define CRYPTOGRAPHY_H
/*
 * cryptography.h
 * 
 * Cryptography module.
 * 
 * Used to create key pairs, calculate hash codes, sign messages, and encrypt private messages
 * 
 */

#include <sodium.h>

enum cryptoReturn
{
    CRYPTO_OK,
    CRYPTO_FAIL,
    CRYPTO_ENCRYPT_ERR,
    CRYPTO_DEENCRYPT_ERR,
    CRYPTO_NULL_MESSAGE,
    CRYPTO_WRONG_SIGNATURE
};

#define PUBLICKEY_BOX_SIZE crypto_box_PUBLICKEYBYTES
#define SECRETKEY_BOX_SIZE crypto_box_SECRETKEYBYTES

// Keys

#define PUBLICKEY_SIZE crypto_sign_PUBLICKEYBYTES
#define SECRETKEY_SIZE crypto_sign_SECRETKEYBYTES
// To calc B64 size
#define PUBLICKEY_SIZE_B64 (((4 * PUBLICKEY_SIZE / 3) + 3) & ~3) + 1
#define SECRETKEY_SIZE_B64 (((4 * SECRETKEY_SIZE / 3) + 3) & ~3) + 1
// Calc String Hex size
#define PUBLICKEY_SIZE_HEX (PUBLICKEY_SIZE * 2) + 1
#define SECRETKEY_SIZE_HEX (SECRETKEY_SIZE * 2) + 1

// Encryption
#define TAG_SIZE crypto_box_MACBYTES
#define TAG_SIZE_B64 (((4 * TAG_SIZE / 3) + 3) & ~3) + 1
#define TAG_SIZE_HEX (TAG_SIZE * 2) + 1

#define NONCE_SIZE crypto_box_NONCEBYTES
#define NONCE_SIZE_B64 (((4 * NONCE_SIZE / 3) + 3) & ~3) + 1

#define NONCE_MSG_SIZE 8
#define NONCE_MSG_SIZE_B64 (((4 * NONCE_MSG_SIZE / 3) + 3) & ~3) + 1

#define SIGNATURE_SIZE crypto_sign_BYTES
#define SIGNATURE_SIZE_B64 (((4 * SIGNATURE_SIZE / 3) + 3) & ~3) + 1
#define SIGNATURE_SIZE_HEX (SIGNATURE_SIZE * 2) + 1

// hash
#define HASH_SIZE crypto_hash_sha256_BYTES
#define HASH_SIZE_B64 (((4 * HASH_SIZE / 3) + 3) & ~3) + 1
#define HASH_SIZE_HEX (HASH_SIZE * 2) + 1

/*
 * createKeys - Create a key pair and return in the variables
 *        pk - public key
 *        sk - secret key
 *  
 * returns
 *        CRYPTO_OK        Everything is ok
 *        CRYPTO_FAIL      Any other error hapens
 */
int createKeyPair(unsigned char *pk, unsigned char *sk);

/*
 * verifyKeys - Verify if keys are ok:
 *        pk - public key
 *        sk - secret key
 * 
 * Size keys - You must provide the size key expected 
 *        pkSize - size of public key
 *        skSise - size of secret key
 * 
 * returns
 *        CRYPTO_OK        Everything is ok
 *        CRYPTO_FAIL      Any other error hapens
 * 
 */
int verifyKeyPair(unsigned char *pk, unsigned char *sk);

int isEmptyKey(unsigned char *pk, int keySize);

/*
 * createInvite - Create a Pub invite in Base 64 format
 *        inviteB64   - invite
 *        inviteSize  - Must be PUB_INVITE_SIZE_B64, otherwise , return error
 *                      Suggestion: don't use a constant in this argument, use sizeof(<invite argument>)
 * 
 * returns
 *        CRYPTO_OK        Everything is ok
 *        CRYPTO_FAIL      Any other error hapens
 * 
 */
int createInvite(unsigned char *inviteB64, size_t inviteSize);

// Encryption functions

void createNonceEncrypt(unsigned char* nonce);
void createNonceSign(unsigned char* nonce);

int encryptMsg(unsigned char *flatMsg, size_t msgLen, unsigned char *encryptMsg,
               unsigned char *recipientPK, unsigned char *senderSK, unsigned char *nonce);

int decryptMsg(unsigned char *flatMsg, size_t msgLen, unsigned char *encryptMsg,
               unsigned char *senderPK, unsigned char *recipientSK, unsigned char *nonce);

int signMsg(unsigned char* message, unsigned char* nonceSign, unsigned char* sk, unsigned char* signature, size_t maxMsgSize);
int verifySignature(unsigned char* message, unsigned char* nonceSign, unsigned char *pk, unsigned char* signature, size_t maxMsgSize);

// Base 64 functions

void bin2b64(char *destB64, size_t sizeB64, unsigned char *bin, size_t sizeBin);
void b642bin(unsigned char *destBin, size_t sizeBin, char *b64, size_t sizeB64);

// Hex Functions
void bin2hex(char *destHex, unsigned char *srcBin, size_t sizeBin);

// hash Functions
void hashSHA256(unsigned char *out, unsigned char *in, size_t sizeIn);

int isEmptyKey(unsigned char *pk, int keySize);

const char *strPk2Hex(char *destHex, unsigned char *pk);
const char *strSk2Hex(char *destHex, unsigned char *sk);
const char *strSignature2Hex(char *destHex, unsigned char *signature);

#endif /* CRYPTOGRAPHY_H */