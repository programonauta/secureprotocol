#ifndef JSON_H
#define JSON_H

#include <jansson.h>

// JSON Functions
json_t *loadJSON(const char *text);
int jsonGetInt(json_t* jsonObj, char* key, int* err);
const char* jsonGetString(json_t* jsonObj, char* key, int* err);


#endif