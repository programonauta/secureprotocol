#include "json.h"


json_t *loadJSON(const char *text)
{
  json_t *root;
  json_error_t error;

  root = json_loads(text, 0, &error);

  if (root)
  {
    return root;
  }
  else
  {
    fprintf(stderr, "[%s] json error on line %d: %s\n", __func__, error.line, error.text);
    return (json_t *)0;
  }
}

int jsonGetInt(json_t* jsonObj, char* key, int* err)
{
  json_t* jsonObjAux;

  jsonObjAux = json_object_get(jsonObj, key);
  if (jsonObjAux == NULL)
  {
    fprintf(stderr, "[%s] There is not key (%s) in JSON Object\n", __func__, key);
    *err = 1;
    return 0;
  }

  *err = 0;
  if (json_typeof(jsonObjAux) == JSON_INTEGER)
    return json_integer_value(jsonObjAux);
  else
  {
    fprintf(stderr, "[%s] Key (%s) is not an integer:\n", __func__, key);
    *err = 1;
    return 0;
  }
}

const char* jsonGetString(json_t* jsonObj, char* key, int* err)
{
  json_t* jsonObjAux;

  jsonObjAux = json_object_get(jsonObj, key);
  if (jsonObjAux == NULL)
  {
    fprintf(stderr, "[%s] There is not key (%s) in JSON Object\n", __func__, key);
    *err = 1;
    return "";
  }

  *err = 0;
  if (json_typeof(jsonObjAux) == JSON_STRING)
    return json_string_value(jsonObjAux);
  else
  {
    fprintf(stderr, "[%s] Key (%s) is not a string\n", __func__, key);
    *err = 1;
    return "";
  }
}
