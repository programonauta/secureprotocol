/**
 * @file dataStructure.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Header to define data strucutres
 * @version 0.1
 * @date 29-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H

#include <string.h>
#include "cryptography.h"

// --------------------------------------------------------------
// Node Data Structure
// --------------------------------------------------------------

/**
 * @brief Node Structure
 * 
 */
typedef struct
{
  unsigned char pk[PUBLICKEY_SIZE];
  unsigned char sk[SECRETKEY_SIZE];
  unsigned char pkB64[PUBLICKEY_SIZE_B64];
  unsigned char skB64[SECRETKEY_SIZE_B64];
  char nodeType;
} node_t;

void nodeBin2B64(node_t *node);
void nodeB642Bin(node_t *node);

int verifyNode(node_t *node);

/**
 * @brief Node Type Enum
 * 
 */
enum nodeType
{
  NODE_REGULAR,
  NODE_PUB
};

/**
 * @brief Node Returns Enum
 * 
 */
enum nodeReturns
{
  NODE_OK,
  NODE_FAIL,
  NODE_INVALID_KEY,
  NODE_EMPTY_KEY,
  NODE_INVALIDE_KEY_SIZE
};

// --------------------------------------------------------------
// Pubs Data Structure
// Invite and Tickets
// --------------------------------------------------------------

#define PUB_INVITE_SIZE crypto_box_NONCEBYTES
#define PUB_INVITE_SIZE_B64 (((4 * PUB_INVITE_SIZE / 3) + 3) & ~3) + 1

/**
 * @brief Invites Issued Structure
 * 
 */
typedef struct
{
  unsigned char inviteB64[PUB_INVITE_SIZE_B64];
  unsigned char nodePk[PUBLICKEY_SIZE]; // Node that received the issue
  char inviteStatus;
} inviteIssued_t;

enum inviteReturns
{
  INVITE_OK,
  INVITE_FAIL,
  INVITE_INVALID_SIZE,
  INVITE_INVALID_KEY_SIZE
};

#define TICKET_SIZE SIGNATURE_SIZE
#define TICKET_SIZE_B64 SIGNATURE_SIZE_B64

/**
 * @brief My Pub Structure
 * 
 */
typedef struct
{
  unsigned char pubPk[PUBLICKEY_SIZE];
  unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
  unsigned char pubInvite[PUB_INVITE_SIZE_B64];
  char inviteStatus;
  unsigned char pubTicket[TICKET_SIZE];
  unsigned char pubTicketB64[TICKET_SIZE_B64];
} myPub_t;

void myPubBin2B64(myPub_t *myPub);
void myPubB642Bin(myPub_t *myPub);

/**
 * @brief Invite Status Enum
 * 
 */
enum inviteStatus
{
  INVITE_CREATED,
  INVITE_GRANTED,
  INVITE_DENIED_ENCRYPT,
  INVITE_DENIED_NOT_FOUND,
  INVITE_DENIED_OTHER_NODE,
  INVITE_DENIED_ALREADY_GRANTED, // This status only for Return message
  INVITE_DENIED_PREVIOUSLY,      // This status only for Return message
  INVITE_RECEIVED_BY_NODE        // This status is only used on node
};

const char *getStrInviteStatus(int statusInvite);

// --------------------------------------------------------------
// Feeds Data Structure
//  - Feed Header
//  - Message
//  - Message Block
// --------------------------------------------------------------

// Define of max message size
#define MAX_MESSAGE_SIZE 2048

#define MESSAGE_SIZE 128
#define MESSAGE_SIZE_B64 (((4 * MESSAGE_SIZE / 3) + 3) & ~3) + 1

// Size of sequence string
#define STRING_SEQUENCE_SIZE 9

// size of string to be hashed (MsgBlock)
#define STRING_BLOCK_SIZE HASH_SIZE_B64 + STRING_SEQUENCE_SIZE + PUBLICKEY_SIZE_B64 + \
                              1 + NONCE_MSG_SIZE_B64 + MESSAGE_SIZE_B64 + SIGNATURE_SIZE_B64 + 1
/**
 * @brief Feed Header Structure
 * 
 */
typedef struct
{
  unsigned char senderPk[PUBLICKEY_SIZE];
  unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
  unsigned char pubPk[PUBLICKEY_SIZE];
  unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
  unsigned char pubTicket[SIGNATURE_SIZE];
  unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
} feedHeader_t;

void feedHeaderB642Bin(feedHeader_t *feedHeader);
void feedHeaderBin2B64(feedHeader_t *feedHeader);

/**
 * @brief Feed Detail Structure
 * 
 */
typedef struct
{
  unsigned char senderPk[PUBLICKEY_SIZE];
  unsigned char senderPkB64[PUBLICKEY_SIZE_B64];
  unsigned char pubPk[PUBLICKEY_SIZE];
  unsigned char pubPkB64[PUBLICKEY_SIZE_B64];
  unsigned char pubTicket[SIGNATURE_SIZE];
  unsigned char pubTicketB64[SIGNATURE_SIZE_B64];
  unsigned long lastSequence;
  unsigned char lastHASH[HASH_SIZE];
  unsigned char lastHASHB64[HASH_SIZE_B64];
} feedDetail_t;

void feedDetailB642Bin(feedDetail_t *feedDetail);
void feedDetailBin2B64(feedDetail_t *feedDetail);

/**
 * @brief Message Block Structure
 * 
 * Order to populate a message box
 * 1. Inform Recipient, Previous Hash and Sequence
 *    1.1 - Verifify if size of recipient PK is valid
 * 2. Create a nonce
 * 3. Upudate Message
 *    3.1 - If private message, encryptMsgBlock provinding sender SK
 *    3.2 - The function will:
 *          - set tagMsg with 1
 *          - encrypt message using nonce
 *    3.3 - If public message, just store the message
 * 4. Sign the message using nonce
 *    4.1 - Sign must consider previousHASH and sequence
 * 5. Calculate all B64 msgBlockBin2B64
 * 6. Calculate Block Hash
 * 
 */
typedef struct
{
  unsigned char previousHASH[HASH_SIZE];
  unsigned char previousHASHB64[HASH_SIZE_B64];
  unsigned long sequence;
  unsigned char recipientPk[PUBLICKEY_SIZE];
  unsigned char recipientPkB64[PUBLICKEY_SIZE_B64];
  unsigned char tagMsg;
  unsigned char nonceSign[NONCE_MSG_SIZE];
  unsigned char nonceSignB64[NONCE_MSG_SIZE_B64];
  unsigned char message[MESSAGE_SIZE];
  unsigned char messageB64[MESSAGE_SIZE_B64];
  unsigned char signature[SIGNATURE_SIZE];
  unsigned char signatureB64[SIGNATURE_SIZE_B64];
  unsigned char blockHASH[HASH_SIZE];
  unsigned char blockHASHB64[HASH_SIZE_B64];
  unsigned long confirmations;
} msgBlock_t;

/**
 * @brief Message Block Returns
 * 
 */
enum msgBlockReturns_e
{
  MSG_BLOCK_OK,
  MSG_BLOCK_FAIL,
  MSG_BLOCK_INVALID_SIGNATURE,
  MSG_BLOCK_INVALID_ENCRYPT,
  MSG_BLOCK_INVALID_HASH,
  MSG_BLOCK_INVALID_PREVIOUS_HASH,
  MSG_BLOCK_INVALID_SEQUENCE,
  MSG_BLOCK_NULL_MSG
};

void resetMsgBlock(msgBlock_t *msgBlock);

void msgBlock2StrBlock(msgBlock_t *msgBlock, unsigned char *strBlock);

void msgBlockBin2B64(msgBlock_t *msgBlock);
void msgBlockB642Bin(msgBlock_t *msgBlock);

int createMsgBlock(msgBlock_t *msgBlock, node_t *node);

int signMsgBlock(msgBlock_t *msgBlock, unsigned char *sk);
int verifySignMsgBlock(msgBlock_t *msgBlock, unsigned char *pk);

int encryptMsgBlock(msgBlock_t *msgBlock, unsigned char *senderSk);
int decryptMsgBlock(msgBlock_t *msgBlock, unsigned char *senderPk, unsigned char *recipientSk);

int hashMsgBlock(msgBlock_t *msgBlock);
int verifyHashMsgBlock(msgBlock_t *msgBlock);

#endif