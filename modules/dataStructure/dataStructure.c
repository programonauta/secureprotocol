/**
 * @file dataStructure.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Functions to deal with data Structure
 * @version 0.1
 * @date 29-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "dataStructure.h"

/*************************************************
 * FUNCTIONS for data structure node
 **************************************************/

int verifyNode(node_t *node)
{
  int rc = verifyKeyPair(node->pk, node->sk);
  return (rc == CRYPTO_OK ? NODE_OK : NODE_FAIL);
}

void nodeBin2B64(node_t *node)
{
  bin2b64(node->pkB64, PUBLICKEY_SIZE_B64, node->pk, PUBLICKEY_SIZE);
  bin2b64(node->skB64, SECRETKEY_SIZE_B64, node->sk, SECRETKEY_SIZE);
}

void nodeB642Bin(node_t *node)
{
  b642bin(node->pk, PUBLICKEY_SIZE, node->pkB64, PUBLICKEY_SIZE_B64);
  b642bin(node->sk, SECRETKEY_SIZE, node->skB64, SECRETKEY_SIZE_B64);
}

/*************************************************
 * FUNCTIONS for data structure inviteIssued
 **************************************************/

/**
 * @brief Create a Invite randomly
 * 
 * @param inviteB64 
 * @param inviteSize 
 * @return int 
 */
int createInvite(unsigned char *inviteB64, size_t inviteSize)
{
  unsigned char inviteBinary[PUB_INVITE_SIZE]; // invite in Binary format

  if (inviteSize != PUB_INVITE_SIZE_B64)
  {
    fprintf(stderr, "ERROR: Invite with wrong size"); // TODO Call generic msg function
    return (CRYPTO_FAIL);
  }

  randombytes_buf(inviteBinary, sizeof(inviteBinary));

  sodium_bin2base64((char *)inviteB64, PUB_INVITE_SIZE_B64, inviteBinary, PUB_INVITE_SIZE, sodium_base64_VARIANT_ORIGINAL);

  return CRYPTO_OK;
}

/**
 * @brief Return a string that describe the invite Status
 * 
 * @param statusInvite 
 * @return const char* 
 */
const char *getStrInviteStatus(int statusInvite)
{
  switch (statusInvite)
  {

  case INVITE_CREATED:
    return "CREATED            ";
  case INVITE_GRANTED:
    return "GRANTED            ";
  case INVITE_DENIED_ENCRYPT:
    return "DENIED ENCRYPTION  ";
  case INVITE_DENIED_NOT_FOUND:
    return "DENIED NOT FOUND   ";
  case INVITE_DENIED_OTHER_NODE:
    return "DENIED OTHER NODE  ";
  case INVITE_DENIED_ALREADY_GRANTED:
    return "ALREADY GRANTED    ";
  case INVITE_DENIED_PREVIOUSLY:
    return "DENIED PREVIOUSLY  ";
  case INVITE_RECEIVED_BY_NODE:
    return "RECEIVED           ";
  default:
    return "UNKNOWN CODE       ";
  }
}

/*************************************************
 * FUNCTIONS for data structure myPub
 **************************************************/
void myPubBin2B64(myPub_t *myPub)
{
  bin2b64((char *)myPub->pubPkB64, sizeof(myPub->pubPkB64), myPub->pubPk, sizeof(myPub->pubPk));
  bin2b64((char *)myPub->pubTicketB64, sizeof(myPub->pubTicketB64), myPub->pubTicket, sizeof(myPub->pubTicket));
}

void myPubB642Bin(myPub_t *myPub)
{
  b642bin(myPub->pubPk, sizeof(myPub->pubPk), myPub->pubPkB64, sizeof(myPub->pubPkB64));
  b642bin(myPub->pubTicket, sizeof(myPub->pubTicket), myPub->pubTicketB64, sizeof(myPub->pubTicketB64));
}

/*************************************************
 * FUNCTIONS for data structure Feeds
 **************************************************/

/*************************************************
 * FUNCTIONS for feedHeader table
 **************************************************/

void feedHeaderB642Bin(feedHeader_t *feedHeader)
{
  b642bin(feedHeader->pubPk, sizeof(feedHeader->pubPk),
          feedHeader->pubPkB64, sizeof(feedHeader->pubPkB64));
  b642bin(feedHeader->senderPk, sizeof(feedHeader->senderPk),
          feedHeader->senderPkB64, sizeof(feedHeader->senderPkB64));
  b642bin(feedHeader->pubTicket, sizeof(feedHeader->pubTicket),
          feedHeader->pubTicketB64, sizeof(feedHeader->pubTicketB64));
}

void feedHeaderBin2B64(feedHeader_t *feedHeader)
{
  bin2b64(feedHeader->pubPkB64, sizeof(feedHeader->pubPkB64),
          feedHeader->pubPk, sizeof(feedHeader->pubPk));
  bin2b64(feedHeader->senderPkB64, sizeof(feedHeader->senderPkB64),
          feedHeader->senderPk, sizeof(feedHeader->senderPk));
  bin2b64(feedHeader->pubTicketB64, sizeof(feedHeader->pubTicketB64),
          feedHeader->pubTicket, sizeof(feedHeader->pubTicket));
}

void feedDetailB642Bin(feedDetail_t *feedDetail)
{
  b642bin(feedDetail->pubPk, sizeof(feedDetail->pubPk),
          feedDetail->pubPkB64, sizeof(feedDetail->pubPkB64));
  b642bin(feedDetail->senderPk, sizeof(feedDetail->senderPk),
          feedDetail->senderPkB64, sizeof(feedDetail->senderPkB64));
  b642bin(feedDetail->pubTicket, sizeof(feedDetail->pubTicket),
          feedDetail->pubTicketB64, sizeof(feedDetail->pubTicketB64));
  b642bin(feedDetail->lastHASH, sizeof(feedDetail->lastHASH),
          feedDetail->lastHASHB64, sizeof(feedDetail->lastHASHB64));
}

void feedDetailBin2B64(feedDetail_t *feedDetail)
{
  bin2b64(feedDetail->pubPkB64, sizeof(feedDetail->pubPkB64),
          feedDetail->pubPk, sizeof(feedDetail->pubPk));
  bin2b64(feedDetail->senderPkB64, sizeof(feedDetail->senderPkB64),
          feedDetail->senderPk, sizeof(feedDetail->senderPk));
  bin2b64(feedDetail->pubTicketB64, sizeof(feedDetail->pubTicketB64),
          feedDetail->pubTicket, sizeof(feedDetail->pubTicket));
  bin2b64(feedDetail->lastHASHB64, sizeof(feedDetail->lastHASHB64),
          feedDetail->lastHASH, sizeof(feedDetail->lastHASH));
}

/*************************************************
 * FUNCTIONS for msgBlock table
 **************************************************/

int createMsgBlock(msgBlock_t *msgBlock, node_t *node)
{
  int rc = MSG_BLOCK_OK;
  if (msgBlock->tagMsg)
    rc = encryptMsgBlock(msgBlock, node->sk);

  if (rc != MSG_BLOCK_OK) 
    return MSG_BLOCK_FAIL;

  rc = signMsgBlock(msgBlock, node->sk);

  rc = hashMsgBlock(msgBlock);

  return MSG_BLOCK_OK;

}

void msgBlock2StrBlock(msgBlock_t *msgBlock, unsigned char *strBlock)
{

  char prvHashB64[HASH_SIZE_B64]; // TODO Remove this block
  char sequenceB64[STRING_SEQUENCE_SIZE];
  char receiverB64[PUBLICKEY_SIZE_B64];
  char nonceSignB64[NONCE_MSG_SIZE_B64];
  char msgB64[MESSAGE_SIZE_B64];
  char signatureB64[SIGNATURE_SIZE_B64];

  // Clear strBlock and message B64

  memset(strBlock, 0, STRING_BLOCK_SIZE);
  memset(msgB64, 0, MESSAGE_SIZE_B64);

  msgBlockBin2B64(msgBlock);

  sprintf(sequenceB64, "%lu", msgBlock->sequence);

  strcat((char *)strBlock, msgBlock->previousHASHB64);
  strcat((char *)strBlock, sequenceB64);
  strcat((char *)strBlock, msgBlock->recipientPkB64);
  strcat((char *)strBlock, msgBlock->tagMsg ? "1" : "0");
  strcat((char *)strBlock, msgBlock->nonceSignB64);
  strcat((char *)strBlock, msgBlock->messageB64);
  strcat((char *)strBlock, msgBlock->signatureB64);
}

void msgBlockB642Bin(msgBlock_t *msgBlock)
{
  memset(msgBlock->message, 0, sizeof(msgBlock->message));

  b642bin(msgBlock->previousHASH, HASH_SIZE,
          msgBlock->previousHASHB64, HASH_SIZE_B64);
  b642bin(msgBlock->recipientPk, PUBLICKEY_SIZE,
          msgBlock->recipientPkB64, PUBLICKEY_SIZE_B64);
  b642bin(msgBlock->nonceSign, NONCE_MSG_SIZE,
          msgBlock->nonceSignB64, NONCE_MSG_SIZE_B64);
  b642bin(msgBlock->message, MESSAGE_SIZE,
          msgBlock->messageB64, strlen((char *)msgBlock->messageB64));
  b642bin(msgBlock->signature, SIGNATURE_SIZE,
          msgBlock->signatureB64, SIGNATURE_SIZE_B64);
  b642bin(msgBlock->blockHASH, HASH_SIZE,
          msgBlock->blockHASHB64, HASH_SIZE_B64);
}

void msgBlockBin2B64(msgBlock_t *msgBlock)
{
  memset(msgBlock->messageB64, 0, sizeof(msgBlock->messageB64));

  bin2b64(msgBlock->previousHASHB64, HASH_SIZE_B64,
          msgBlock->previousHASH, HASH_SIZE);
  bin2b64(msgBlock->recipientPkB64, PUBLICKEY_SIZE_B64,
          msgBlock->recipientPk, PUBLICKEY_SIZE);
  bin2b64(msgBlock->nonceSignB64, NONCE_MSG_SIZE_B64,
          msgBlock->nonceSign, NONCE_MSG_SIZE);
  bin2b64(msgBlock->messageB64, MESSAGE_SIZE_B64,
          msgBlock->message, strlen((char *)msgBlock->message));
  bin2b64(msgBlock->signatureB64, SIGNATURE_SIZE_B64,
          msgBlock->signature, SIGNATURE_SIZE);
  bin2b64(msgBlock->blockHASHB64, HASH_SIZE_B64,
          msgBlock->blockHASH, HASH_SIZE);
}

int hashMsgBlock(msgBlock_t *msgBlock)
{

  unsigned char strBlock[STRING_BLOCK_SIZE];

  msgBlock2StrBlock(msgBlock, strBlock);

  hashSHA256(msgBlock->blockHASH, (unsigned char *)strBlock, strlen((char *)strBlock));

   msgBlockBin2B64(msgBlock);

  return MSG_BLOCK_OK;
}

/**
 * @brief Verify HASH from a MsgBlock
 * 
 * @param msgBlock 
 * @return int MSG_BLOCK_OK, MSG_BLOCK_INVALID_HASH
 */
int verifyHashMsgBlock(msgBlock_t *msgBlock)
{

  unsigned char strBlock[STRING_BLOCK_SIZE];
  unsigned char auxHash[HASH_SIZE];

  msgBlock2StrBlock(msgBlock, strBlock);

  hashSHA256(auxHash, (unsigned char *)strBlock, strlen((char *)strBlock));

  if (memcmp(auxHash, msgBlock->blockHASH, HASH_SIZE) == 0)
    return MSG_BLOCK_OK;
  else
    return MSG_BLOCK_INVALID_HASH;

}

int encryptMsgBlock(msgBlock_t *msgBlock, unsigned char *senderSk)
{
  int rc;
  unsigned char msgAux[MESSAGE_SIZE];

  /*
  rc = encryptMsg(flatMsg, msgLen, msgAux, msgBlock->recipient, sk);
  if (rc != CRYPTO_OK)
    return CONTROL_FAIL;
  
#ifdef DEBUG
  int i;
  printf("[%s] - Message Encrpyted (%llu bytes = %llu + %d)\n", __func__, msgLen+TAG_SIZE, msgLen, TAG_SIZE);
  for (i=0; i< msgLen+TAG_SIZE; i++)
    printf("%02x", msgAux[i]);
  printf("\n");
#endif

  msgBlock->tagMsg = 1; // Private Message
  
  bin2b64((char*)msgBlock->message, MESSAGE_SIZE_B64, msgAux, msgLen + TAG_SIZE);

#ifdef DEBUG
  printf("[%s] - Message Encrpyted (Base64): %s\n", __func__, msgBlock->message);
#endif
  
  return CONTROL_OK;
  */
  return MSG_BLOCK_OK; // TODO Write Encrpyt Message 
}

int decryptMsgBlock(msgBlock_t *msgBlock, unsigned char *senderPk, unsigned char *recipientSk)
{
  int rc;
  unsigned char msgAux[MESSAGE_SIZE]; // Tag + Message

  int padChar = 0;

  // First Calculate the size of Base 64
  size_t msgSize = strlen((char *)msgBlock->message);

  if (msgBlock->message[msgSize - 1] == '=')
    ++padChar;

  if (msgBlock->message[msgSize - 2] == '=')
    ++padChar;

  msgSize = (msgSize / 4 * 3) - padChar;

  //sodium_base642bin(msgAux, msgSize, (char*)msgBlock->message, strlen((char*)msgBlock->message+1),
  //      "\n\r ", NULL, NULL, sodium_base64_VARIANT_ORIGINAL);
  b642bin(msgAux, msgSize, (char *)msgBlock->message, strlen((char *)msgBlock->message + 1));

#ifdef DEBUG
  int i;
  printf("[%s] - Message to be Decrypted (Base64 - Len: %llu) %s\n", __func__, msgSize, msgBlock->message);
  printf("[%s] - Message to be Decrypted (%llu bytes)\n", __func__, msgSize);
  for (i = 0; i < msgSize; i++)
    printf("%02x", msgAux[i]);
  printf("\n");
#endif

  /*
  rc = decryptMsg(flatMsg, (msgSize-TAG_SIZE), msgAux, senderPk, recipientSk);
  if (rc != CRYPTO_OK)
    return CONTROL_FAIL;
  else
    return CONTROL_OK;
    */
  return 0; // TODO Rewrite
}

int signMsgBlock(msgBlock_t *msgBlock, unsigned char *sk)
{
  int rc;

  char strMessage[MESSAGE_SIZE_B64 + HASH_SIZE_B64];

  msgBlockBin2B64(msgBlock);

  memset(strMessage, 0, sizeof(strMessage));

  strcat((char *)strMessage, msgBlock->messageB64);
  strcat((char *)strMessage, msgBlock->previousHASHB64);

  if (strlen((char *)msgBlock->message) == 0)
    return MSG_BLOCK_NULL_MSG;

  createNonceSign(msgBlock->nonceSign);

  rc = signMsg(strMessage, msgBlock->nonceSign, sk, msgBlock->signature, strlen(strMessage));

  if (rc == CRYPTO_OK)
    return MSG_BLOCK_OK;
  else
    return MSG_BLOCK_FAIL;
}

/**
 * @brief Verify the signature of a msgBlock
 * 
 * @param msgBlock 
 * @param pk 
 * @return int MSG_BLOCK_OK, MSG_BLOCK_INVALID_SIGNATURE, MSG_BLOCK_NULL_MSG
 */
int verifySignMsgBlock(msgBlock_t *msgBlock, unsigned char *pk)
{
  int rc;

  char strMessage[MESSAGE_SIZE_B64 + HASH_SIZE_B64];

  msgBlockBin2B64(msgBlock);

  memset(strMessage, 0, sizeof(strMessage));

  strcat((char *)strMessage, msgBlock->messageB64);
  strcat((char *)strMessage, msgBlock->previousHASHB64);

  if (strlen((char *)msgBlock->message) == 0)
    return MSG_BLOCK_NULL_MSG;

  rc = verifySignature(strMessage, msgBlock->nonceSign, pk, msgBlock->signature, strlen(strMessage));
  
  return (rc == 0 ? MSG_BLOCK_OK : MSG_BLOCK_INVALID_SIGNATURE);
}
