/**
 * @file printColors.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 31-Aug-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef PRINTCOLORS_H
#define PRINTCOLORS_H

/*
char red[] = "\e[31m";
char orange[] = "\e[38;5;202m";
char brown[] = "\e[38;5;130m";
char yellow[] = "\e[93m";
char puke[] = "\e[33m";
char lime[] = "\e[92m";
char green[] = "\e[32m";
char aqua[] = "\e[96m";
char sky[] = "\e[94m";
char magenta[] = "\e[35m";
char reset[] = "\e[0m";
*/

#define TEXT_RED "\e[31m"
#define TEXT_ORANGE "\e[38;5;202m"
#define TEXT_BROWN "\e[38;5;130m"
#define TEXT_YELLOW "\e[93m"
#define TEXT_PUKE "\e[33m"
#define TEXT_LIME "\e[92m"
#define TEXT_GREEN "\e[32m"
#define TEXT_AQUA "\e[96m"
#define TEXT_SKY "\e[94m"
#define TEXT_MAGENTA "\e[35m"
#define COLOR_RESET "\e[0m"

#endif