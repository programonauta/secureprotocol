/**
 * @file persistence.c
 * @author Ricardo Brandão (rbrandao@protonmail.com)
 * @brief Functions to deal with databases
 * @version 0.1
 * @date 18-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "persistence.h"

extern sqlite3 *dbFeeds;
extern sqlite3 *dbNode;
extern sqlite3 *dbPubs;
extern char verbose;

void showDbMsg(int msgType, const char *funcName, sqlite3 *dbConn, char *customMsg)
{
  switch (msgType)
  {
  case MSG_ERROR:
    fprintf(stderr, "[%s] [%s] DB ERROR: (%d) %s\n\t%s\n",
            funcName,
            sqlite3_db_filename(dbConn, "main"),
            sqlite3_errcode(dbConn),
            sqlite3_errmsg(dbConn),
            customMsg);
    break;
  case MSG_WARNING:
    fprintf(stderr, "[%s] [%s] DB WARNING: %s\n",
            funcName,
            sqlite3_db_filename(dbConn, "main"),
            customMsg);
    break;
  default:
    fprintf(stdout, "[%s] [%s] DB MESSAGE: %s\n",
            funcName,
            sqlite3_db_filename(dbConn, "main"),
            customMsg);
    break;
  }
  return;
}

/**
 * @brief Open and verify databases
 * 
 * @param errCode address to get error code
 * @param errMsg address to get message error
 * @return int PERSISTENCE_OK if success
 */
int persistenceInit()
{

  int rc; // Return Code

  rc = dbNodeInit();
  if (rc != PERSISTENCE_OK)
    return PERSISTENCE_ERR_OPEN;

  rc = dbPubsInit();
  if (rc != PERSISTENCE_OK)
    return PERSISTENCE_ERR_OPEN;

  rc = dbFeedsInit(); 
  if (rc != PERSISTENCE_OK)
    return PERSISTENCE_ERR_OPEN;

  return PERSISTENCE_OK;
}

int closeDatabase(sqlite3 *dbase)
{
  int rc = 0;
  rc += sqlite3_close(dbase);
  return rc;
}

int persistenceFinish()
{
  int rc = 0, rcSum = 0;

  rc = closeDatabase(dbNode);
  rcSum += rc;

  if (rc != 0)
    showDbMsg(MSG_ERROR, __func__, dbNode, "During close database <node>");
#ifdef DEBUG
  else
    showDbMsg(MSG_LOG, __func__, dbNode, "Database <node> Closed Successfully");
#endif

  rc = closeDatabase(dbPubs);
  rcSum += rc;

  if (rc != 0)
    showDbMsg(MSG_ERROR, __func__, dbNode, "During close database <pubs>");
#ifdef DEBUG
  else
    showDbMsg(MSG_LOG, __func__, dbNode, "Database <pubs> Closed Successfully");
#endif

  rc = closeDatabase(dbFeeds);

  if (rc != 0)
    showDbMsg(MSG_ERROR, __func__, dbNode, "During close database <feeds>");
#ifdef DEBUG
  else
    showDbMsg(MSG_LOG, __func__, dbNode, "Database <feeds> Closed Successfully");
#endif

  rcSum += rc;

  if (rc == 0)
    return PERSISTENCE_OK;
  else
    return PERSISTENCE_ERR_CLOSE;
}

