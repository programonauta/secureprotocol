/**
 * @file dbKeys.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Header of functions to deal with dbKeys database
 * @version 0.1
 * @date 28-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef DBNODE_H
#define DBNODE_H

#include "dataStructure.h"

int dbNodeInit();

int insertNode(node_t *node);

int readNode(node_t *node);

#endif