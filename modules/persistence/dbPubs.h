/**
 * @file dbPubs.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Header of functions to deal with dbPubs database
 * @version 0.1
 * @date 01-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef DBPUBS_H
#define DBPUBS_H

#include "persistence.h"

int dbPubsInit();

int insertInviteIssued(inviteIssued_t *invite, int *sequence);
int readInviteIssued(inviteIssued_t *invite);
int updateInviteIssued(inviteIssued_t *invite);

int prepareInviteIssued(sqlite3_stmt **pStmt);
int nextInviteIssued(sqlite3_stmt *pStmt, inviteIssued_t *inviteIssued);

int insertMyPub(myPub_t *myPub);
int readMyPub(myPub_t *myPub);
int updateMyPub(myPub_t *myPub);

int prepareMyPub(sqlite3_stmt **pStmt);
int prepareMyPubByPub(sqlite3_stmt **pStmt, myPub_t *myPub);
int nextMyPub(sqlite3_stmt *pStmt, myPub_t *myPub);

int verifyMyPub();

#endif