/**
 * @file dbFeeds.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 01-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef DBFEEDS_H
#define DBFEEDS_H

int dbFeedsInit();
int insertFeedHeader(feedHeader_t *feedHeader);
int readFeedHeader(inviteIssued_t *feedHeader);
int verifyFeedHeader(feedHeader_t *feedHeader);
int prepareFeedHeader(sqlite3_stmt **pStmt);
int nextFeedHeader(sqlite3_stmt *pStmt, feedHeader_t *feedHeader);
int getTicketFromFeedHeader(feedHeader_t *feedHeader);

int readMsgBlock(char *tableName, msgBlock_t *msgBlock);
int getLastMsgBlock(char *tableName, msgBlock_t *msgBlock);
int insertMsgBlock(char *tableName, msgBlock_t *msgBlock);
int prepareMsgBlockBySequence(feedDetail_t *feedDetail, sqlite3_stmt **pStmt);
int nextMsgBlock(sqlite3_stmt *pStmt, msgBlock_t *msgBlock);
int updateMsgBlock(char *tableName, msgBlock_t *ptrMsgBlock);
int verifyMsgBlock(msgBlock_t *ptrMsgBlock, feedDetail_t *ptrFeedDetail);
int getLastFeedDetail(feedDetail_t *feedDetail);

#define SIZE_TABLE_NAME 35
const char *feedTableName(char *tableName, unsigned char *pub, unsigned char *sender);

#endif