/**
 * @file persistence.h
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Header file of Persistence variables and functions
 * @version 0.1
 * @date 28-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef PERSISTENCE_H
#define PERSISTENCE_H
#include <sqlite3.h>
#include <stdio.h>
#include <string.h>

#include "dbNode.h"
#include "dbPubs.h"
#include "dbFeeds.h"
#include "cryptography.h"
#include "dataStructure.h"

enum persistenceReturns_e
{
    // Global returns
    PERSISTENCE_OK,
    PERSISTENCE_ERR_GENERAL,
    PERSISTENCE_ERR_OPEN,
    PERSISTENCE_ERR_CLOSE,
    PERSISTENCE_ERR_BEGIN,
    PERSISTENCE_ERR_CREATE_TAB,
    PERSISTENCE_ERR_INSERT,
    PERSISTENCE_ERR_READ,
    PERSISTENCE_ERR_UPDATE,
    PERSISTENCE_ERR_DELETE,
    PERSISTENCE_ERR_COMMIT,
    PERSISTENCE_ERR_EXECUTE,
    PERSISTENCE_NO_ROWS,

};

enum msgType_e
{
    MSG_ERROR,
    MSG_WARNING,
    MSG_LOG
};


// this converts to string
#define STR_(X) #X
// this makes sure the argument is expanded before converting to string
#define STR(X) STR_(X)

void showDbMsg(int msgType, const char* funcName, sqlite3* dbConn, char* customMsg);

int persistenceInit();

int persistenceFinish();

//int insertInviteAff(unsigned char *invite, size_t inviteSize, unsigned char *pubPublicKeyB64, size_t publicKeySize);

#endif /* PERSISTENCE_H */
