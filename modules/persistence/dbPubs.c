/**
 * @file dbPubs.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Functions to deal with dbPubs Database
 * @version 0.1
 * @date 01-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "dbPubs.h"

sqlite3 *dbPubs = NULL;

/**
 * @brief Initialize dbPubs database, verify and create tables
 * 
 * @return int Enum Persistence Message Returns
 */
int dbPubsInit()
{
    int rc = 0;
    const char *pubsDBName = "pubs.db";

    const char *sqlCreateTableInvitesIssued =
        "Create Table if not exists invitesIssued ("
        " invite text, "
        " nodePk blob, "
        " inviteStatus integer "
        " )";

    const char *sqlCreateTableMyPubs =
        "Create Table if not exists myPubs ("
        " pubPk blob, "
        " pubInvite text, "
        " inviteStatus integer, "
        " pubTicket blob "
        " )";

    rc = sqlite3_open(pubsDBName, &dbPubs);
    if (rc)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, "Trying to open database");
        return PERSISTENCE_ERR_OPEN;
    }

#ifdef DEBUG
    showDbMsg(MSG_LOG, __func__, dbPubs, "Opened database successfully");
#endif

    rc = sqlite3_exec(dbPubs, sqlCreateTableInvitesIssued, NULL, 0, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlCreateTableInvitesIssued);
        return PERSISTENCE_ERR_CREATE_TAB;
    }

    rc = sqlite3_exec(dbPubs, sqlCreateTableMyPubs, NULL, 0, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlCreateTableMyPubs);
        return PERSISTENCE_ERR_CREATE_TAB;
    }

    return PERSISTENCE_OK;
}

int insertInviteIssued(inviteIssued_t *invite, int *sequence)
{
    sqlite3_stmt *pStmt;
    int rc;

    const char *errMsg;

    const char *sqlInsertTableInvites = "Insert into invitesIssued "
                                        "(invite, inviteStatus) "
                                        "values (?, ?)";
    const char *sqlCountTableInvites = "Select count(*) from invitesIssued";

    // dbPubs is not opened
    if (dbPubs == NULL)
    {
        rc = dbPubsInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Open Pubs db");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    // Start a Begin Transaction
    rc = sqlite3_exec(dbPubs, "BEGIN;", NULL, 0, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, "Begin Transaction");
        return PERSISTENCE_ERR_BEGIN;
    }
#ifdef DEBUG
    else
    {
        showDbMsg(MSG_LOG, __func__, dbPubs, "Begin Successfully");
    }
#endif

    rc = sqlite3_prepare_v2(dbPubs, sqlInsertTableInvites, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlInsertTableInvites);
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    sqlite3_bind_text(pStmt, 1, (char *)invite->inviteB64, sizeof(invite->inviteB64), SQLITE_STATIC);
    sqlite3_bind_int(pStmt, 2, INVITE_CREATED);

    rc = sqlite3_step(pStmt);

    if (rc != SQLITE_DONE)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlInsertTableInvites);
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_INSERT;
    }
#ifdef DEBUG
    else
    {
        showDbMsg(MSG_LOG, __func__, dbPubs, "Insert successfully");
    }
#endif

    rc = sqlite3_exec(dbPubs, "COMMIT;", NULL, 0, 0);
    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, "Commit Transaction");
        return PERSISTENCE_ERR_COMMIT;
    }
#ifdef DEBUG
    else
    {
        showDbMsg(MSG_LOG, __func__, dbPubs, "Commit successfully");
    }
#endif

    sqlite3_finalize(pStmt);

    // Read public and secret key
    rc = sqlite3_prepare_v2(dbPubs, sqlCountTableInvites, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, "Prepare Statement");
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    // read first row in the select
    rc = sqlite3_step(pStmt);

    if (rc == SQLITE_ROW) // There is record in the table
    {
        *sequence = sqlite3_column_int(pStmt, 0);
    }
    else if (rc != SQLITE_DONE)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, "During Gather the key");
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    sqlite3_finalize(pStmt);

    return PERSISTENCE_OK;
}

/**
 * @brief verify if invite was already imported to the database
 * 
 * @param invite invite structure to be populated
 * @return int PERSISTENCE_OK if ok
 */
int readInviteIssued(inviteIssued_t *invite)
{
    sqlite3_stmt *pStmt;
    int rc, ret;

    const char *sqlSearchInviteByPub =
        "Select invite, nodePk, inviteStatus "
        " from invitesIssued "
        " where invite = ?";

    // dbKeys is not opened
    if (dbPubs == NULL)
    {
        rc = persistenceInit();
        if (rc != PERSISTENCE_OK)
            return PERSISTENCE_ERR_OPEN;
    }

    // Read tag isPub
    rc = sqlite3_prepare_v2(dbPubs, sqlSearchInviteByPub, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
#ifdef DEBUG
        showDbMsg(MSG_ERROR, __func__, dbPubs, "Failed to prepare statement");
#endif
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    sqlite3_bind_text(pStmt, 1, (const char *)invite->inviteB64,
                      sizeof(invite->inviteB64), SQLITE_STATIC);

    // read first row in the select
    rc = sqlite3_step(pStmt);

    if (rc == SQLITE_ROW) // There is record in the table
    {
        strncpy((char *)invite->inviteB64, sqlite3_column_text(pStmt, 0), sizeof(invite->inviteB64));
        if (sqlite3_column_bytes(pStmt, 1) == sizeof(invite->nodePk))
            memcpy(invite->nodePk, sqlite3_column_blob(pStmt, 1), sizeof(invite->nodePk));
        else
            memset((char *)invite->nodePk, 0, sizeof(invite->nodePk));
        invite->inviteStatus = sqlite3_column_int64(pStmt, 2);
        ret = PERSISTENCE_OK;
    }
    else if (rc == SQLITE_DONE) // There is no row
        ret = PERSISTENCE_NO_ROWS;
    else if (rc != SQLITE_DONE)
    {
#ifdef DEBUG
        fprintf(stderr, "Error when gather the key. Code: %d\n", rc);
#endif
        ret = PERSISTENCE_ERR_READ;
    }

    sqlite3_finalize(pStmt);

    return ret;
}

int updateInviteIssued(inviteIssued_t *invite)
{
    sqlite3_stmt *pStmt;
    int rc;

    const char *sqlUpdateInvite =
        " Update invitesIssued set inviteStatus = %d, "
        " nodePk = ? "
        " where invite like \'%s\'";
    char sqlDynamic[strlen(sqlUpdateInvite) + PUB_INVITE_SIZE_B64 + 5];

    sprintf(sqlDynamic, sqlUpdateInvite, invite->inviteStatus, invite->inviteB64);

    // Read tag isPub
    rc = sqlite3_prepare_v2(dbPubs, sqlDynamic, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
#ifdef DEBUG
        showDbMsg(MSG_ERROR, __func__, dbPubs, "Failed to prepare statement");
#endif
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    sqlite3_bind_blob(pStmt, 1, invite->nodePk, sizeof(invite->nodePk), SQLITE_STATIC);

    rc = sqlite3_step(pStmt);

    if (rc != SQLITE_DONE)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlUpdateInvite);
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_UPDATE;
    }

    return PERSISTENCE_OK;
}

int prepareInviteIssued(sqlite3_stmt **pStmt)
{

    int rc;

    const char *sqlSelectInvites = "Select "
                                   "invite, nodePk, inviteStatus "
                                   "from invitesIssued ";

    // dbKeys is not opened
    if (dbPubs == NULL)
    {
        rc = persistenceInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Dbase Not Open");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    rc = sqlite3_prepare_v2(dbPubs, sqlSelectInvites, -1, pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlSelectInvites);
        sqlite3_finalize(*pStmt);
        return PERSISTENCE_ERR_READ;
    }

    return PERSISTENCE_OK;
}

int nextInviteIssued(sqlite3_stmt *pStmt, inviteIssued_t *inviteIssued)
{
    int rc = sqlite3_step(pStmt);
    int pkSize;

    memset(inviteIssued, 0, sizeof(*inviteIssued));

    if (rc == SQLITE_ROW)
    {

        memcpy(inviteIssued->inviteB64, sqlite3_column_text(pStmt, 0), sizeof(inviteIssued->inviteB64));
        if (sqlite3_column_bytes(pStmt, 1) == sizeof(inviteIssued->nodePk))
            memcpy(inviteIssued->nodePk, sqlite3_column_blob(pStmt, 1), sizeof(inviteIssued->nodePk));
        inviteIssued->inviteStatus = sqlite3_column_int(pStmt, 2);
        return PERSISTENCE_OK;
    }

    sqlite3_finalize(pStmt);

    return PERSISTENCE_NO_ROWS;
}

/**
 * @brief Read from myPubs table and insert values on myPub address
 *        Where clause uses pubPk and pubInvite
 *        If you don't have pubInvite, just send a myPub.pubInvite with zeroes
 * 
 * @param myPub 
 * @return int 
 */
int readMyPub(myPub_t *myPub)
{

    sqlite3_stmt *pStmt;

    int rc, retValue;

    const char *sqlSelectMyPub = "Select pubPk, pubInvite, inviteStatus, pubTicket from myPubs "
                                 "where pubPk = x'%s'"
                                 " and pubInvite like '%s' ";

    char sqlDynamic[strlen(sqlSelectMyPub) + PUBLICKEY_SIZE_HEX + sizeof(myPub->pubInvite) + 2];

    char strPkHex[PUBLICKEY_SIZE_HEX];

    // dbPubs is not opened
    if (dbPubs == NULL)
    {
        rc = dbPubsInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Open Pubs Db");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    memset(sqlDynamic, 0, sizeof(sqlDynamic));

    strPk2Hex(strPkHex, myPub->pubPk);

    if (myPub->pubInvite[0] == 0)
        sprintf(sqlDynamic, sqlSelectMyPub, strPkHex, "%%");
    else
        sprintf(sqlDynamic, sqlSelectMyPub, strPkHex, myPub->pubInvite);

    memset(myPub, 0, sizeof(*myPub));
    // Read public and secret key
    rc = sqlite3_prepare_v2(dbPubs, sqlDynamic, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlDynamic);
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_READ;
    }

    rc = sqlite3_step(pStmt);

    retValue = PERSISTENCE_OK;

    if (rc == SQLITE_ROW) // There is record in the table
    {
        memcpy((char *)myPub->pubPk, sqlite3_column_blob(pStmt, 0), sizeof(myPub->pubPk));
        strncpy((char *)myPub->pubInvite, sqlite3_column_text(pStmt, 1), sizeof(myPub->pubInvite));
        myPub->inviteStatus = sqlite3_column_int(pStmt, 2);
        memcpy((char *)myPub->pubTicket, sqlite3_column_blob(pStmt, 3), sizeof(myPub->pubTicket));

        myPubBin2B64(myPub); // Populate Public key B64
    }
    else if (rc == SQLITE_DONE) // No problems but there is no row
    {
        retValue = PERSISTENCE_NO_ROWS;
    }
    else
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlDynamic);
        retValue = PERSISTENCE_ERR_READ;
    }

    sqlite3_finalize(pStmt);

    return retValue;
}

/**
 * @brief Insert values to myPubs table 
 * 
 * @param myPub 
 * @return int 
 */
int insertMyPub(myPub_t *myPub)
{

    sqlite3_stmt *pStmt;

    int rc, retValue;

    const char *sqlInsertMyPub = "insert into mypubs "
                                 "(pubPk, pubInvite, inviteStatus, pubTicket) "
                                 " values (?, ?, ?, ?)";

    // dbPubs is not opened
    if (dbPubs == NULL)
    {
        rc = dbPubsInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Open Pubs Db");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    // Read public and secret key
    rc = sqlite3_prepare_v2(dbPubs, sqlInsertMyPub, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlInsertMyPub);
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_INSERT;
    }

    sqlite3_bind_blob(pStmt, 1, myPub->pubPk, sizeof(myPub->pubPk), SQLITE_STATIC);
    sqlite3_bind_text(pStmt, 2, myPub->pubInvite, sizeof(myPub->pubInvite), SQLITE_STATIC);
    sqlite3_bind_int(pStmt, 3, myPub->inviteStatus);
    sqlite3_bind_text(pStmt, 4, myPub->pubTicket, sizeof(myPub->pubTicket), SQLITE_STATIC);

    rc = sqlite3_step(pStmt);

    retValue = PERSISTENCE_OK;

    if (rc != SQLITE_DONE) // Problem on Insert
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlInsertMyPub);
        retValue = PERSISTENCE_ERR_INSERT;
    }

    sqlite3_finalize(pStmt);

    return retValue;
}

int updateMyPub(myPub_t *myPub) 
{
    int rc, retValue;

    sqlite3_stmt *pStmt;

    const char *sqlUpdateMyPub =
        " Update mypubs set inviteStatus = %d, "
        "                   pubTicket = ? "
        "where pubInvite like \'%s\'";
    char sqlDynamic[strlen(sqlUpdateMyPub) +
                    PUB_INVITE_SIZE_B64 + 5];

    sprintf(sqlDynamic, sqlUpdateMyPub,
            myPub->inviteStatus,
            myPub->pubInvite);

    rc = sqlite3_prepare_v2(dbPubs, sqlDynamic, -1, &pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlDynamic);
        sqlite3_finalize(pStmt);
        return PERSISTENCE_ERR_INSERT;
    }

    sqlite3_bind_text(pStmt, 1, myPub->pubTicket, sizeof(myPub->pubTicket), SQLITE_STATIC);

    rc = sqlite3_step(pStmt);

    retValue = PERSISTENCE_OK;

    if (rc != SQLITE_DONE) // Problem on Insert
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlDynamic);
        retValue = PERSISTENCE_ERR_UPDATE;
    }

    sqlite3_finalize(pStmt);

    return retValue;
}

/**
 * @brief Execute a Select statement bring all fields
 * 
 * @param pStmt Pointer to Pointer - Return the pointer to select
 * @return int 
 */
int prepareMyPub(sqlite3_stmt **pStmt)
{

    int rc;

    const char *sqlSelectInvites = "Select "
                                   "pubPk, pubInvite, inviteStatus, pubTicket "
                                   "from myPubs ";

    // dbKeys is not opened
    if (dbPubs == NULL)
    {
        rc = persistenceInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Dbase Not Open");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    rc = sqlite3_prepare_v2(dbPubs, sqlSelectInvites, -1, pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlSelectInvites);
        sqlite3_finalize(*pStmt);
        return PERSISTENCE_ERR_READ;
    }

    return PERSISTENCE_OK;
}

/**
 * @brief Execute a Select statement bring all fields 
 *        The where clause consider only Pub
 * 
 * @param pStmt Pointer to Pointer - Return the pointer to select
 * @return int 
 */
int prepareMyPubByPub(sqlite3_stmt **pStmt, myPub_t *myPub)
{

    int rc;

    const char *sqlSelectMyPub = "Select "
                                 "pubPk, pubInvite, inviteStatus, pubTicket "
                                 "from myPubs "
                                 "where pubPk = x'%s'";

    char sqlDynamic[strlen(sqlSelectMyPub) + PUBLICKEY_SIZE_HEX + 2];

    char strPkHex[PUBLICKEY_SIZE_HEX];

    // dbKeys is not opened
    if (dbPubs == NULL)
    {
        rc = persistenceInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Dbase Not Open");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    memset(sqlDynamic, 0, sizeof(sqlDynamic));

    //strPk2Hex(strPkHex, myPub->pubPk);
    strPk2Hex(strPkHex, myPub->pubPk);

    sprintf(sqlDynamic, sqlSelectMyPub, strPkHex);

    rc = sqlite3_prepare_v2(dbPubs, sqlDynamic, -1, pStmt, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlDynamic);
        sqlite3_finalize(*pStmt);
        return PERSISTENCE_ERR_READ;
    }

    return PERSISTENCE_OK;
}

int nextMyPub(sqlite3_stmt *pStmt, myPub_t *myPub)
{
    int rc = sqlite3_step(pStmt);
    int retValue;

    memset(myPub, 0, sizeof(*myPub));

    retValue = PERSISTENCE_OK;

    if (rc == SQLITE_ROW) // There is record in the table
    {
        strncpy((char *)myPub->pubPk, sqlite3_column_text(pStmt, 0), sizeof(myPub->pubPk));
        strncpy((char *)myPub->pubInvite, sqlite3_column_text(pStmt, 1), sizeof(myPub->pubInvite));
        myPub->inviteStatus = sqlite3_column_int(pStmt, 2);
        strncpy((char *)myPub->pubTicket, sqlite3_column_text(pStmt, 3), sizeof(myPub->pubTicket));

        myPubBin2B64(myPub); // Populate Public key B64

        return retValue;
    }
    else if (rc == SQLITE_DONE) // No problems but there is no row
    {
        retValue = PERSISTENCE_NO_ROWS;
    }
    else
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, "On step cursor");
        retValue = PERSISTENCE_ERR_READ;
    }

    sqlite3_finalize(pStmt);
    return retValue;
}

/**
 * @brief Verify if node is a pub and has myPub record
 * 
 * @param node 
 * @return int 
 */
int verifyMyPub()
{
    node_t node;
    node_t *ptrNode = &node;
    myPub_t myPub;
    myPub_t *ptrMyPub = &myPub;
    unsigned char nonceSign[NONCE_MSG_SIZE];

    const char *sqlDeleteMyPub = "Delete "
                                 "from myPubs "
                                 "where pubPk = x'%s'";

    char sqlDynamic[strlen(sqlDeleteMyPub) + PUBLICKEY_SIZE_HEX + 2];

    unsigned char pubTicket[SIGNATURE_SIZE];
    char strPkHex[PUBLICKEY_SIZE_HEX];

    int rc;

    if (dbPubs == NULL)
    {
        rc = persistenceInit();
        if (rc != PERSISTENCE_OK)
        {
            showDbMsg(MSG_ERROR, __func__, dbPubs, "Dbase Not Open");
            return PERSISTENCE_ERR_OPEN;
        }
    }

    rc = readNode(&node);

    if (rc != PERSISTENCE_OK)
        return rc;

    if (node.nodeType == NODE_REGULAR)
        return PERSISTENCE_OK;

    memcpy(ptrMyPub->pubPk, ptrNode->pk, sizeof(node.pk));

    memset(&nonceSign, 0, sizeof(nonceSign));

    rc = signMsg(ptrNode->pk, nonceSign, ptrNode->sk, ptrMyPub->pubTicket, sizeof(node.pk));

    if (rc != CRYPTO_OK)
        return PERSISTENCE_ERR_GENERAL;

    myPubBin2B64(ptrMyPub);

    memset(sqlDynamic, 0, sizeof(sqlDynamic));

    //strPk2Hex(strPkHex, myPub->pubPk);
    strPk2Hex(strPkHex, ptrMyPub->pubPk);

    sprintf(sqlDynamic, sqlDeleteMyPub, strPkHex);

    rc = sqlite3_exec(dbPubs, sqlDynamic, NULL, 0, 0);

    if (rc != SQLITE_OK)
    {
        showDbMsg(MSG_ERROR, __func__, dbPubs, (char *)sqlDynamic);
        return PERSISTENCE_ERR_EXECUTE;
    }

    myPub.inviteStatus = INVITE_GRANTED;
    rc = insertMyPub(ptrMyPub);

    return PERSISTENCE_OK;
    
}
