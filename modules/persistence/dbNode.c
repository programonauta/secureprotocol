/**
 * @file dbNode.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief fuctions to deal with dbNode database
 * @version 0.1
 * @date 28-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "persistence.h"

sqlite3 *dbNode = NULL;

/**
 * @brief Initialize dbNode database, verify and create tables
 * 
 * @return int 
 */
int dbNodeInit()
{
  int rc = 0;
  const char *nodeDBName = "node.db";

  const char *sqlCreateTableNodes =
      "Create Table if not exists nodes ("
      " publicKey blob, "
      " secretKey blob, "
      " nodeType integer "
      " )";

  // If database is already open, close it first
  if (dbNode != NULL)
  {
    int rc = 0;
    rc = sqlite3_close(dbNode);
    if (rc)
    {
      showDbMsg(MSG_ERROR, __func__, dbNode, "Trying to close database");
      return PERSISTENCE_ERR_CLOSE;
    }
  }

  rc = sqlite3_open(nodeDBName, &dbNode);
  if (rc)
  {
    showDbMsg(MSG_ERROR, __func__, dbNode, "Trying to open database");
    return PERSISTENCE_ERR_OPEN;
  }

#ifdef DEBUG
  showDbMsg(MSG_LOG, __func__, dbNode, "Opened database successfully");
#endif

  // Verify if table exists, if not create it
  rc = sqlite3_exec(dbNode, sqlCreateTableNodes, NULL, 0, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbNode, (char *)sqlCreateTableNodes);
    return PERSISTENCE_ERR_CREATE_TAB;
  }

  return PERSISTENCE_OK;
}

/**
 * @brief save Keys on database
 * 
 * @param pk public key to be saved
 * @param sk secret key to be saved
 * @param pkSize 
 * @param skSize 
 * @param valueIsPub true if the node is a pub 0 for regular node
 * @return int  PERSISTENCE_OK        Everything is ok
 *        PERSISTENCE_NOT_OPEN  Cannot open database
 *        PERSISTENCE_NOT_READ  Cannot read the database content
 *        PERSISTENCE_NOT_WRITE Cannot write the database content
 *        PERSISTENCE_FAIL      Any other error hapens
 */
int insertNode(node_t *node)
{
  sqlite3_stmt *pStmt;

   int rc;

   const char *sqlTruncTableNodes = "Delete from nodes";
   const char *sqlInsertTableNodes = "Insert into nodes (publicKey, secretKey, nodeType) values (?, ?, ?)";

   // dbNode is not opened
   if (dbNode == NULL)
   {
     rc = dbNodeInit();
     if (rc != PERSISTENCE_OK)
     {
       showDbMsg(MSG_ERROR, __func__, dbNode, "Open Node db");
       return PERSISTENCE_ERR_OPEN;
     }
   }

   // Start a Begin Transaction
   rc = sqlite3_exec(dbNode, "BEGIN;", NULL, 0, 0);
   if (rc != SQLITE_OK)
   {
     showDbMsg(MSG_ERROR, __func__, dbNode, "Begin Transaction");
     return PERSISTENCE_ERR_BEGIN;
   }
 #ifdef DEBUG
   else
     showDbMsg(MSG_LOG, __func__, dbNode, "Begin Successfully");
 #endif

   // Drop table Keys
   rc = sqlite3_exec(dbNode, sqlTruncTableNodes, NULL, 0, 0);
   if (rc != SQLITE_OK)
   {
     showDbMsg(MSG_ERROR, __func__, dbNode, "Trunc Table Nodes");
     return PERSISTENCE_ERR_GENERAL;
   }
 #ifdef DEBUG
   showDbMsg(MSG_LOG, __func__, dbNode, "Table Nodes Deleted");
 #endif

   rc = sqlite3_prepare_v2(dbNode, sqlInsertTableNodes, -1, &pStmt, 0);
   if (rc != SQLITE_OK)
   {
     showDbMsg(MSG_ERROR, __func__, dbNode, "Insert keys");
     sqlite3_finalize(pStmt);
     return PERSISTENCE_ERR_INSERT;
   }
   sqlite3_bind_blob(pStmt, 1, node->pk, sizeof(node->pk), SQLITE_STATIC);
   sqlite3_bind_blob(pStmt, 2, node->sk, sizeof(node->sk), SQLITE_STATIC);
   sqlite3_bind_int(pStmt, 3, node->nodeType);
   rc = sqlite3_step(pStmt);
   if (rc != SQLITE_DONE)
   {
     showDbMsg(MSG_ERROR, __func__, dbNode, "Insert keys");
     sqlite3_finalize(pStmt);
     return PERSISTENCE_ERR_INSERT;
   }
 #ifdef DEBUG
   else
   {
     showDbMsg(MSG_LOG, __func__, dbNode, "Insert successfully");
   }
 #endif
   rc = sqlite3_exec(dbNode, "COMMIT;", NULL, 0, 0);
   if (rc != SQLITE_OK)
   {
     showDbMsg(MSG_ERROR, __func__, dbNode, "Insert Node Commit");
     sqlite3_finalize(pStmt);
     return PERSISTENCE_ERR_COMMIT;
   }
 #ifdef DEBUG
   else
   {
     showDbMsg(MSG_LOG, __func__, dbNode, "Commit Successfully");
   }
 #endif
   sqlite3_finalize(pStmt);
  
  return PERSISTENCE_OK;
}

/**
 * @brief Read node info from database
 * 
 * @return int PERSISTENCE_OK if found
 */
int readNode(node_t *node)
{
 
  sqlite3_stmt *pStmt;

  int rc;

  const char *sqlSelectNode = "Select publicKey, secretKey, nodeType from nodes";

   if (dbNode == NULL)
   {
     rc = dbNodeInit();
     if (rc != PERSISTENCE_OK)
     {
       showDbMsg(MSG_ERROR, __func__, dbNode, "Open Node db");
       return PERSISTENCE_ERR_OPEN;
     }
   }

  memset(node->pk, 0, sizeof(node->pk));
  memset(node->sk, 0, sizeof(node->sk));
  node->nodeType = NODE_REGULAR;

  // Read public and secret key
  rc = sqlite3_prepare_v2(dbNode, sqlSelectNode, -1, &pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbNode, (char *)sqlSelectNode);
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_READ;
  }

  // read first row in the select
  rc = sqlite3_step(pStmt);

  if (rc == SQLITE_ROW) // There is record in the table
  {
    // Verify size of public key
    if (sqlite3_column_bytes(pStmt, 0) == sizeof(node->pk) &&
        sqlite3_column_bytes(pStmt, 1) == sizeof(node->sk))
    {
      memcpy((char *)node->pk, sqlite3_column_blob(pStmt, 0), sizeof(node->pk));
      memcpy((char *)node->sk, sqlite3_column_blob(pStmt, 1), sizeof(node->sk));
      node->nodeType = sqlite3_column_int64(pStmt, 2);
    }
    else
    {
#ifdef DEBUG
      fprintf(stderr, "Size of keys doesn't match: size pk (%d), size sk (%d)\n",
              sqlite3_column_bytes(pStmt, 0),
              sqlite3_column_bytes(pStmt, 1));
#endif
      sqlite3_finalize(pStmt);
      return PERSISTENCE_ERR_READ;
    }
  }
  else if (rc == SQLITE_DONE) // No problems but there is no row
  {
    sqlite3_finalize(pStmt);
    return PERSISTENCE_NO_ROWS;
  }
  else
  {
#ifdef DEBUG
    fprintf(stderr, "Error when gather the key. Code: %d\n", rc);
#endif
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_READ;
  }

  if (verifyNode(node))
  {
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_READ;
  }

  sqlite3_finalize(pStmt);

  nodeBin2B64(node);

  return PERSISTENCE_OK;
}