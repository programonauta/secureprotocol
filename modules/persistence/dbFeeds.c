/**
 * @file dbFeeds.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Functions to deal with dbFeeds Database
 * @version 0.1
 * @date 01-Jun-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "persistence.h"

sqlite3 *dbFeeds = NULL;

/**
 * @brief Initialize dbFeeds database, verify and create tables
 * 
 * @return int Enum Persistence Message Returns
 */
int dbFeedsInit()
{
  int rc = 0;
  const char *feedsDBName = "feeds.db";

  const char *sqlCreateTableFeedHeader =
      "Create Table if not exists feedHeader ("
      " pubPk blob, "
      " senderPk blob, "
      " pubTicket blob, "
      " feedTable text primary key "
      " )";

  rc = sqlite3_open(feedsDBName, &dbFeeds);
  if (rc)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, "Trying to open database");
    return PERSISTENCE_ERR_OPEN;
  }

#ifdef DEBUG
  showDbMsg(MSG_LOG, __func__, dbFeeds, "Opened database successfully");
#endif

  rc = sqlite3_exec(dbFeeds, sqlCreateTableFeedHeader, NULL, 0, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlCreateTableFeedHeader);
    return PERSISTENCE_ERR_CREATE_TAB;
  }

  return PERSISTENCE_OK;
}

/**
 * @brief Verify feedHeader table insert if not exists register 
 *        and create table msgBlock for pair Pub x Sender provided
 *        in the parameter
 * 
 * @param feedHeader 
 * @return int PERSISTENCE_OK, PERSISTENCE_ERR_GENERAL (if invalid ticket), PERSISTENCE_ERR_OPEN, PERSISTENCE_ERR_INSERT, PERSISTENCE_ERR_CREATE_TAB
 */
int verifyFeedHeader(feedHeader_t *feedHeader)
{

  sqlite3_stmt *pStmt;

  int rc, retValue;

  unsigned char nonceSign[NONCE_MSG_SIZE];

  char tableName[SIZE_TABLE_NAME];

  const char *sqlInsertFeedHeader = "insert into feedHeader "
                                    "(pubPk, senderPk, pubTicket, feedTable) "
                                    " values (?, ?, ?, ?)";

  const char *sqlCreateTableMsgBlock =
      "Create Table if not exists %s ("
      " previousHASH blob, "
      " sequence int, "
      " recipientPk blob, "
      " tagMsg text, "
      " nonceSign blob, "
      " message text, "
      " signature blob, "
      " blockHASH blob, "
      " confirmations int"
      " )";

  char sqlDynamic[strlen(sqlCreateTableMsgBlock) + SIZE_TABLE_NAME + 2];

  memset(nonceSign, 0, sizeof(nonceSign));

  rc = verifySignature(feedHeader->senderPk, nonceSign, feedHeader->pubPk, feedHeader->pubTicket, sizeof(feedHeader->senderPk));

  if (rc != CRYPTO_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, "Invalid Ticket");
    return PERSISTENCE_ERR_GENERAL;
  }

  feedTableName(tableName, feedHeader->pubPk, feedHeader->senderPk);

  // dbPubs is not opened
  if (dbFeeds == NULL)
  {
    rc = dbFeedsInit();
    if (rc != PERSISTENCE_OK)
    {
      showDbMsg(MSG_ERROR, __func__, dbFeeds, "Open Feeds Db");
      return PERSISTENCE_ERR_OPEN;
    }
  }

  rc = sqlite3_prepare_v2(dbFeeds, sqlInsertFeedHeader, -1, &pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlInsertFeedHeader);
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_INSERT;
  }

  sqlite3_bind_blob(pStmt, 1, feedHeader->pubPk, sizeof(feedHeader->pubPk), SQLITE_STATIC);
  sqlite3_bind_blob(pStmt, 2, feedHeader->senderPk, sizeof(feedHeader->senderPk), SQLITE_STATIC);
  sqlite3_bind_blob(pStmt, 3, feedHeader->pubTicket, sizeof(feedHeader->pubTicket), SQLITE_STATIC);
  sqlite3_bind_text(pStmt, 4, tableName, sizeof(tableName), SQLITE_STATIC);

  // read first row in the select
  rc = sqlite3_step(pStmt);

  retValue = PERSISTENCE_OK;

  if (rc != SQLITE_DONE && rc != SQLITE_CONSTRAINT) // Problem on Insert
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlInsertFeedHeader);
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_INSERT;
  }

  sqlite3_finalize(pStmt);

  sprintf(sqlDynamic, sqlCreateTableMsgBlock, tableName);

  rc = sqlite3_exec(dbFeeds, sqlDynamic, NULL, 0, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    return PERSISTENCE_ERR_CREATE_TAB;
  }

  return retValue;
}

/**
 * @brief Return the table name based on Pub and Sender Public Keys
 * 
 * @param tableName 
 * @param pub 
 * @param sender 
 * @return const char* 
 */
const char *feedTableName(char *tableName, unsigned char *pub, unsigned char *sender)
{
  int i;

  memset(tableName, 0, SIZE_TABLE_NAME);

  tableName[0] = 't';

  for (i = 0; i < 8; i++)
    sprintf(tableName + (i * 2 + 1), "%02x", pub[i]);

  tableName[17] = 'X';

  for (i = 0; i < 8; i++)
    sprintf(tableName + (i * 2 + 18), "%02x", sender[i]);

  return tableName;
}

/**
 * @brief Create a select and update the pointer pStmt
 * 
 * @param pStmt 
 * @return int PERSISTENCE_OK if ok, 
 *             PERSISTENCE_ERR_OPEN if any problem with database
 *             PERSISTENCE_ERR_READ if any problem with prepare statement
 */
int prepareFeedHeader(sqlite3_stmt **pStmt)
{

  int rc;

  const char *sqlSelectFeedHeader = "Select "
                                    "pubPk, senderPk, pubTicket "
                                    "from feedHeader ";

  // dbKeys is not opened
  if (dbFeeds == NULL)
  {
    rc = persistenceInit();
    if (rc != PERSISTENCE_OK)
    {
      showDbMsg(MSG_ERROR, __func__, dbFeeds, "Dbase Not Open");
      return PERSISTENCE_ERR_OPEN;
    }
  }

  rc = sqlite3_prepare_v2(dbFeeds, sqlSelectFeedHeader, -1, pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlSelectFeedHeader);
    sqlite3_finalize(*pStmt);
    return PERSISTENCE_ERR_READ;
  }

  return PERSISTENCE_OK;
}

/**
 * @brief Get the Ticket From Feed Header table
 * 
 * @param feedHeader the strucutre must be filled with pubPk and senderPk
 *                   the pubTicket found will be returned in the same strucutre
 * @return int PERSISTENCE_OK, PERSISTENCE_NO_ROWS, or PERSISTENCE_ERR_OPEN
 */
int getTicketFromFeedHeader(feedHeader_t *feedHeader)
{

  int rc, retValue;
  sqlite3_stmt *pStmt;

  const char *sqlSelectFeedHeader = "Select "
                                    "pubTicket "
                                    "from feedHeader "
                                    "where pubPk = x'%s' "
                                    "and senderPk = x'%s' ";

  //char sqlDynamic[strlen(sqlSelectFeedHeader) + (PUBLICKEY_SIZE_HEX * 2) + 2];
  char sqlDynamic[250];

  char strPubHex[PUBLICKEY_SIZE_HEX];
  char strSenderHex[PUBLICKEY_SIZE_HEX];

  // database is not opened
  if (dbFeeds == NULL)
  {
    rc = persistenceInit();
    if (rc != PERSISTENCE_OK)
    {
      showDbMsg(MSG_ERROR, __func__, dbFeeds, "Dbase Not Open");
      return PERSISTENCE_ERR_OPEN;
    }
  }

  memset(sqlDynamic, 0, sizeof(sqlDynamic));

  strPk2Hex(strPubHex, feedHeader->pubPk);
  strPk2Hex(strSenderHex, feedHeader->senderPk);

  sprintf(sqlDynamic, sqlSelectFeedHeader, strPubHex, strSenderHex);

  rc = sqlite3_prepare_v2(dbFeeds, sqlDynamic, -1, &pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_READ;
  }

  rc = sqlite3_step(pStmt);

  retValue = PERSISTENCE_OK;

  if (rc == SQLITE_ROW) // There is record in the table
  {
    memcpy(feedHeader->pubTicket, sqlite3_column_blob(pStmt, 0), sizeof(feedHeader->pubTicket));

    feedHeaderBin2B64(feedHeader); // Populate Public key B64

    return retValue;
  }
  else if (rc == SQLITE_DONE) // No problems but there is no row
  {
    memset(feedHeader->pubTicketB64, 0, sizeof(feedHeader->pubTicket));
    feedHeaderBin2B64(feedHeader); // Populate Public key B64

    retValue = PERSISTENCE_NO_ROWS;
  }
  else
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, "On step cursor");
    retValue = PERSISTENCE_ERR_READ;
  }

  sqlite3_finalize(pStmt);
  return retValue;

  return PERSISTENCE_OK;
}

/**
 * @brief Move to next record in the pointer and
 *        populate feedHeader structure
 * 
 * @param pStmt 
 * @param feedHeader 
 * @return int 
 */
int nextFeedHeader(sqlite3_stmt *pStmt, feedHeader_t *feedHeader)
{
  int rc = sqlite3_step(pStmt);
  int retValue;

  memset(feedHeader, 0, sizeof(*feedHeader));

  retValue = PERSISTENCE_OK;

  if (rc == SQLITE_ROW) // There is record in the table
  {
    memcpy(feedHeader->pubPk, sqlite3_column_blob(pStmt, 0), sizeof(feedHeader->pubPk));
    memcpy(feedHeader->senderPk, sqlite3_column_blob(pStmt, 1), sizeof(feedHeader->senderPk));
    memcpy(feedHeader->pubTicket, sqlite3_column_blob(pStmt, 2), sizeof(feedHeader->pubTicket));

    feedHeaderBin2B64(feedHeader); // Populate Public key B64

    return retValue;
  }
  else if (rc == SQLITE_DONE) // No problems but there is no row
  {
    retValue = PERSISTENCE_NO_ROWS;
  }
  else
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, "On step cursor");
    retValue = PERSISTENCE_ERR_READ;
  }

  sqlite3_finalize(pStmt);
  return retValue;
}

/**
 * @brief Populate blockHASH and sequence of last message stored
 * 
 * @param tableName 
 * @param msgBlock 
 * @return int PERSISTENCE_OK, PERSISTENCE_NO_ROWS, PERSISTENCE_ERR_OPEN, PERSISTENCE_ERR_READ
 */
int getLastMsgBlock(char *tableName, msgBlock_t *msgBlock)
{
  sqlite3_stmt *pStmt;

  int rc, retValue;

  const char *sqlSelectMsgBlock = "Select blockHASH, sequence from %s "
                                  " order by sequence desc limit 1 ";

  char sqlDynamic[strlen(sqlSelectMsgBlock) + SIZE_TABLE_NAME + 2];

  // dbFeeds is not opened
  if (dbFeeds == NULL)
  {
    rc = dbFeedsInit();
    if (rc != PERSISTENCE_OK)
    {
      showDbMsg(MSG_ERROR, __func__, dbFeeds, "Open Feeds Db");
      return PERSISTENCE_ERR_OPEN;
    }
  }

  memset(sqlDynamic, 0, sizeof(sqlDynamic));

  sprintf(sqlDynamic, sqlSelectMsgBlock, tableName);

  rc = sqlite3_prepare_v2(dbFeeds, sqlDynamic, -1, &pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_READ;
  }

  rc = sqlite3_step(pStmt);

  retValue = PERSISTENCE_OK;

  memset(msgBlock->blockHASH, 0, sizeof(msgBlock->blockHASH));
  msgBlock->sequence = 0;

  if (rc == SQLITE_ROW) // There is record in the table
  {
    memcpy(msgBlock->blockHASH, sqlite3_column_blob(pStmt, 0), sizeof(msgBlock->blockHASH));
    msgBlock->sequence = sqlite3_column_int(pStmt, 1);
  }
  else if (rc == SQLITE_DONE) // No problems but there is no row
  {
    retValue = PERSISTENCE_NO_ROWS;
  }
  else
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    retValue = PERSISTENCE_ERR_READ;
  }

  sqlite3_finalize(pStmt);

  return retValue;
}

int prepareMsgBlockBySequence(feedDetail_t *feedDetail, sqlite3_stmt **pStmt)
{

  int rc, retValue;

  char tableName[SIZE_TABLE_NAME];

  const char *sqlSelectMsgBlock = "Select previousHash, "
                                  "sequence, "
                                  "recipientPk, "
                                  "tagMsg, "
                                  "nonceSign, "
                                  "message, "
                                  "signature, "
                                  "blockHASH, "
                                  "confirmations "
                                  " from %s "
                                  " where sequence >= %d order by sequence ";

  char sqlDynamic[strlen(sqlSelectMsgBlock) + SIZE_TABLE_NAME + 80];

  // dbFeeds is not opened
  if (dbFeeds == NULL)
  {
    rc = dbFeedsInit();
    if (rc != PERSISTENCE_OK)
    {
      showDbMsg(MSG_ERROR, __func__, dbFeeds, "Open Feeds Db");
      return PERSISTENCE_ERR_OPEN;
    }
  }

  memset(sqlDynamic, 0, sizeof(sqlDynamic));

  feedTableName(tableName, feedDetail->pubPk, feedDetail->senderPk);

  sprintf(sqlDynamic, sqlSelectMsgBlock, tableName, feedDetail->lastSequence);

  rc = sqlite3_prepare_v2(dbFeeds, sqlDynamic, -1, pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    sqlite3_finalize(*pStmt);
    return PERSISTENCE_ERR_READ;
  }

  return PERSISTENCE_OK;
}

int nextMsgBlock(sqlite3_stmt *pStmt, msgBlock_t *msgBlock)
{
  int rc = sqlite3_step(pStmt);
  int retValue;

  memset(msgBlock, 0, sizeof(msgBlock_t));

  retValue = PERSISTENCE_OK;

  if (rc == SQLITE_ROW) // There is record in the table
  {
    memcpy(msgBlock->previousHASH, sqlite3_column_blob(pStmt, 0), sizeof(msgBlock->previousHASH));
    msgBlock->sequence = sqlite3_column_int(pStmt, 1);
    memcpy(msgBlock->recipientPk, sqlite3_column_blob(pStmt, 2), sizeof(msgBlock->recipientPk));
    msgBlock->tagMsg = sqlite3_column_int(pStmt, 3);
    memcpy(msgBlock->nonceSign, sqlite3_column_blob(pStmt, 4), sizeof(msgBlock->nonceSign));
    memcpy(msgBlock->message, sqlite3_column_text(pStmt, 5), sizeof(msgBlock->message));
    memcpy(msgBlock->signature, sqlite3_column_blob(pStmt, 6), sizeof(msgBlock->signature));
    memcpy(msgBlock->blockHASH, sqlite3_column_blob(pStmt, 7), sizeof(msgBlock->blockHASH));
    msgBlock->confirmations = sqlite3_column_int(pStmt, 8);

    msgBlockBin2B64(msgBlock); // Populate Public key B64

    return retValue;
  }
  else if (rc == SQLITE_DONE) // No problems but there is no row
  {
    retValue = PERSISTENCE_NO_ROWS;
  }
  else
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, "On step cursor");
    retValue = PERSISTENCE_ERR_READ;
  }

  sqlite3_finalize(pStmt);
  return retValue;
}

int insertMsgBlock(char *tableName, msgBlock_t *msgBlock)
{
  sqlite3_stmt *pStmt;

  int rc, retValue;

  const char *sqlInsertMsgBlock = "insert into %s ("
                                  "previousHash, "
                                  "sequence, "
                                  "recipientPk, "
                                  "tagMsg, "
                                  "nonceSign,"
                                  "message, "
                                  "signature, "
                                  "blockHash, "
                                  "confirmations"
                                  ") "
                                  " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

  char sqlDynamic[strlen(sqlInsertMsgBlock) + SIZE_TABLE_NAME + 2];

  memset(sqlDynamic, 0, sizeof(sqlDynamic));

  sprintf(sqlDynamic, sqlInsertMsgBlock, tableName);

  // dbFeeds is not opened
  if (dbFeeds == NULL)
  {
    rc = dbFeedsInit();
    if (rc != PERSISTENCE_OK)
    {
      showDbMsg(MSG_ERROR, __func__, dbFeeds, "Open Feeds Db");
      return PERSISTENCE_ERR_OPEN;
    }
  }
  // Read public and secret key
  rc = sqlite3_prepare_v2(dbFeeds, sqlDynamic, -1, &pStmt, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    sqlite3_finalize(pStmt);
    return PERSISTENCE_ERR_INSERT;
  }

  sqlite3_bind_blob(pStmt, 1, msgBlock->previousHASH, sizeof(msgBlock->previousHASH), SQLITE_STATIC);
  sqlite3_bind_int(pStmt, 2, msgBlock->sequence);
  sqlite3_bind_blob(pStmt, 3, msgBlock->recipientPk, sizeof(msgBlock->recipientPk), SQLITE_STATIC);
  sqlite3_bind_int(pStmt, 4, msgBlock->tagMsg);
  sqlite3_bind_blob(pStmt, 5, msgBlock->nonceSign, sizeof(msgBlock->nonceSign), SQLITE_STATIC);
  sqlite3_bind_text(pStmt, 6, msgBlock->message, sizeof(msgBlock->message), SQLITE_STATIC);
  sqlite3_bind_blob(pStmt, 7, msgBlock->signature, sizeof(msgBlock->signature), SQLITE_STATIC);
  sqlite3_bind_blob(pStmt, 8, msgBlock->blockHASH, sizeof(msgBlock->blockHASH), SQLITE_STATIC);
  sqlite3_bind_int(pStmt, 9, msgBlock->confirmations);

  // read first row in the select
  rc = sqlite3_step(pStmt);

  retValue = PERSISTENCE_OK;

  if (rc != SQLITE_DONE) // Problem on Insert
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    retValue = PERSISTENCE_ERR_INSERT;
  }

  sqlite3_finalize(pStmt);

  return retValue;
}

/**
 * @brief Update confirmations Msg Block 
 * 
 * @param tableName 
 * @param ptrMsgBlock 
 * @return int PERSISTENCE_OK or PERSISTENCE_ERR_UPDATE
 */
int updateMsgBlock(char *tableName, msgBlock_t *ptrMsgBlock)
{
  int rc;

  const char *sqlUpdateMsgBlock =
      " Update %s set confirmations = %d "
      "where sequence = %d";
  char sqlDynamic[strlen(sqlUpdateMsgBlock) + 30];

  sprintf(sqlDynamic, sqlUpdateMsgBlock,
          tableName,
          ptrMsgBlock->confirmations,
          ptrMsgBlock->sequence);

  rc = sqlite3_exec(dbFeeds, sqlDynamic, NULL, 0, 0);

  if (rc != SQLITE_OK)
  {
    showDbMsg(MSG_ERROR, __func__, dbFeeds, (char *)sqlDynamic);
    return PERSISTENCE_ERR_UPDATE;
  }

  return PERSISTENCE_OK;
}

int verifyMsgBlock(msgBlock_t *ptrMsgBlock, feedDetail_t *ptrFeedDetail)
{
  int rc;

  sqlite3_stmt *pStmt;
  feedDetail_t prevFeedDetail;
  msgBlock_t prevMsgBlock;
  msgBlock_t *ptrPrevMsgBlock = &prevMsgBlock;

  memset(&prevMsgBlock, 0, sizeof(prevMsgBlock));
  memcpy(&prevFeedDetail, ptrFeedDetail, sizeof(prevFeedDetail));

  if (ptrMsgBlock->sequence > 1) // Get the HASH of previous msgBlock
  {
    prevFeedDetail.lastSequence--; // Try to get the previous msgBlock stored
    rc = prepareMsgBlockBySequence(&prevFeedDetail, &pStmt);
    if (rc != PERSISTENCE_OK)
      return PERSISTENCE_ERR_GENERAL;

    rc = nextMsgBlock(pStmt, &prevMsgBlock);

    if (rc == PERSISTENCE_NO_ROWS)
      return MSG_BLOCK_INVALID_SEQUENCE;
    else if (rc != PERSISTENCE_OK)
      return PERSISTENCE_ERR_GENERAL;

    if ((prevMsgBlock.sequence+1)!=ptrMsgBlock->sequence)
      return MSG_BLOCK_INVALID_SEQUENCE;

  }

  if (memcmp(ptrMsgBlock->previousHASH, ptrPrevMsgBlock->blockHASH, sizeof(ptrMsgBlock->previousHASH)) != 0)
    return MSG_BLOCK_INVALID_PREVIOUS_HASH;

  rc = verifySignMsgBlock(ptrMsgBlock, ptrFeedDetail->senderPk);

  if (rc != MSG_BLOCK_OK)
    return MSG_BLOCK_INVALID_SIGNATURE;

  rc = verifyHashMsgBlock(ptrMsgBlock);
  if (rc != MSG_BLOCK_OK)
    return MSG_BLOCK_INVALID_HASH;

  return MSG_BLOCK_OK;
}

/**
 * @brief Get the Last Feed Detail stored in msgBlock from a given pub x sender
 * 
 * @param feedDetail 
 * @return int PERSISTENCE_OK, PERSISTENCE_NO_ROWS, PERSISTENCE_ERR_OPEN, PERSISTENCE_ERR_READ
 */
int getLastFeedDetail(feedDetail_t *feedDetail)
{
  int rc;
  char tableName[SIZE_TABLE_NAME];

  msgBlock_t msgBlock;
  msgBlock_t *ptrMsgBlock = &msgBlock;

  feedHeader_t feedHeader;
  feedHeader_t *ptrFeedDetail = &feedHeader;

  memcpy(ptrFeedDetail->pubPk, feedDetail->pubPk, sizeof(feedDetail->pubPk));
  memcpy(ptrFeedDetail->senderPk, feedDetail->senderPk, sizeof(feedDetail->senderPk));

  feedTableName(tableName, feedDetail->pubPk, feedDetail->senderPk);

  rc = getTicketFromFeedHeader(ptrFeedDetail);

  memcpy(feedDetail->pubTicket, ptrFeedDetail->pubTicket, sizeof(feedDetail->pubTicket));

  rc = getLastMsgBlock(tableName, &msgBlock);
  if (rc != PERSISTENCE_OK && rc != PERSISTENCE_NO_ROWS)
    return rc;

  memcpy(feedDetail->lastHASH, ptrMsgBlock->blockHASH, sizeof(msgBlock.blockHASH));
  feedDetail->lastSequence = msgBlock.sequence;

  feedDetailBin2B64(feedDetail);
  return rc;
}
