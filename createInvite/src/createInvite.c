/**
 * @file createInvite.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief 
 * @version 0.1
 * @date 27-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <libgen.h>
#include <string.h>
#include "persistence.h"

#define OPTSTR "ho:v"  // : after option make argument not optional

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char        verbose;
  FILE        *outputInvite;
} options_t;

/**
 * @brief 
 * 
 * @param prgName 
 */
void helpMsg(char* prgName);

int main(int argc, char* argv[])
{
  node_t node;

  inviteIssued_t invite;

  unsigned char pkB64[PUBLICKEY_SIZE_B64];

  int opt, rc;

  int sum = 0, i;
  
  char fileName[80];
  options_t options = { 0, NULL };

  opterr = 0;

  memset(fileName, 0, sizeof(fileName));

  while ((opt = getopt(argc, argv, OPTSTR)) != EOF)
    switch(opt)
    {
    case 'o':
      if (strlen(optarg) > 70)
      {
        fprintf(stderr, "\nERROR: File name must be maximum 70 characters\n");
        exit(EXIT_FAILURE);
      }

      strcpy(fileName, optarg);
      break;

    case 'v':
      options.verbose += 1;
      break;

    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    default:
      fprintf(stderr, "\nERROR: Invalid argument -%c\n", (char)opt);
      helpMsg(argv[0]);
      break;
    }

  if (sodium_init() < 0) // TODO create a library init on crypto module
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_FAILURE);
  }

  rc = persistenceInit();
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database:\n");
    return (EXIT_FAILURE);
  }

  rc = readNode(&node);

  if (rc == PERSISTENCE_OK)
  {
    if (node.nodeType == NODE_REGULAR)
    {
      fprintf(stderr, "\nERROR: This node is not a pub\n");
      rc = persistenceFinish();
      return (EXIT_FAILURE);
    }
  }
  else if (rc == PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "\nERROR: There is no information about this node, please run createKeys\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }
  else
  {
    fprintf(stderr, "\nERROR: When try to get Pub Information.\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }

  // Populate Invite structure

  memcpy(invite.nodePk, node.pk, sizeof(invite.nodePk));
  if (createInvite(invite.inviteB64, sizeof(invite.inviteB64)) != CRYPTO_OK) // 
  {
    fprintf(stderr, "Error creating the invite\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }

  if (rc)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc);
    rc = persistenceFinish();
    return(EXIT_FAILURE);
  }

  invite.inviteStatus = INVITE_CREATED;

  // convert Node Public Key to Base 64
  bin2b64((char*)pkB64, sizeof(pkB64), node.pk, sizeof(node.pk));

  int sequence;

  rc = insertInviteIssued(&invite, &sequence);
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR: Save Invite failed\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }

  if (fileName[0] == 0)
    sprintf(fileName, "%.6s-%06d.inv", pkB64, sequence);

  for (i=0; i<6; i++)
    if (strchr("\\/#(){}[].;<>&|*$", fileName[i]) != NULL)
      fileName[i] = '+';

  if (!(options.outputInvite = fopen(fileName, "wb")) )
  {
    fprintf(stderr, "\nERROR: Error trying to create file: %s\n", fileName);
    rc = persistenceFinish();
    exit(EXIT_FAILURE);
  }

  fputs((char*)pkB64, options.outputInvite);
  fputc('\n', options.outputInvite);
  fputs((char*)invite.inviteB64, options.outputInvite);
  fputc('\n', options.outputInvite);

  fclose(options.outputInvite);

  printf("Invite Created: %s (%d Bytes)\n", invite.inviteB64, PUB_INVITE_SIZE);
  printf("To PUB Address: %s\n", pkB64);
  printf("Wrote at File : %s\n", fileName);

  rc = persistenceFinish();

  return (EXIT_SUCCESS);
}

void helpMsg(char* prgName)
{
  printf("\nUSAGE: %s [OPTIONS]\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -o FILENAME (if not provided file name, will be xxxxxx-nnnnnn.inv)\n");
  printf("        where:\n");
  printf("            - xxxxxx (first 6 bytes of base64 Pub's Public Key\n");
  printf("            - nnnnnn (invite sequence)\n");
  printf("    -h             show this text\n");
  printf("    -v verbose\n");
  /* NOTREACHED */
}
