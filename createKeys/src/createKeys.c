/*
 * createKeys
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <libgen.h>
#include <string.h>
#include "persistence.h"

#define OPTSTR "ht:v" // : after option make argument not optional

extern int errno;
extern char *optarg;
extern int opterr, optind;

typedef struct
{
  char verbose;
  char nodeType;
} options_t;

char verbose = 0;

void helpMsg(char *prgName);

int main(int argc, char *argv[])
{
  unsigned char pkB64[PUBLICKEY_SIZE_B64];

  int opt, rc;
  int i;

  options_t options = {0, 0};

  node_t node;

  opterr = 0;

  if (argc < 2) // There is no args
  {
    fprintf(stderr, "\nERROR: Please inform the type of node\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt(argc, argv, OPTSTR)) != EOF)
    switch (opt)
    {
    case 't':

      if (strcmp(optarg, "r") == 0)
        options.nodeType = 'r';
      else if (strcmp(optarg, "p") == 0)
        options.nodeType = 'p';
      else
      {
        fprintf(stderr, "\nERROR: invalid argument for option -t (%s)\n", optarg);
        helpMsg(argv[0]);
        exit(EXIT_FAILURE);
      }
      break;

    case 'v':
      options.verbose += 1;
      break;

    case 'h':
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    default:
      fprintf(stderr, "\nERROR: Invalid argument -%c\n", (char)opt);
      helpMsg(argv[0]);
      return (EXIT_SUCCESS);
    }

  if (options.nodeType == 0)
  {
    fprintf(stderr, "\nERROR: Please inform node type\n");
    helpMsg(argv[0]);
    exit(EXIT_FAILURE);
  }

  verbose = options.verbose;

  if (sodium_init() < 0)
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_FAILURE);
  }

  rc = persistenceInit();
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database:\n");
    return (EXIT_FAILURE);
  }

  rc = readNode(&node);

  if (rc == PERSISTENCE_OK)
  {
    printf("Key Pair already exists in database\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }
  else if (rc != PERSISTENCE_NO_ROWS)
  {
    fprintf(stderr, "Can't get keys - Error code %d\n", rc); //TODO create text msg for error 
    return (EXIT_FAILURE);
  }

  // Create new keys
  createKeyPair(node.pk, node.sk);
  printf("Key Pair created\n");

  // Verify Key Pair
  rc = verifyKeyPair(node.pk, node.sk);

  if (rc != CRYPTO_OK)
  {
    fprintf(stderr, "Key Pair Invalid\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }

  printf("Key Pair Tested - OK\n");

  node.nodeType = (options.nodeType == 'p'?NODE_PUB: NODE_REGULAR);

  rc = insertNode(&node);
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "Save Keys failed\n");
    rc = persistenceFinish();
    return (EXIT_FAILURE);
  }

  bin2b64((char *)pkB64, sizeof(pkB64), node.pk, sizeof(node.pk));

  printf("Public Key (Base64): %s\n", pkB64);
  
  printf("Public Key (Hex)   : ");
  for (i=0; i < sizeof(node.pk); i++)
    printf("%02x", node.pk[i]);

  printf("\n");

  rc = persistenceFinish();

  return (EXIT_SUCCESS);
}

void helpMsg(char *prgName)
{
  printf("\nUSAGE: %s [OPTIONS] -t [r|p]\n\n", basename(prgName));
  printf("OPTIONS\n");
  printf("    -h  show this text\n");
  printf("    -t type [r|p] Node Type: (r)egular or (p)ub\n");
  printf("    -v verbose\n");
  return;
}
