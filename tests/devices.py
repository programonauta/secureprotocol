import os
import subprocess
import multiprocessing
import shutil
import signal
import time

class Device:
    def __init__(self, _type, _wDir, _rootDir):
        self.type = _type
        self.wDir = _wDir
        self.rootDir = _rootDir
        self.b64Pk = ''
        self.inviteFiles = []
        self.installedInvites = []
        self.serverPID = 0

    def createKeys(self):
        os.mkdir(self.wDir)
        os.chdir(self.wDir)

        cmdCreateKey = self.rootDir+'/createKeys/build/createKeys'

        cmd = [cmdCreateKey, "-t", self.type]
        retCmd = subprocess.run(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
        )

        if (retCmd.returncode==0):
            listRetCmd=retCmd.stdout.split('\n')
            self.b64Pk = listRetCmd[2].split(' ')[-1]
        else:
            print(retCmd.stdout)
            print(retCmd.stderr)

        assert retCmd.returncode == 0
        return retCmd.returncode

    def createInvites(self, qtyInvites):
        os.chdir(self.wDir)

        cmdCreateInvite = self.rootDir+'/createInvite/build/createInvite'

        cmd = [cmdCreateInvite]
        for i in range(qtyInvites):
            retCmd = subprocess.run(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
            )

            if (retCmd.returncode==0):
                listRetCmd=retCmd.stdout.split('\n')
                self.inviteFiles.append((listRetCmd[2].split(' ')[-1], listRetCmd[0].split(' ')[2]))
            else:
                print(retCmd.stdout)
                print(retCmd.stderr)

            assert retCmd.returncode == 0
        return retCmd.returncode

    def importInvite(self, dev):
        if self.type != 'p':
            print("Must be a pub node to install an invite")
            return 1
        
        strImportedInvite =''

        if len(self.inviteFiles) == 0:
            self.createInvites(1)

        self.installedInvites.append((self.inviteFiles[0][0], self.inviteFiles[0][1], dev))

        shutil.move(self.wDir+'/'+self.inviteFiles[0][0], dev.wDir+'/'+self.inviteFiles[0][0])
        
        del self.inviteFiles[0]

        os.chdir(dev.wDir)

        cmdImportInvite = self.rootDir+'/importInvite/build/importInvite'

        cmd = [cmdImportInvite, '-i', self.installedInvites[-1][0]]

        retCmd = subprocess.run(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
        )

        if (retCmd.returncode==0):
            listRetCmd=retCmd.stdout.split('\n')
            strImportedInvite = listRetCmd[3].split(' ')[-1]
        else:
            print(retCmd.stdout)
            print(retCmd.stderr)

        assert retCmd.returncode == 0 and strImportedInvite == self.installedInvites[-1][1]
        return retCmd.returncode

    def startServer(self, dev):
        cmdServer = self.rootDir+'/server/build/server'

        print("Starting Server...")

        os.chdir(dev.wDir)
        cmd = [cmdServer]
        #retCmd = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        retCmd = subprocess.Popen(cmd)

        return retCmd

    def getTicket(self, pub):

        proc = self.startServer(pub)

        time.sleep(2)

        os.chdir(self.wDir)

        cmdGetTicket = self.rootDir+'/getTicket/build/getTicket'

        cmd = [cmdGetTicket, '-k', pub.b64Pk, '-p', '127.0.0.1']

        retCmd = subprocess.run(cmd)
#        retCmd = subprocess.run(
#            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
#        )

        if (retCmd.returncode==0):
            pass
            #listRetCmd=retCmd.stdout.split('\n')
            #strImportedInvite = listRetCmd[3].split(' ')[-1]
        else:
            print(retCmd.stdout)
            print(retCmd.stderr)

        proc.kill()

        time.sleep(2)

        assert retCmd.returncode == 0
        
        return retCmd.returncode

    def createMsg(self, strMsg, pub, devRecip):
        os.chdir(self.wDir)

        cmdCreateMessage = self.rootDir+'/createMsg/build/createInvite'

        cmd = [cmdCreateMessage, '-k', pub.b64Pk, '-r', devRecip.b64Pk, '-m', strMsg]
        retCmd = subprocess.run(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
        )

        if (retCmd.returncode==0):
            listRetCmd=retCmd.stdout.split('\n')
            self.inviteFiles.append((listRetCmd[2].split(' ')[-1], listRetCmd[0].split(' ')[2]))
        else:
            print(retCmd.stdout)
            print(retCmd.stderr)

        assert retCmd.returncode == 0
        return retCmd.returncode


