#
# Tests of device creations
#

import os
from devices import Device
import shutil
import time


def showError(msgErr):
    print("Error on script")
    print("---------------")
    print(msgErr)
    exit(1)


def cleanDirectory(dirName):
    if os.path.exists(dirName):
        shutil.rmtree(dirName)
    os.makedirs(dirName)

strCurrentDir = os.getcwd()

listCurrentDir = strCurrentDir.split("/")
if strCurrentDir.split("/")[-1] != "tests":
    showError("Please run test on tests directory")


listCurrentDir.remove("tests")
strRootDir = "/".join(listCurrentDir)

print("Root Directory: " + strRootDir)

strDataDir = strCurrentDir + "/data"

strPub1Dir = strDataDir + "/pub1"
strPub2Dir = strDataDir + "/pub2"
strPub3Dir = strDataDir + "/pub3"
strDev1Dir = strDataDir + "/dev1"
strDev2Dir = strDataDir + "/dev2"
strDev3Dir = strDataDir + "/dev3"
strDev4Dir = strDataDir + "/dev4"

cleanDirectory(strDataDir)

pub1 = Device("p", strPub1Dir, strRootDir)
pub2 = Device("p", strPub2Dir, strRootDir)
pub3 = Device("p", strPub3Dir, strRootDir)
dev1 = Device("r", strDev1Dir, strRootDir)
dev2 = Device("r", strDev2Dir, strRootDir)
dev3 = Device("r", strDev3Dir, strRootDir)
dev4 = Device("r", strDev4Dir, strRootDir)

pub1.createKeys()
pub2.createKeys()
pub3.createKeys()
dev1.createKeys()
dev2.createKeys()
dev3.createKeys()
dev4.createKeys()

print("Creating Devices... OK!")

pub1.createInvites(3)
pub2.createInvites(2)
pub3.createInvites(1)

print("Creating Invites... OK!")

pub1.importInvite(dev1)
pub1.importInvite(dev2)
pub1.importInvite(dev4)
pub2.importInvite(dev2)
pub2.importInvite(dev3)
pub3.importInvite(dev4)

print("Importing Invites... OK!")

print("Getting Tickets")

dev1.getTicket(pub1)
print("Dev1 get Pub1 Ticket... OK!")

dev2.getTicket(pub1)
print("Dev2 get Pub1 Ticket... OK!")

dev2.getTicket(pub2)
print("Dev2 get Pub2 Ticket... OK!")

dev3.getTicket(pub2)
print("Dev3 get Pub2 Ticket... OK!")

dev4.getTicket(pub1)
print("Dev4 get Pub1 Ticket... OK!")
dev4.getTicket(pub3)
print("Dev4 get Pub3 Ticket... OK!")

assert True
