cmake_minimum_required(VERSION 3.0.0)
project(exStore1stMsg VERSION 0.1.0)

file(GLOB SOURCES "src/*.c" 
    "../../modules/persistence/*.c"
    "../../modules/cryptography/*.c"
    "../../modules/control/*.c")

link_libraries(sqlite3 sodium)

include_directories(../../modules)

add_executable(exStore1stMsg ${SOURCES})