/**
 * @file exStore1stMsg.c
 * @author Ricardo Brandao (rbrandao@protonmail.com)
 * @brief Example to store the first message
 * @version 0.1
 * @date 21-May-2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#define __USE_MINGW_ANSI_STDIO
#include <stdio.h>
#include <string.h>
#include "persistence/persistence.h"
#include "control/control.h"
#include "cryptography/cryptography.h"

int main(int argc, char **argv)
{
  int rc, errCode;
  const char* errorMsg;

  int mStored, mErrors;

  unsigned char pk[PUBLICKEY_SIZE];
  unsigned char sk[SECRETKEY_SIZE];

  rc = persistenceInit(&errCode, &errorMsg);
  if (rc != PERSISTENCE_OK)
  {
    fprintf(stderr, "\nERROR Init Database: (%d-%d) %s\n", rc, errCode, errorMsg);
    return (EXIT_FAILURE);
  }

  rc = getKeys(pk, sk, sizeof(pk), sizeof(sk));

  rc = storeAffiliationRequest(&mStored, &mErrors, pk, sk);

  rc = persistenceFinish(&errCode, &errorMsg);

  return 0;
}
