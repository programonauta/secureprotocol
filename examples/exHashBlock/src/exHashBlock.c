/*
 * Example of hashBlock and veirifyHashBlock functions
 *
 */
#include <stdio.h>
#include <string.h>
#include "cryptography/cryptography.h"
#include "control/control.h"

int main(int argc, char **argv)
{
  msgBlock_t msgBlock;

  int rc;

  unsigned char hash[HASH_SIZE] = { 0xc1, 0xf3, 0x29, 0x00, 0x26, 0x3a, 0x91, 0xb3,
                                    0xb2, 0x43, 0xc0, 0x42, 0x74, 0x75, 0x9f, 0x96,
                                    0x91, 0x05, 0xbb, 0xde, 0xc5, 0x26, 0x24, 0x29,
                                    0x99, 0x0d, 0xb9, 0x7d, 0x87, 0x62, 0x6b, 0x12
                                  };

  unsigned char senderPk[PUBLICKEY_SIZE], senderSk[SECRETKEY_SIZE];
  unsigned char recipientPk[PUBLICKEY_SIZE], recipientSk[SECRETKEY_SIZE];

  unsigned char flatMsg[MESSAGE_SIZE];

  if (sodium_init() < 0)
  {
    /* panic! the library couldn't be initialized, it is not safe to use */
    fprintf(stderr, "Cryptography library couldn't be initalized\n");
    return (EXIT_SUCCESS);
  }

  // Create two keypairs
  crypto_sign_ed25519_keypair(senderPk,  senderSk);
  crypto_sign_ed25519_keypair(recipientPk,  recipientSk);

  resetMsgBlock(&msgBlock);

  // Initialize Message Block with some examples.
  memcpy(msgBlock.previousHASH, hash, HASH_SIZE);
  memcpy(msgBlock.recipient, recipientPk, PUBLICKEY_SIZE);
  msgBlock.sequence = 3404;
//  strcat((char*)msgBlock.message, "Hello World!");

  rc = encryptMsgBlock(&msgBlock, senderSk, (unsigned char*)"Hello World!! Nice job!", sizeof("Hello World!! Nice job!"));

  signMessage(&msgBlock, senderSk);

  rc = hashBlock(&msgBlock);

  if (rc != CONTROL_OK)
    return EXIT_FAILURE;

  unsigned char* hashB64[HASH_SIZE_B64];

  bin2b64((char*)hashB64, HASH_SIZE_B64, msgBlock.blockHASH, (size_t)HASH_SIZE);

  printf("Block Hash - Base64: %s\n", (char*)hashB64);

  rc = verifyHash(&msgBlock);
  if (rc != CONTROL_OK)
  {
    printf("ERROR: Hash wrong\n");
    return EXIT_FAILURE;
  }

  printf("Hash verified!!\n");

  rc = verfiySignature(&msgBlock, senderPk);
  if (rc != CONTROL_OK)
  {
    printf("ERROR: Signature wrong\n");
    return EXIT_FAILURE;
  }

  printf("Signature verified!!\n");

  rc = decryptMsgBlock(&msgBlock, senderPk, recipientSk, flatMsg);
  if (rc != CONTROL_OK)
  {
    printf("ERROR: Decryption fail\n");
    return EXIT_FAILURE;
  }
  
  printf("Message Decrypted:%s\n", flatMsg);
  
  
  return 0;
}
