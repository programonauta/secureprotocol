/*
 * Example of signMessage function
 *
 */
#include <stdio.h>
#include <string.h>
#include "cryptography/cryptography.h"
#include "control/control.h"

int main(int argc, char **argv)
{

  msgBlock_t msgBlock;

  int rc;

  unsigned char pk[PUBLICKEY_SIZE] = {0x54, 0x74, 0xa1, 0x0f, 0xaf, 0x77, 0x09, 0x87, 
                                      0x0d, 0x7a, 0xe3, 0x1e, 0x37, 0x0a, 0x7e, 0xfb, 
                                      0xd2, 0xf1, 0x17, 0x55, 0x86, 0xb6, 0x06, 0x9a, 
                                      0x5b, 0x62, 0x68, 0x77, 0xc6, 0x7e, 0xbc, 0x07};
                                      
  unsigned char sk[SECRETKEY_SIZE] = {0xdd, 0x92, 0x5c, 0x32, 0x86, 0x6c, 0x74, 0xef, 
                                      0xbd, 0xc9, 0x50, 0x17, 0x87, 0xde, 0x45, 0x38, 
                                      0x50, 0x0c, 0x4a, 0x20, 0x7c, 0x73, 0x23, 0x17, 
                                      0x9d, 0x28, 0xa8, 0xa6, 0xf0, 0xc1, 0xa1, 0xc8, 
                                      0x54, 0x74, 0xa1, 0x0f, 0xaf, 0x77, 0x09, 0x87, 
                                      0x0d, 0x7a, 0xe3, 0x1e, 0x37, 0x0a, 0x7e, 0xfb, 
                                      0xd2, 0xf1, 0x17, 0x55, 0x86, 0xb6, 0x06, 0x9a, 
                                      0x5b, 0x62, 0x68, 0x77, 0xc6, 0x7e, 0xbc, 0x07};

  unsigned char hash[HASH_SIZE] = { 0xc1, 0xf3, 0x29, 0x00, 0x26, 0x3a, 0x91, 0xb3,
                                    0xb2, 0x43, 0xc0, 0x42, 0x74, 0x75, 0x9f, 0x96,
                                    0x91, 0x05, 0xbb, 0xde, 0xc5, 0x26, 0x24, 0x29,
                                    0x99, 0x0d, 0xb9, 0x7d, 0x87, 0x62, 0x6b, 0x12};

  memset(msgBlock.message, 0, sizeof(msgBlock.message));
  memset(msgBlock.signature, 0, sizeof(msgBlock.signature));
  memset(msgBlock.blockHASH, 0, sizeof(msgBlock.blockHASH));

  // Initialize Message Block with some examples.
  memcpy(msgBlock.previousHASH, hash, HASH_SIZE);
  memcpy(msgBlock.recipient, pk, PUBLICKEY_SIZE);
  msgBlock.sequence = 2034;
  msgBlock.tagMsg = '\0x00';

  rc = signMessage(&msgBlock, sk);
  if (rc != CONTROL_OK)
    fprintf(stderr, "ERROR(%d): Signing message\n", rc);

  strcat((char*)msgBlock.message, "Hello World!");
  
  rc = signMessage(&msgBlock, sk);
  if (rc != CONTROL_OK)
    return EXIT_FAILURE;

  printf("Message Signed %s\n", msgBlock.message);
  
  rc = verfiySignature(&msgBlock, pk);
  
  if (rc == CONTROL_OK)
    printf("Signature Verified OK\n");
  else if (rc == CONTROL_WRONG_SIGNATURE)
    printf("ERROR: Signature not verified!!\n");
  else
    printf("ERROR: Error on verify %d\n", rc);
  
  return EXIT_SUCCESS;
}
