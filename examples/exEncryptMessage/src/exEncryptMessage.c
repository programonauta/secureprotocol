/*
 * Example of encrpytMessage function
 *
 */
#include <stdio.h>
#include <string.h>
#include "cryptography/cryptography.h"
#include "persistence/persistence.h"
#include "control/control.h"

int main(int argc, char **argv)
{

  msgBlock_t msgBlock;
  
  unsigned char flatMsg1[MESSAGE_SIZE];
  
  unsigned char encMsg1[MESSAGE_SIZE+ TAG_SIZE];
  
  unsigned char flatMsg2[MESSAGE_SIZE];

  memset(flatMsg1, 0, sizeof(flatMsg1));
  memset(flatMsg2, 0, sizeof(flatMsg2));

  int rc;

  unsigned char senderPk[PUBLICKEY_SIZE], senderSk[SECRETKEY_SIZE], 
                recipientPk[PUBLICKEY_SIZE], recipientSk[SECRETKEY_SIZE];

  unsigned char senderEncPk[crypto_box_PUBLICKEYBYTES];
  unsigned char senderEncSk[crypto_box_SECRETKEYBYTES];
  unsigned char recipientEncPk[crypto_box_PUBLICKEYBYTES];
  unsigned char recipientEncSk[crypto_box_SECRETKEYBYTES];
  
  unsigned char hash[HASH_SIZE] = { 0xc1, 0xf3, 0x29, 0x00, 0x26, 0x3a, 0x91, 0xb3,
                                    0xb2, 0x43, 0xc0, 0x42, 0x74, 0x75, 0x9f, 0x96,
                                    0x91, 0x05, 0xbb, 0xde, 0xc5, 0x26, 0x24, 0x29,
                                    0x99, 0x0d, 0xb9, 0x7d, 0x87, 0x62, 0x6b, 0x12};

  memset(msgBlock.message, 0, sizeof(msgBlock.message));
  memset(msgBlock.signature, 0, sizeof(msgBlock.signature));
  memset(msgBlock.blockHASH, 0, sizeof(msgBlock.blockHASH));

  // Create two keypairs

  crypto_sign_ed25519_keypair(senderPk,  senderSk);
  crypto_sign_ed25519_keypair(recipientPk, recipientSk);
  
  rc = crypto_sign_ed25519_pk_to_curve25519(senderEncPk, senderPk);
  rc = crypto_sign_ed25519_pk_to_curve25519(recipientEncPk, recipientPk);

  crypto_sign_ed25519_sk_to_curve25519(senderEncSk, senderSk);
  crypto_sign_ed25519_sk_to_curve25519(recipientEncSk, recipientSk);
  
  
  // Initialize Message Block with some examples.
  memcpy(msgBlock.previousHASH, hash, HASH_SIZE);
  memcpy(msgBlock.recipient, recipientPk, sizeof(msgBlock.recipient));
  msgBlock.sequence = 2034;

  sprintf((char*)flatMsg1, "Hello World! This is a long message to test !!");
  size_t msgLen = strlen((char*)flatMsg1);

  unsigned char nonce[crypto_box_NONCEBYTES];
  memset(nonce, 0, sizeof(nonce));

  int i;
  
  printf("\n---------------- Recipient ----------------\n");
  printf("PK   : ");
  for (i = 0; i<sizeof(recipientPk); i++)
    printf("%02x", recipientPk[i]);
  printf("\nSK   : ");
  for (i = 0; i<sizeof(recipientSk); i++)
    printf("%02x", recipientSk[i]);
  printf("\nEncPK: ");
  for (i = 0; i<sizeof(recipientEncPk); i++)
    printf("%02x", recipientEncPk[i]);
  printf("\nEncSK: ");
  for (i = 0; i<sizeof(recipientEncSk); i++)
    printf("%02x", recipientEncSk[i]);

  printf("\n---------------- Sender ----------------\n");
  printf("PK   : ");
  for (i = 0; i<sizeof(senderPk); i++)
    printf("%02x", senderPk[i]);
  printf("\nSK   : ");
  for (i = 0; i<sizeof(senderSk); i++)
    printf("%02x", senderSk[i]);
  printf("\nEncPK: ");
  for (i = 0; i<sizeof(senderEncPk); i++)
    printf("%02x", senderEncPk[i]);
  printf("\nEncSK: ");
  for (i = 0; i<sizeof(senderEncSk); i++)
    printf("%02x", senderEncSk[i]);
  printf("\n");


  printf("\n---------------- BEGIN crypto_box_easy ----------------\n");
  rc = crypto_box_easy(encMsg1, flatMsg1, msgLen, nonce, recipientEncPk, senderEncSk);
  if (rc != CRYPTO_OK)
    fprintf(stderr, "[%s] - crypto_box_easy Error in encryption\n", __func__);
  else  
    fprintf(stderr, "[%s] - crypto_box_easy Encryption OK\n", __func__ );

  printf("[%s] - crypto_box_easy Encrypted Message - (%lu bytes)\n", __func__, msgLen+crypto_box_MACBYTES);
  for (i = 0; i<(msgLen+crypto_box_MACBYTES); i++)
    printf("%02x", encMsg1[i]);
  printf("\n---------------- END crypto_box_easy ----------------\n");

  rc = crypto_box_open_easy(flatMsg2, encMsg1, msgLen+crypto_box_MACBYTES, nonce, senderEncPk, recipientEncSk);
  if (rc != CRYPTO_OK)
    fprintf(stderr, "[%s] - crytpo_box_open Error in decryption\n", __func__);
  else  
    fprintf(stderr, "[%s] - crytpo_box_open Decryption OK. Message: %s\n", __func__, flatMsg2);

  printf("\n---------------- BEGIN encryptMsg ----------------\n");
  rc = encryptMsg(flatMsg1, msgLen, encMsg1, recipientPk, senderSk);
  if (rc != CRYPTO_OK)
    fprintf(stderr, "[%s] - encryptMsg Error in encryption\n", __func__);
  else  
    fprintf(stderr, "[%s] - encryptMsg Encryption OK\n", __func__);

  printf("[%s] - encryptMsg Encrypted Message - (%lu bytes)\n", __func__, msgLen+TAG_SIZE);
  for (i = 0; i<(msgLen+TAG_SIZE); i++)
    printf("%02x", encMsg1[i]);
  printf("\n---------------- END encrytpMsg ----------------\n\n");


  printf("\n---------------- BEGIN decryptMsg ----------------\n");
  rc = decryptMsg(flatMsg2, msgLen, encMsg1, senderPk, recipientSk);
  if (rc != CRYPTO_OK)
    fprintf(stderr, "[%s] - decryptMsg Error in decryption\n", __func__);
  else  
    fprintf(stderr, "[%s] - Decryption OK. Message: %s\n", __func__, flatMsg2);
  printf("\n---------------- END decryptMsg ----------------\n");

  printf("\n\n---------------- BEGIN encryptMsgBlock ----------------\n");
  rc = encryptMsgBlock(&msgBlock, senderSk, flatMsg1, msgLen);
  if (rc != CRYPTO_OK)
    fprintf(stderr, "[%s] - encryptMsgBlock Error in encryption\n", __func__);
  else  
    fprintf(stderr, "[%s] - encryptMsgBlock Encryption OK\n", __func__);
  printf("encryptMsgBlock Encrypted Message (Base64)\n%s\n", msgBlock.message);
  printf("\n---------------- END encryptMsgBlock ----------------\n");

  memset(flatMsg2, 0, sizeof(flatMsg2));

  printf("\n---------------- BEGIN decryptMsgBlock ----------------\n");
  rc = decryptMsgBlock(&msgBlock, senderPk, recipientSk, flatMsg2);
  if (rc != CRYPTO_OK)
    fprintf(stderr, "[%s] - decryptMsgBlock Error in encryption\n", __func__);
  else  
    fprintf(stderr, "[%s] - decryptMsgBlock Decryption OK: %s\n", __func__, flatMsg2);
  printf("\n---------------- END decryptMsgBlock ----------------\n");


  printf("\n---------------- signMessage ----------------\n");
  rc = signMessage(&msgBlock, senderSk);
  if (rc != CONTROL_OK)
    fprintf(stderr, "ERROR(%d): Signing message\n", rc);

  printf("Message Signed %s\n", msgBlock.message);
  
  rc = verfiySignature(&msgBlock, senderPk);
  
  if (rc == CONTROL_OK)
    printf("Signature Verified OK\n");
  else if (rc == CONTROL_WRONG_SIGNATURE)
    printf("ERROR: Signature not verified!!\n");
  else
    printf("ERROR: Error on verify %d\n", rc);
  
  
  return EXIT_SUCCESS;
}
