cmake_minimum_required(VERSION 3.0.0)
project(exEncryptMessage VERSION 0.1.0)

file(GLOB SOURCES_EXENCRYPTMSG "src/*.c" 
    "../../modules/persistence/*.c"
    "../../modules/cryptography/*.c"
    "../../modules/control/*.c")

link_libraries(sqlite3 sodium)

include_directories(../../modules)

add_executable(exEncryptMessage ${SOURCES_EXENCRYPTMSG})