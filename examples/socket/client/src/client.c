/**
 * @file client.c
 * @author Ricardo Brandão
 * @brief A stream socket cliente demo
 * @version 0.1
 * @date 2021-05-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>
#include "protocol/protocol.h"

#define PORT "8008" // the port client will be connecting to 


/**
 * @brief Main program to connect to server
 * 
 * @param argc 
 * @param argv hostname to connect
 * @return int 
 */
int main(int argc, char *argv[])
{
  int sockFd;
  enum ipFamily ipFamily;

  if (argc != 2)
  {
    fprintf(stderr,"usage: client hostname\n");
    exit(1);
  }

  ipFamily = ipFamilyAny;

  sockFd = tcpConnect(argv[1], PORT, ipFamily);
  
  if (sockFd < 0)
  {
    fprintf(stderr, "Error trying to contact server\n");
    return EXIT_FAILURE;
  }

  char s[INET6_ADDRSTRLEN];
  getPeerIP(sockFd, s);
  printf("client: got connection to server %s\n", s);

  protocolCli(sockFd);

  close(sockFd);

  return 0;
}
