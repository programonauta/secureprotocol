.PHONY: clean All

All:
	@echo "----------Building project:[ createInvite - Debug ]----------"
	@cd "createInvite" && "$(MAKE)" -f  "createInvite.mk"
clean:
	@echo "----------Cleaning project:[ createInvite - Debug ]----------"
	@cd "createInvite" && "$(MAKE)" -f  "createInvite.mk" clean
